/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/HomePage/HomePage.scss":
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/HomePage/HomePage.scss ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/getUrl.js */ "./node_modules/css-loader/dist/runtime/getUrl.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fonts_EastmanTrial_Black_otf__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../fonts/EastmanTrial-Black.otf */ "./src/fonts/EastmanTrial-Black.otf");
// Imports



var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
var ___CSS_LOADER_URL_REPLACEMENT_0___ = _node_modules_css_loader_dist_runtime_getUrl_js__WEBPACK_IMPORTED_MODULE_1___default()(_fonts_EastmanTrial_Black_otf__WEBPACK_IMPORTED_MODULE_2__["default"]);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "@charset \"UTF-8\";\n* {\n  box-sizing: border-box;\n  padding: 0;\n  margin: 0;\n}\n\n/*\n*Declaración de los colores del documento\n*/\n/*\n* Definición de la fuente importada.\n* \n* Para usarla, copiar y pegar la primera línea en donde se vaya a usar dicha fuente.\n* Si se van a importa   r más fuentes, deben ponerse en la carpeta fonts y seguir este mismo proceso.\n*/\n@font-face {\n  font-family: \"Eastman\";\n  src: url(" + ___CSS_LOADER_URL_REPLACEMENT_0___ + ") format(\"otf\");\n  font-weight: 600;\n  font-style: normal;\n}\nhtml {\n  font-family: Helvetica;\n  scroll-behavior: smooth;\n  overflow-x: hidden;\n}\n\na {\n  text-decoration: none;\n}\n\n.container {\n  min-height: 100vh;\n  width: 100%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n}\n\n.npage h1 {\n  font-family: \"Eastman\";\n  font-size: 35px;\n  text-transform: uppercase;\n  color: #797aa3;\n}\n\n#header {\n  position: fixed;\n  z-index: 1000;\n  left: 0;\n  top: 0;\n  width: 100vw;\n  height: auto;\n}\n#header .header {\n  min-height: 8vh;\n  background-color: #cae7b9, 0;\n  transition: 0.3s ease background-color;\n}\n#header .nav-bar {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n  width: 100%;\n  height: 100%;\n  max-width: 1300px;\n  padding: 0 10px;\n}\n#header .nav-bar .npage {\n  cursor: pointer;\n}\n#header .nav-list ul {\n  list-style: none;\n  position: initial;\n  width: fit-content;\n  height: auto;\n  left: 100%;\n  top: 0;\n  display: block;\n  flex-direction: column;\n  justify-content: center;\n  align-items: center;\n  z-index: 1;\n  overflow-x: hidden;\n  transition: 0.5s ease left;\n}\n#header .nav-list ul:active {\n  left: 0%;\n}\n#header .nav-list ul li {\n  display: inline-block;\n}\n#header .nav-list ul li:hover a {\n  color: #797aa3;\n}\n#header .nav-list ul li:hover a:after {\n  transform: translate(-50%, -50%) scale(1);\n  letter-spacing: initial;\n}\n#header .nav-list ul li a {\n  font-size: 17px;\n  font-weight: 700;\n  letter-spacing: 2px;\n  text-decoration: none;\n  color: #6ba0c8;\n  text-transform: uppercase;\n  padding: 20px;\n  display: block;\n  transition: 0.3s ease;\n}\n\n#presentation .presentation {\n  background-color: #ffffff;\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 0;\n  position: relative;\n  flex-flow: wrap row;\n}\n#presentation .presentation .parrafo {\n  display: flex;\n  margin-right: 50px;\n  text-align: center;\n  align-items: center;\n  position: absolute;\n  width: 410px;\n  height: 650px;\n  flex-flow: wrap row;\n  padding: 10px;\n  left: 60px;\n  margin-top: 10px;\n  border-radius: 40px;\n}\n#presentation .presentation .parrafo #presentationTitle {\n  position: relative;\n  font-size: 70px;\n  width: 100%;\n  height: 50px;\n  color: #797aa3;\n  margin-top: -440px;\n}\n#presentation .presentation .parrafo .presentationIconColor {\n  position: absolute;\n  top: 250px;\n  left: 30px;\n  opacity: 0;\n  transition: 0.5s ease;\n  background-color: #ffffff;\n}\n#presentation .presentation .parrafo .presentationIconColor img {\n  height: 120px;\n  margin: 20px;\n}\n#presentation .presentation .parrafo .presentationIconColor:hover {\n  transform: rotate(15deg);\n}\n#presentation .presentation .parrafo .presentationIcon {\n  position: absolute;\n  top: 250px;\n  left: 30px;\n}\n#presentation .presentation .parrafo .presentationIcon img {\n  height: 120px;\n  margin: 20px;\n}\n#presentation .presentation .skills {\n  position: absolute;\n  flex-flow: wrap row;\n  margin-top: 10px;\n  width: 600px;\n  height: 500px;\n  display: flex;\n  text-align: start;\n  padding-left: 15px;\n  right: 60px;\n}\n#presentation .presentation .skills .skillTitle {\n  position: relative;\n  display: flex;\n  justify-content: center;\n  height: 70px;\n  width: 90%;\n  margin-bottom: -70px;\n  border-radius: 30px;\n  line-height: 70px;\n  font-size: 50px;\n}\n#presentation .presentation .skills .prog-lang, #presentation .presentation .skills .adobe {\n  display: flex;\n  flex-flow: column nowrap;\n  width: 50%;\n}\n#presentation .presentation .skills .prog-lang .bar, #presentation .presentation .skills .adobe .bar {\n  height: 40px;\n  margin: 2px 0 2px 0;\n  border-radius: 15px;\n  width: 80%;\n}\n#presentation .presentation .skills .prog-lang .bar .filler, #presentation .presentation .skills .adobe .bar .filler {\n  height: 100%;\n  width: 0%;\n  transition: 0.5s ease;\n  border-radius: 12px 0 0 12px;\n}\n#presentation .presentation .skills .skill-title {\n  margin: 7% 0 8% 0;\n  font-size: 35px;\n}\n#presentation .presentation .skills .prog-lang .bar {\n  border: 2px #000000 solid;\n}\n#presentation .presentation .skills .prog-lang .bar .filler {\n  background-color: #6ba0c8;\n}\n#presentation .presentation .skills .adobe .bar {\n  border: 2px #000000 solid;\n}\n#presentation .presentation .skills .adobe .bar .filler {\n  background-color: #cae7b9;\n}\n\n#biography .biography {\n  display: flex;\n  position: relative;\n  justify-content: center;\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 100px 0;\n}\n#biography .biography #Titulo_Bio {\n  position: absolute;\n  text-align: center;\n  font-size: 70px;\n  bottom: 400px;\n  width: 100vw;\n  top: 95px;\n  font-weight: bold;\n}\n#biography .biography .Exp_Educ {\n  padding: 0px;\n  margin: 10px;\n  display: flex;\n  justify-content: center;\n  position: relative;\n  left: 0;\n  top: 35px;\n  height: 750px;\n  width: 45vw;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos {\n  display: flex;\n  justify-content: flex-start;\n  flex-wrap: wrap;\n  position: relative;\n  top: 170px;\n  width: 600px;\n  height: 600px;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboSolitario {\n  opacity: 0;\n  left: 200px;\n  top: 200px;\n  display: flex;\n  text-align: center;\n  align-items: center;\n  justify-content: center;\n  position: absolute;\n  width: 165px;\n  height: 165px;\n  transform: rotate(45deg) scale(0, 0);\n  background-color: #eb9486;\n  transition: 0.5s ease;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboSolitario:hover {\n  top: 190px;\n  box-shadow: 8px 8px 5px 0px #797aa3;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboSolitario .TextoInfo {\n  transform: rotate(-45deg);\n  font-size: 18px;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboInfo {\n  left: 20px;\n  top: 0;\n  display: flex;\n  text-align: center;\n  align-items: center;\n  justify-content: center;\n  padding: 0;\n  margin: 50px;\n  position: relative;\n  width: 165px;\n  height: 165px;\n  transform: rotate(45deg) scale(0, 0);\n  background-color: #eb9486;\n  transition: 0.5s ease;\n  opacity: 0;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboInfo:hover {\n  top: -10px;\n  box-shadow: 8px 8px 5px 0px #797aa3;\n}\n#biography .biography .Exp_Educ #OrganizadorRombos .RomboInfo .TextoInfo {\n  transform: rotate(-45deg);\n  font-size: 18px;\n}\n#biography .biography .Exp_Educ #Titulo_Exp {\n  position: absolute;\n  color: #000000;\n  font-size: 70px;\n  bottom: 400px;\n  width: 100vw;\n  top: 60px;\n  left: 40px;\n  font-weight: unset;\n}\n#biography .biography .Exp_Educ #Titulo_Edu {\n  position: absolute;\n  color: #000000;\n  font-size: 70px;\n  bottom: 400px;\n  width: 100vw;\n  top: 60px;\n  left: 110px;\n  font-weight: unset;\n}\n\n#projects .projects {\n  flex-direction: column;\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 100px 0;\n  position: relative;\n}\n#projects .projects .projects-header {\n  position: relative;\n}\n#projects .projects .projects-header h1 {\n  margin-bottom: 50px;\n}\n#projects .projects .projects-header h1 span {\n  color: #cae7b9;\n}\n#projects .projects .projects-title {\n  font-size: 70px;\n  font-weight: bold;\n  color: #000000;\n  margin-bottom: 10px;\n  text-transform: uppercase;\n  letter-spacing: 0.2rem;\n  text-align: center;\n}\n#projects .projects .all-projects {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: column;\n}\n#projects .projects .all-projects .project-item {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  flex-direction: row;\n  height: 400px;\n  width: 100%;\n  margin: 0;\n  overflow: hidden;\n  border-radius: 0;\n}\n#projects .projects .all-projects .project-item:nth-child(even) {\n  flex-direction: row-reverse;\n}\n#projects .projects .all-projects .project-item .project-info {\n  padding: 30px;\n  flex-basis: 50%;\n  height: 100%;\n  display: flex;\n  align-items: flex-start;\n  justify-content: center;\n  flex-direction: column;\n  background-image: linear-gradient(60deg, #6ba0c8 0%, #797aa3 100%);\n  color: #ffffff;\n}\n#projects .projects .all-projects .project-item .project-info .ptitulo {\n  display: block;\n  width: fit-content;\n  font-size: 4rem;\n  font-weight: 500;\n  position: relative;\n  color: transparent;\n  /*animation: text_reveal .5s ease forwards;*/\n  animation-delay: 1s;\n}\n#projects .projects .all-projects .project-item .project-info .ptitulo .animationP {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 0;\n  background-color: #ffffff;\n  /*animation: text_reveal_box 1s ease;*/\n  animation-delay: 0.5s;\n}\n#projects .projects .all-projects .project-item .project-info .psubtitulo {\n  display: block;\n  width: fit-content;\n  font-size: 1.8rem;\n  font-weight: 500;\n  margin-top: 10px;\n  position: relative;\n  color: transparent;\n  /*animation: text_reveal .5s ease forwards;*/\n  animation-delay: 1s;\n}\n#projects .projects .all-projects .project-item .project-info .psubtitulo .animationP {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 0;\n  background-color: #ffffff;\n  /*animation: text_reveal_box 1s ease;*/\n  animation-delay: 0.5s;\n}\n#projects .projects .all-projects .project-item .project-info .ptexto {\n  display: block;\n  width: fit-content;\n  color: #ffffff;\n  font-size: 1.4rem;\n  margin-top: 5px;\n  line-height: 2.5rem;\n  font-weight: 300;\n  letter-spacing: 0.05rem;\n  position: relative;\n  color: transparent;\n  /*animation: text_reveal .5s ease forwards;*/\n  animation-delay: 1s;\n}\n#projects .projects .all-projects .project-item .project-info .ptexto .animationP {\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 100%;\n  width: 0;\n  background-color: #ffffff;\n  /*animation: text_reveal_box 1s ease;*/\n  animation-delay: 0.5s;\n}\n#projects .projects .all-projects .project-item .project-img {\n  flex-basis: 50%;\n  height: 100%;\n  overflow: hidden;\n  position: relative;\n}\n#projects .projects .all-projects .project-item .project-img:after {\n  content: \"\";\n  position: absolute;\n  left: 0;\n  top: 0;\n  height: 100%;\n  width: 100%;\n  background-image: linear-gradient(60deg, #458fe4 0%, #485563 100%);\n  opacity: 0.4;\n}\n#projects .projects .all-projects .project-item .project-img img {\n  height: 100%;\n  width: 100%;\n  object-fit: cover;\n  transition: 0.3s ease transform;\n}\n#projects .projects .all-projects .project-item .project-img:hover img {\n  transform: scale(1.1);\n}\n\n#photos .photos {\n  background-color: #ffffff;\n  text-align: center;\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 100px 0;\n  display: flex;\n  justify-content: center;\n  flex-flow: wrap row;\n}\n#photos .photos #photoTitle {\n  width: 100vw;\n  font-size: 60px;\n  position: relative;\n  text-align: center;\n  margin: 10px;\n  margin-left: 50px;\n  font-weight: bold;\n}\n#photos .photos #photoSubTitle {\n  width: 100vw;\n  font-size: 30px;\n  position: relative;\n  text-align: start;\n  margin: 10px;\n  margin-left: 50px;\n  margin-bottom: 30px;\n}\n#photos .photos .display {\n  position: relative;\n  top: 0px;\n  left: 0px;\n  height: 650px;\n  width: 100vw;\n  display: grid;\n  grid-column: 4;\n  grid-template-columns: 350px auto 350px;\n  grid-template-rows: 300px 300px;\n  align-content: center;\n  grid-row-gap: 50px;\n  padding: 15px;\n  overflow-y: hidden;\n  opacity: 0;\n  transition: 0.8s ease;\n}\n#photos .photos .display .displayPhotoBig {\n  grid-column-start: 2;\n  grid-column-end: 3;\n  grid-row-start: 1;\n  grid-row-end: 3;\n  overflow-y: hidden;\n}\n#photos .photos .display .displayPhotoBig img {\n  max-width: 96%;\n  max-height: 98%;\n}\n#photos .photos .display .displayPhoto {\n  overflow-y: hidden;\n  height: 100%;\n  width: 100%;\n}\n#photos .photos .display .displayPhoto img {\n  max-width: 97%;\n  max-height: 97%;\n}\n#photos .photos .display img {\n  max-width: 97%;\n  max-height: 97%;\n  box-shadow: 10px 10px 5px 0px #797aa3;\n  transition: 0.5s ease;\n}\n#photos .photos .display img:hover {\n  transform: scale(0.95, 0.95);\n  box-shadow: 4px 4px 5px 0px #797aa3;\n}\n\n#video .video {\n  background-color: #ffffff;\n  flex-direction: column;\n  flex-flow: wrap row;\n  text-align: center;\n  align-content: center;\n  align-items: center;\n  max-width: 1200px;\n  margin: 0 auto;\n  padding: 100px 0;\n  display: flex;\n}\n#video .video #videoTitle {\n  width: 100vw;\n  font-size: 60px;\n  position: relative;\n  text-align: center;\n  margin-bottom: 10px;\n  margin-top: 20px;\n  font-weight: bold;\n}\n#video .video #videoSubTitle {\n  width: 100vw;\n  font-size: 30px;\n  position: relative;\n  text-align: justify;\n  margin-top: 0px;\n  padding-right: 50px;\n  padding-left: 50px;\n}\n#video .video .player {\n  position: relative;\n  width: 100vw;\n  height: 650px;\n  background-color: #ffffff;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  transition: 0.2s ease;\n  opacity: 0;\n  transition: 1s ease;\n  cursor: pointer;\n}\n#video .video .player video {\n  height: 580px;\n}\n\n#footer {\n  background-image: linear-gradient(60deg, #cae7b9 0%, #ffeda5 100%);\n}\n#footer .footer {\n  min-height: 200px;\n  flex-direction: column;\n  padding-top: 30px;\n  padding-bottom: 10px;\n}\n#footer .footer h2 {\n  color: #000000;\n  font-weight: 500;\n  font-size: 1.8rem;\n  letter-spacing: 0.1rem;\n  margin-top: 10px;\n  margin-bottom: 10px;\n}\n#footer .footer .footer-title {\n  font-family: \"Eastman\";\n  font-size: 2.8rem;\n  font-weight: 300;\n  color: #000000;\n  margin-bottom: 10px;\n  text-transform: uppercase;\n  letter-spacing: 0.2rem;\n  text-align: center;\n}\n#footer .footer .social-icon {\n  display: flex;\n  margin-bottom: 30px;\n}\n#footer .footer .social-icon .social-item {\n  height: 50px;\n  width: 50px;\n  margin: 0 5px;\n}\n#footer .footer .social-icon .social-item img {\n  filter: grayscale(1);\n  transition: 0.3s ease filter;\n}\n#footer .footer .social-icon .social-item img:hover img {\n  filter: grayscale(0);\n}\n#footer .footer p {\n  color: #000000;\n  font-size: 1.3rem;\n}\n\n/*keyframes*/\n@keyframes text_reveal_box {\n  50% {\n    width: 100%;\n    left: 0;\n  }\n  100% {\n    width: 0;\n    left: 100%;\n  }\n}\n@keyframes text_reveal {\n  100% {\n    color: #ffffff;\n  }\n}\n@keyframes text_reveal_name {\n  100% {\n    color: crimson;\n    font-weight: 500;\n  }\n}", "",{"version":3,"sources":["webpack://src/app/pages/HomePage/HomePage.scss"],"names":[],"mappings":"AAAA,gBAAgB;AAAhB;EACI,sBAAA;EACA,UAAA;EACA,SAAA;AAEJ;;AAEA;;CAAA;AAaA;;;;;CAAA;AAMA;EACI,sBAAA;EACA,0DAAA;EACA,gBAAA;EACA,kBAAA;AATJ;AAmBA;EACI,sBAAA;EAEA,uBAAA;EACA,kBAAA;AAlBJ;;AAqBA;EACI,qBAAA;AAlBJ;;AAqBA;EACI,iBAAA;EACA,WAAA;EACA,aAAA;EACA,uBAAA;EACA,mBAAA;AAlBJ;;AAsBI;EACI,sBAAA;EACA,eAAA;EACH,yBAAA;EACA,cArDQ;AAkCb;;AAuBA;EACI,eAAA;EACA,aAAA;EACA,OAAA;EACA,MAAA;EACA,YAAA;EACA,YAAA;AApBJ;AAqBI;EACI,eAAA;EACA,4BAAA;EACA,sCAAA;AAnBR;AAsBI;EACI,aAAA;EACA,mBAAA;EACA,8BAAA;EACA,WAAA;EACA,YAAA;EACA,iBAAA;EACA,eAAA;AApBR;AAqBQ;EACI,eAAA;AAnBZ;AAuBQ;EACI,gBAAA;EACH,iBAAA;EACA,kBAAA;EACA,YAAA;EACA,UAAA;EACA,MAAA;EACA,cAAA;EACA,sBAAA;EACA,uBAAA;EACA,mBAAA;EACA,UAAA;EACA,kBAAA;EACG,0BAAA;AArBZ;AAsBY;EACI,QAAA;AApBhB;AAsBY;EACI,qBAAA;AApBhB;AAsBoB;EACI,cAxGX;AAoFb;AAqBwB;EACI,yCAAA;EACH,uBAAA;AAnBzB;AAuBgB;EACI,eAAA;EACA,gBAAA;EACA,mBAAA;EACA,qBAAA;EACA,cAnHJ;EAoHI,yBAAA;EACA,aAAA;EACA,cAAA;EACA,qBAAA;AArBpB;;AA+BI;EAEI,yBArIG;EAuIH,iBAAA;EACA,cAAA;EACA,UAAA;EACA,kBAAA;EACA,mBAAA;AA9BR;AAgCQ;EACI,aAAA;EACA,kBAAA;EACA,kBAAA;EACA,mBAAA;EACA,kBAAA;EACA,YAAA;EACA,aAAA;EACA,mBAAA;EACA,aAAA;EACA,UAAA;EACA,gBAAA;EACA,mBAAA;AA9BZ;AAkCY;EACI,kBAAA;EACA,eAAA;EACA,WAAA;EACA,YAAA;EACA,cAjKH;EAkKG,kBAAA;AAhChB;AAoCY;EACI,kBAAA;EACA,UAAA;EACA,UAAA;EACA,UAAA;EACA,qBAAA;EAEA,yBA9KL;AA2IX;AAqCgB;EACI,aAAA;EACA,YAAA;AAnCpB;AAoCiB;EACG,wBAAA;AAlCpB;AAuCY;EACI,kBAAA;EACA,UAAA;EACA,UAAA;AArChB;AAuCgB;EACI,aAAA;EACA,YAAA;AArCpB;AA2CQ;EACI,kBAAA;EACA,mBAAA;EACA,gBAAA;EACA,YAAA;EACA,aAAA;EACA,aAAA;EACA,iBAAA;EACA,kBAAA;EACA,WAAA;AAzCZ;AA4CY;EACI,kBAAA;EACA,aAAA;EACA,uBAAA;EACA,YAAA;EACA,UAAA;EACA,oBAAA;EACA,mBAAA;EACA,iBAAA;EACA,eAAA;AA1ChB;AA+CY;EACI,aAAA;EACA,wBAAA;EACA,UAAA;AA7ChB;AA+CgB;EACI,YAAA;EACA,mBAAA;EACA,mBAAA;EACA,UAAA;AA7CpB;AA8CoB;EACI,YAAA;EACA,SAAA;EACA,qBAAA;EACA,4BAAA;AA5CxB;AAiDY;EACI,iBAAA;EACA,eAAA;AA/ChB;AAkDY;EACI,yBAAA;AAhDhB;AAiDgB;EACI,yBAzPJ;AA0MhB;AAkDY;EACI,yBAAA;AAhDhB;AAiDgB;EACI,yBA7PJ;AA8MhB;;AAwDI;EACI,aAAA;EACA,kBAAA;EACA,uBAAA;EACH,iBAAA;EACA,cAAA;EACG,gBAAA;AArDR;AAuDQ;EACI,kBAAA;EACA,kBAAA;EACA,eAAA;EACA,aAAA;EACA,YAAA;EACA,SAAA;EACA,iBAAA;AArDZ;AAwDQ;EACI,YAAA;EACA,YAAA;EACA,aAAA;EACA,uBAAA;EACA,kBAAA;EACA,OAAA;EACA,SAAA;EACA,aAAA;EACA,WAAA;AAtDZ;AAwDY;EACI,aAAA;EACA,2BAAA;EACA,eAAA;EACA,kBAAA;EACA,UAAA;EACA,YAAA;EACA,aAAA;AAtDhB;AAwDgB;EACI,UAAA;EACA,WAAA;EACA,UAAA;EACA,aAAA;EACA,kBAAA;EACA,mBAAA;EACA,uBAAA;EACA,kBAAA;EACA,YAAA;EACA,aAAA;EACA,oCAAA;EACA,yBAvTH;EAwTG,qBAAA;AAtDpB;AAuDoB;EACI,UAAA;EACA,mCAAA;AArDxB;AAwDoB;EACI,yBAAA;EACA,eAAA;AAtDxB;AA0DgB;EACI,UAAA;EACA,MAAA;EACA,aAAA;EACA,kBAAA;EACA,mBAAA;EACA,uBAAA;EACA,UAAA;EACA,YAAA;EACA,kBAAA;EACA,YAAA;EACA,aAAA;EACA,oCAAA;EACA,yBAjVH;EAkVG,qBAAA;EACA,UAAA;AAxDpB;AAyDoB;EACI,UAAA;EACA,mCAAA;AAvDxB;AAyDoB;EACI,yBAAA;EACA,eAAA;AAvDxB;AA4DY;EACI,kBAAA;EACA,cAnWJ;EAoWI,eAAA;EACA,aAAA;EACA,YAAA;EACA,SAAA;EACA,UAAA;EACA,kBAAA;AA1DhB;AA6DY;EACI,kBAAA;EACA,cA9WJ;EA+WI,eAAA;EACA,aAAA;EACA,YAAA;EACA,SAAA;EACA,WAAA;EACA,kBAAA;AA3DhB;;AAoEI;EACI,sBAAA;EACH,iBAAA;EACA,cAAA;EACG,gBAAA;EACA,kBAAA;AAjER;AAkEQ;EACI,kBAAA;AAhEZ;AAiEY;EACI,mBAAA;AA/DhB;AAgEgB;EACI,cAvYJ;AAyUhB;AAkEQ;EACI,eAAA;EACA,iBAAA;EACA,cA/YA;EAgZA,mBAAA;EACA,yBAAA;EACA,sBAAA;EACA,kBAAA;AAhEZ;AAmEQ;EACI,aAAA;EACA,mBAAA;EACA,uBAAA;EACA,sBAAA;AAjEZ;AAkEY;EACI,aAAA;EACA,mBAAA;EACA,uBAAA;EACA,mBAAA;EACA,aAAA;EACA,WAAA;EACA,SAAA;EACA,gBAAA;EACA,gBAAA;AAhEhB;AAiEgB;EACI,2BAAA;AA/DpB;AAiEgB;EACI,aAAA;EACA,eAAA;EACA,YAAA;EACA,aAAA;EACA,uBAAA;EACA,uBAAA;EACA,sBAAA;EACA,kEAAA;EACA,cApbT;AAqXX;AAgEoB;EACI,cAAA;EACA,kBAAA;EACA,eAAA;EACA,gBAAA;EACA,kBAAA;EACA,kBAAA;EACH,4CAAA;EACA,mBAAA;AA9DrB;AA+DwB;EACI,kBAAA;EACA,MAAA;EACA,OAAA;EACA,YAAA;EACA,QAAA;EACA,yBApcjB;EAqciB,sCAAA;EACA,qBAAA;AA7D5B;AAgEoB;EACI,cAAA;EACA,kBAAA;EACA,iBAAA;EACA,gBAAA;EACA,gBAAA;EACA,kBAAA;EACA,kBAAA;EACA,4CAAA;EACH,mBAAA;AA9DrB;AA+DwB;EACI,kBAAA;EACA,MAAA;EACA,OAAA;EACA,YAAA;EACA,QAAA;EACA,yBAzdjB;EA0diB,sCAAA;EACA,qBAAA;AA7D5B;AAgEoB;EACI,cAAA;EACA,kBAAA;EACA,cAjeb;EAkea,iBAAA;EACA,eAAA;EACA,mBAAA;EACA,gBAAA;EACA,uBAAA;EACA,kBAAA;EACH,kBAAA;EACA,4CAAA;EACA,mBAAA;AA9DrB;AA+DwB;EACI,kBAAA;EACA,MAAA;EACA,OAAA;EACA,YAAA;EACA,QAAA;EACA,yBAjfjB;EAkfiB,sCAAA;EACA,qBAAA;AA7D5B;AAiEgB;EACI,eAAA;EACA,YAAA;EACA,gBAAA;EACA,kBAAA;AA/DpB;AAgEoB;EACI,WAAA;EACA,kBAAA;EACA,OAAA;EACA,MAAA;EACA,YAAA;EACA,WAAA;EACA,kEAAA;EACA,YAAA;AA9DxB;AAgEoB;EACI,YAAA;EACA,WAAA;EACA,iBAAA;EACA,+BAAA;AA9DxB;AAiEwB;EACI,qBAAA;AA/D5B;;AA0EI;EACI,yBA1hBG;EA2hBH,kBAAA;EACA,iBAAA;EACA,cAAA;EACA,gBAAA;EACA,aAAA;EACA,uBAAA;EACA,mBAAA;AAvER;AAyEQ;EACI,YAAA;EACA,eAAA;EACA,kBAAA;EACA,kBAAA;EACA,YAAA;EACA,iBAAA;EACA,iBAAA;AAvEZ;AA2EQ;EACI,YAAA;EACA,eAAA;EACA,kBAAA;EACA,iBAAA;EACA,YAAA;EACA,iBAAA;EACA,mBAAA;AAzEZ;AA4EQ;EACI,kBAAA;EAGA,QAAA;EACA,SAAA;EACA,aAAA;EACA,YAAA;EACA,aAAA;EACA,cAAA;EACA,uCAAA;EACA,+BAAA;EACA,qBAAA;EACA,kBAAA;EACA,aAAA;EACA,kBAAA;EACA,UAAA;EACA,qBAAA;AA5EZ;AA+EY;EACI,oBAAA;EACA,kBAAA;EACA,iBAAA;EACA,eAAA;EACA,kBAAA;AA7EhB;AA8EgB;EACI,cAAA;EACA,eAAA;AA5EpB;AAiFY;EAEI,kBAAA;EACA,YAAA;EACA,WAAA;AAhFhB;AAiFgB;EACI,cAAA;EACA,eAAA;AA/EpB;AAmFY;EACI,cAAA;EACA,eAAA;EACA,qCAAA;EACA,qBAAA;AAjFhB;AAkFgB;EACI,4BAAA;EACA,mCAAA;AAhFpB;;AAyFI;EACI,yBArnBG;EAsnBH,sBAAA;EACA,mBAAA;EACA,kBAAA;EACA,qBAAA;EACA,mBAAA;EACA,iBAAA;EACA,cAAA;EACA,gBAAA;EACA,aAAA;AAtFR;AAwFQ;EACI,YAAA;EACA,eAAA;EACA,kBAAA;EACA,kBAAA;EACA,mBAAA;EACA,gBAAA;EACA,iBAAA;AAtFZ;AA0FQ;EACI,YAAA;EACA,eAAA;EACA,kBAAA;EACA,mBAAA;EACA,eAAA;EACA,mBAAA;EACA,kBAAA;AAxFZ;AA6FQ;EACI,kBAAA;EACA,YAAA;EACA,aAAA;EACA,yBA3pBD;EA4pBC,aAAA;EACA,uBAAA;EACA,mBAAA;EACA,qBAAA;EACA,UAAA;EACA,mBAAA;EACA,eAAA;AA3FZ;AA4FY;EACI,aAAA;AA1FhB;;AAgGA;EACI,kEAAA;AA7FJ;AA8FI;EACI,iBAAA;EACA,sBAAA;EACA,iBAAA;EACA,oBAAA;AA5FR;AA6FQ;EACI,cA/qBA;EAgrBA,gBAAA;EACA,iBAAA;EACA,sBAAA;EACA,gBAAA;EACA,mBAAA;AA3FZ;AA8FQ;EACI,sBAAA;EACA,iBAAA;EACA,gBAAA;EACA,cA3rBA;EA4rBA,mBAAA;EACA,yBAAA;EACA,sBAAA;EACA,kBAAA;AA5FZ;AAiGQ;EACI,aAAA;EACA,mBAAA;AA/FZ;AAgGY;EACI,YAAA;EACA,WAAA;EACA,aAAA;AA9FhB;AA+FgB;EACI,oBAAA;EACA,4BAAA;AA7FpB;AA+FwB;EACI,oBAAA;AA7F5B;AAmGQ;EACI,cAvtBA;EAwtBA,iBAAA;AAjGZ;;AAuGA,YAAA;AACA;EACC;IACC,WAAA;IACA,OAAA;EApGA;EAsGD;IACC,QAAA;IACA,UAAA;EApGA;AACF;AAsGA;EACC;IACC,cA9uBS;EA0oBT;AACF;AAsGA;EACC;IACC,cAAA;IACA,gBAAA;EApGA;AACF","sourcesContent":["* {\r\n    box-sizing: border-box;\r\n    padding: 0;\r\n    margin: 0;\r\n    \r\n}\r\n\r\n/*\r\n*Declaración de los colores del documento\r\n*/\r\n$bg-color: #ffffff;\r\n$title-color:#797aa3;\r\n$subtitle-color:#6ba0c8;\r\n$text-color:#000000;\r\n$separtor-color:#cae7b9;\r\n$ODetails-color: #eb9486;\r\n$YDetails-color: #ffeda5;\r\n\r\n\r\n\r\n/*\r\n* Definición de la fuente importada.\r\n* \r\n* Para usarla, copiar y pegar la primera línea en donde se vaya a usar dicha fuente.\r\n* Si se van a importa   r más fuentes, deben ponerse en la carpeta fonts y seguir este mismo proceso.\r\n*/\r\n@font-face {\r\n    font-family: \"Eastman\";\r\n    src: url(\"../../../fonts/EastmanTrial-Black.otf\") format(\"otf\");\r\n    font-weight: 600;\r\n    font-style: normal;\r\n}\r\n\r\n// @font-face {\r\n//     font-family: \"Coolvetica\";\r\n//     src: url(\"../../../fonts/coolvetica\\ crammed\\ rg.ttf\") format(\"ttf\");\r\n//     font-weight: 600;\r\n//     font-style: normal;\r\n// }\r\n\r\nhtml {\r\n    font-family: Helvetica;\r\n    \r\n    scroll-behavior: smooth;\r\n    overflow-x: hidden;\r\n}\r\n\r\na {\r\n    text-decoration: none;\r\n}\r\n\r\n.container {\r\n    min-height: 100vh;\r\n    width: 100%;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n}\r\n\r\n.npage{\r\n    h1{\r\n        font-family:\"Eastman\";\r\n        font-size: 35px;\r\n\t    text-transform: uppercase;\r\n\t    color: $title-color;\r\n    }\r\n}\r\n\r\n#header {\r\n    position: fixed;\r\n    z-index: 1000;\r\n    left: 0;\r\n    top: 0;\r\n    width: 100vw;\r\n    height: auto;\r\n    .header {\r\n        min-height: 8vh;\r\n        background-color: $separtor-color, 0;\r\n        transition: .3s ease background-color;\r\n        // box-shadow: 10px 5px 30px $separtor-color;\r\n    }\r\n    .nav-bar {\r\n        display: flex;\r\n        align-items: center;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n        max-width: 1300px;\r\n        padding: 0 10px;\r\n        .npage{\r\n            cursor: pointer;\r\n        }\r\n    }\r\n    .nav-list{\r\n        ul{\r\n            list-style: none;\r\n\t        position: initial;\r\n\t        width: fit-content;\r\n\t        height: auto;\r\n\t        left: 100%;\r\n\t        top: 0;\r\n\t        display: block;\r\n\t        flex-direction: column;\r\n\t        justify-content: center;\r\n\t        align-items: center;\r\n\t        z-index: 1;\r\n\t        overflow-x: hidden;\r\n            transition: .5s ease left;\r\n            &:active{\r\n                left: 0%;\r\n            }\r\n            li{\r\n                display: inline-block;\r\n                &:hover{\r\n                    a{\r\n                        color: $title-color;\r\n                        &:after{\r\n                            transform: translate(-50%, -50%) scale(1);\r\n\t                        letter-spacing: initial;\r\n                        }\r\n                    }\r\n                }\r\n                a{\r\n                    font-size: 17px;\r\n                    font-weight: 700;\r\n                    letter-spacing: 2px;\r\n                    text-decoration: none;\r\n                    color: $subtitle-color;\r\n                    text-transform: uppercase;\r\n                    padding: 20px;\r\n                    display: block;\r\n                    transition: 0.3s ease;\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n\r\n\r\n#presentation {\r\n\r\n    .presentation {\r\n       \r\n        background-color: $bg-color;\r\n        // background-color: $YDetails-color;\r\n        max-width: 1200px;\r\n        margin: 0 auto;\r\n        padding: 0;\r\n        position: relative;\r\n        flex-flow: wrap row;\r\n        \r\n        .parrafo{\r\n            display: flex;\r\n            margin-right: 50px;\r\n            text-align: center;\r\n            align-items: center;\r\n            position: absolute;\r\n            width: 410px;\r\n            height: 650px;\r\n            flex-flow: wrap row;\r\n            padding: 10px;\r\n            left: 60px;\r\n            margin-top: 10px;\r\n            border-radius: 40px; \r\n            // border: 2px solid blue;\r\n            // background-image: linear-gradient(0deg, #6ba0c880 0%, #cae7b980  100%);\r\n            \r\n            #presentationTitle{\r\n                position: relative;\r\n                font-size: 70px;\r\n                width: 100%;\r\n                height: 50px;\r\n                color: $title-color;\r\n                margin-top: -440px;\r\n            }\r\n            \r\n\r\n            .presentationIconColor{\r\n                position: absolute;\r\n                top: 250px;\r\n                left: 30px;\r\n                opacity: 0;\r\n                transition: 0.5s ease ;\r\n                // transition: 0.3s ease transform;\r\n                background-color: $bg-color;\r\n               \r\n                img{\r\n                    height: 120px;\r\n                    margin: 20px;\r\n                }&:hover{\r\n                    transform: rotate(15deg);\r\n                    \r\n                }\r\n            }\r\n\r\n            .presentationIcon{\r\n                position: absolute;\r\n                top: 250px;\r\n                left: 30px;\r\n               \r\n                img{\r\n                    height: 120px;\r\n                    margin: 20px;\r\n                }\r\n            }\r\n    \r\n        }\r\n\r\n        .skills{\r\n            position: absolute;\r\n            flex-flow: wrap row;\r\n            margin-top: 10px;\r\n            width: 600px;\r\n            height: 500px;\r\n            display: flex;\r\n            text-align: start;\r\n            padding-left: 15px;\r\n            right: 60px;\r\n            // border: 2px solid blue;\r\n\r\n            .skillTitle{\r\n                position: relative;\r\n                display: flex;\r\n                justify-content: center;\r\n                height: 70px;\r\n                width: 90%;\r\n                margin-bottom: -70px;\r\n                border-radius: 30px;\r\n                line-height: 70px;\r\n                font-size: 50px;\r\n                //background-color: $ODetails-color;\r\n            }\r\n    \r\n            \r\n            .prog-lang,.adobe{\r\n                display: flex;\r\n                flex-flow: column nowrap;\r\n                width: 50%;\r\n                              \r\n                .bar{\r\n                    height: 40px;\r\n                    margin: 2px 0 2px 0;\r\n                    border-radius: 15px;\r\n                    width: 80%;\r\n                    .filler{\r\n                        height: 100%;\r\n                        width: 0%;\r\n                        transition: 0.5s ease;\r\n                        border-radius: 12px 0 0 12px;\r\n                    }\r\n                }\r\n            }\r\n\r\n            .skill-title{\r\n                margin: 7% 0 8% 0;\r\n                font-size: 35px;\r\n            }\r\n\r\n            .prog-lang .bar{\r\n                border: 2px $text-color solid;\r\n                .filler{\r\n                    background-color: $subtitle-color;\r\n                }\r\n            }\r\n            .adobe .bar{\r\n                border: 2px $text-color solid;\r\n                .filler{\r\n                    background-color: $separtor-color;\r\n                }\r\n            }\r\n        }\r\n\r\n    }\r\n}\r\n\r\n#biography{\r\n    .biography{\r\n        display: flex;\r\n        position: relative;\r\n        justify-content: center;\r\n\t    max-width: 1200px;\r\n\t    margin: 0 auto;\r\n        padding: 100px 0;\r\n        \r\n        #Titulo_Bio{\r\n            position: absolute;\r\n            text-align: center;\r\n            font-size: 70px;\r\n            bottom: 400px;\r\n            width: 100vw;\r\n            top: 95px;\r\n            font-weight: bold;\r\n        }\r\n\r\n        .Exp_Educ{\r\n            padding: 0px;\r\n            margin: 10px;\r\n            display: flex;\r\n            justify-content: center;\r\n            position: relative;\r\n            left: 0;\r\n            top: 35px;\r\n            height: 750px;\r\n            width: 45vw;\r\n\r\n            #OrganizadorRombos{\r\n                display: flex;\r\n                justify-content: flex-start;\r\n                flex-wrap: wrap;\r\n                position: relative;\r\n                top: 170px;\r\n                width: 600px;\r\n                height: 600px;\r\n\r\n                .RomboSolitario{\r\n                    opacity: 0;\r\n                    left: 200px;\r\n                    top: 200px;\r\n                    display: flex;\r\n                    text-align: center;\r\n                    align-items: center;\r\n                    justify-content: center;\r\n                    position: absolute;\r\n                    width: 165px;\r\n                    height: 165px;\r\n                    transform: rotate(45deg) scale(0,0);\r\n                    background-color: $ODetails-color;\r\n                    transition: 0.5s ease;\r\n                    &:hover{\r\n                        top: 190px;\r\n                        box-shadow: 8px 8px 5px 0px $title-color;\r\n                        \r\n                    }\r\n                    .TextoInfo{\r\n                        transform: rotate(-45deg);\r\n                        font-size: 18px;\r\n                    }\r\n                }\r\n\r\n                .RomboInfo{\r\n                    left: 20px;\r\n                    top: 0;\r\n                    display: flex;\r\n                    text-align: center;\r\n                    align-items: center;\r\n                    justify-content: center;\r\n                    padding: 0;\r\n                    margin: 50px;\r\n                    position: relative;\r\n                    width: 165px;\r\n                    height: 165px;\r\n                    transform: rotate(45deg) scale(0,0);\r\n                    background-color: $ODetails-color;\r\n                    transition: 0.5s ease;\r\n                    opacity: 0;\r\n                    &:hover{\r\n                        top: -10px;\r\n                        box-shadow: 8px 8px 5px 0px $title-color;\r\n                    }\r\n                    .TextoInfo{\r\n                        transform: rotate(-45deg);\r\n                        font-size: 18px;\r\n                    }\r\n                }\r\n            }\r\n\r\n            #Titulo_Exp{\r\n                position: absolute;\r\n                color: $text-color;\r\n                font-size: 70px;\r\n                bottom: 400px;\r\n                width: 100vw;\r\n                top: 60px;\r\n                left: 40px;\r\n                font-weight: unset;\r\n            }\r\n\r\n            #Titulo_Edu{\r\n                position: absolute;\r\n                color: $text-color;\r\n                font-size: 70px;\r\n                bottom: 400px;\r\n                width: 100vw;\r\n                top: 60px;\r\n                left: 110px;\r\n                font-weight: unset;\r\n            }\r\n\r\n        }\r\n\r\n    }\r\n}\r\n\r\n#projects{\r\n    .projects{\r\n        flex-direction: column;\r\n\t    max-width: 1200px;\r\n\t    margin: 0 auto;\r\n        padding: 100px 0;\r\n        position: relative;\r\n        .projects-header{\r\n            position: relative;\r\n            h1{\r\n                margin-bottom: 50px;\r\n                span{\r\n                    color: $separtor-color;\r\n                }\r\n            }\r\n        }\r\n        .projects-title {\r\n            font-size: 70px;\r\n            font-weight: bold;\r\n            color: $text-color;\r\n            margin-bottom: 10px;\r\n            text-transform: uppercase;\r\n            letter-spacing: .2rem;\r\n            text-align: center;\r\n        }\r\n\r\n        .all-projects{\r\n            display: flex;\r\n            align-items: center;\r\n            justify-content: center;\r\n            flex-direction: column;\r\n            .project-item{\r\n                display: flex;\r\n                align-items: center;\r\n                justify-content: center;\r\n                flex-direction: row;\r\n                height: 400px;\r\n                width: 100%;\r\n                margin: 0;\r\n                overflow: hidden;\r\n                border-radius: 0;\r\n                &:nth-child(even){\r\n                    flex-direction: row-reverse;\r\n                }\r\n                .project-info{\r\n                    padding: 30px;\r\n                    flex-basis: 50%;\r\n                    height: 100%;\r\n                    display: flex;\r\n                    align-items: flex-start;\r\n                    justify-content: center;\r\n                    flex-direction: column;\r\n                    background-image: linear-gradient(60deg, $subtitle-color 0%, $title-color 100%);\r\n                    color: $bg-color;\r\n                    .ptitulo{\r\n                        display: block;\r\n                        width: fit-content;\r\n                        font-size: 4rem;\r\n                        font-weight: 500;\r\n                        position: relative;\r\n                        color: transparent;\r\n\t                    /*animation: text_reveal .5s ease forwards;*/\r\n\t                    animation-delay: 1s;\r\n                        .animationP {\r\n                            position: absolute;\r\n                            top: 0;\r\n                            left: 0;\r\n                            height: 100%;\r\n                            width: 0;\r\n                            background-color: $bg-color;\r\n                            /*animation: text_reveal_box 1s ease;*/\r\n                            animation-delay: .5s;\r\n                        }\r\n                    }\r\n                    .psubtitulo{\r\n                        display: block;\r\n                        width: fit-content;\r\n                        font-size: 1.8rem;\r\n                        font-weight: 500;\r\n                        margin-top: 10px;\r\n                        position: relative;\r\n                        color: transparent;\r\n                        /*animation: text_reveal .5s ease forwards;*/\r\n\t                    animation-delay: 1s;\r\n                        .animationP{\r\n                            position: absolute;\r\n                            top: 0;\r\n                            left: 0;\r\n                            height: 100%;\r\n                            width: 0;\r\n                            background-color: $bg-color;\r\n                            /*animation: text_reveal_box 1s ease;*/\r\n                            animation-delay: .5s;\r\n                        }\r\n                    }\r\n                    .ptexto{\r\n                        display: block;\r\n                        width: fit-content;  \r\n                        color: $bg-color;\r\n                        font-size: 1.4rem;\r\n                        margin-top: 5px;\r\n                        line-height: 2.5rem;\r\n                        font-weight: 300;\r\n                        letter-spacing: .05rem;\r\n                        position: relative;\r\n\t                    color: transparent;\r\n\t                    /*animation: text_reveal .5s ease forwards;*/\r\n\t                    animation-delay: 1s;\r\n                        .animationP {\r\n                            position: absolute;\r\n                            top: 0;\r\n                            left: 0;\r\n                            height: 100%;\r\n                            width: 0;\r\n                            background-color: $bg-color;\r\n                            /*animation: text_reveal_box 1s ease;*/\r\n                            animation-delay: .5s;\r\n                        }\r\n                    }\r\n                }\r\n                .project-img{\r\n                    flex-basis: 50%;\r\n                    height: 100%;\r\n                    overflow: hidden;\r\n                    position: relative;\r\n                    &:after{\r\n                        content: '';\r\n                        position: absolute;\r\n                        left: 0;\r\n                        top: 0;\r\n                        height: 100%;\r\n                        width: 100%;\r\n                        background-image: linear-gradient(60deg, #458fe4 0%, #485563 100%);\r\n                        opacity: .4;\r\n                    }\r\n                    img{\r\n                        height: 100%;\r\n                        width: 100%;\r\n                        object-fit: cover;\r\n                        transition: .3s ease transform;\r\n                    }\r\n                    &:hover{\r\n                        img{\r\n                            transform: scale(1.1);\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n\r\n#photos {\r\n    \r\n    .photos{\r\n        background-color: $bg-color;\r\n        text-align: center;\r\n        max-width: 1200px;\r\n        margin: 0 auto;\r\n        padding: 100px 0;\r\n        display: flex;\r\n        justify-content: center;\r\n        flex-flow: wrap row;\r\n       \r\n        #photoTitle{\r\n            width: 100vw;\r\n            font-size: 60px;\r\n            position: relative;\r\n            text-align: center;\r\n            margin: 10px;\r\n            margin-left: 50px;\r\n            font-weight: bold;\r\n                       \r\n        }\r\n\r\n        #photoSubTitle{\r\n            width: 100vw;\r\n            font-size: 30px;\r\n            position: relative;\r\n            text-align: start;\r\n            margin: 10px;\r\n            margin-left: 50px;\r\n            margin-bottom: 30px;\r\n        }\r\n\r\n        .display{\r\n            position: relative;\r\n            // background-color: aqua;\r\n            // top: 40px;\r\n            top:0px;\r\n            left: 0px;\r\n            height: 650px;\r\n            width: 100vw;\r\n            display: grid;\r\n            grid-column: 4;\r\n            grid-template-columns: 350px auto 350px;\r\n            grid-template-rows: 300px 300px;\r\n            align-content: center;\r\n            grid-row-gap: 50px;\r\n            padding: 15px;\r\n            overflow-y: hidden;\r\n            opacity: 0;\r\n            transition: 0.8s ease;\r\n            \r\n\r\n            .displayPhotoBig{\r\n                grid-column-start: 2;\r\n                grid-column-end: 3;\r\n                grid-row-start: 1;\r\n                grid-row-end: 3;\r\n                overflow-y: hidden;\r\n                img{\r\n                    max-width: 96%;\r\n                    max-height: 98%;\r\n                }\r\n\r\n            }\r\n\r\n            .displayPhoto{\r\n                // background-color: blue;\r\n                overflow-y: hidden;\r\n                height: 100%;\r\n                width: 100%;\r\n                img{\r\n                    max-width: 97%;\r\n                    max-height: 97%;\r\n                }\r\n            }\r\n\r\n            img{\r\n                max-width: 97%;\r\n                max-height: 97%;\r\n                box-shadow: 10px 10px 5px 0px $title-color;\r\n                transition: 0.5s ease;\r\n                &:hover{\r\n                    transform: scale(0.95,0.95);\r\n                    box-shadow: 4px 4px 5px 0px $title-color;\r\n                }\r\n            }\r\n        }\r\n    }\r\n}\r\n\r\n#video {\r\n    // background-color: green;\r\n    .video {\r\n        background-color: $bg-color;\r\n        flex-direction: column;\r\n        flex-flow: wrap row;\r\n        text-align: center;\r\n        align-content: center;\r\n        align-items: center;\r\n        max-width: 1200px;\r\n        margin: 0 auto;\r\n        padding: 100px 0;\r\n        display: flex;\r\n\r\n        #videoTitle{\r\n            width: 100vw;\r\n            font-size: 60px;\r\n            position: relative;\r\n            text-align: center;\r\n            margin-bottom: 10px;\r\n            margin-top: 20px;\r\n            font-weight: bold;\r\n           \r\n        }\r\n\r\n        #videoSubTitle{\r\n            width: 100vw;\r\n            font-size: 30px;\r\n            position: relative;\r\n            text-align: justify;\r\n            margin-top: 0px;\r\n            padding-right: 50px;\r\n            padding-left: 50px;\r\n            \r\n        }\r\n\r\n\r\n        .player{\r\n            position: relative;\r\n            width: 100vw;\r\n            height: 650px;\r\n            background-color: $bg-color;\r\n            display: flex;\r\n            justify-content: center;\r\n            align-items: center;\r\n            transition: 0.2s ease;\r\n            opacity: 0;\r\n            transition: 1s ease;\r\n            cursor: pointer;\r\n            video{\r\n                height: 580px;\r\n            }\r\n        }\r\n    }\r\n}\r\n\r\n#footer{\r\n    background-image: linear-gradient(60deg, $separtor-color 0%, $YDetails-color 100%);\r\n    .footer{\r\n        min-height: 200px;\r\n        flex-direction: column;\r\n        padding-top: 30px;\r\n        padding-bottom: 10px;\r\n        h2{\r\n            color:$text-color;\r\n            font-weight: 500;\r\n            font-size: 1.8rem;\r\n            letter-spacing: .1rem;\r\n            margin-top: 10px;\r\n            margin-bottom: 10px;\r\n        }\r\n\r\n        .footer-title {\r\n            font-family: \"Eastman\";\r\n            font-size: 2.8rem;\r\n            font-weight: 300;\r\n            color: $text-color;\r\n            margin-bottom: 10px;\r\n            text-transform: uppercase;\r\n            letter-spacing: .2rem;\r\n            text-align: center;\r\n            \r\n        }\r\n        \r\n\r\n        .social-icon {\r\n            display: flex;\r\n            margin-bottom: 30px;\r\n            .social-item {\r\n                height: 50px;\r\n                width: 50px;\r\n                margin: 0 5px;\r\n                img {\r\n                    filter: grayscale(1);\r\n                    transition: .3s ease filter;\r\n                    &:hover{\r\n                        img{\r\n                            filter: grayscale(0);\r\n                        }\r\n                    }\r\n                }\r\n            }\r\n        }\r\n        p{\r\n            color:$text-color;\r\n            font-size: 1.3rem;\r\n        }\r\n    }\r\n}\r\n\r\n\r\n/*keyframes*/\r\n@keyframes text_reveal_box {\r\n\t50% {\r\n\t\twidth: 100%;\r\n\t\tleft: 0;\r\n\t}\r\n\t100% {\r\n\t\twidth: 0;\r\n\t\tleft: 100%;\r\n\t}\r\n}\r\n@keyframes text_reveal {\r\n\t100% {\r\n\t\tcolor: $bg-color;\r\n\t}\r\n}\r\n@keyframes text_reveal_name {\r\n\t100% {\r\n\t\tcolor: crimson;\r\n\t\tfont-weight: 500;\r\n\t}\r\n}\r\n"],"sourceRoot":""}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/LoadingPage/LoadingPage.scss":
/*!*********************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/LoadingPage/LoadingPage.scss ***!
  \*********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(true);
// Module
___CSS_LOADER_EXPORT___.push([module.i, "@charset \"UTF-8\";\n* {\n  box-sizing: border-box;\n  padding: 0;\n  margin: 0;\n}\n\n/*\n*Declaración de los colores del documento\n*/\n#loading-page {\n  z-index: 1001;\n  display: flex;\n  width: 100vw;\n  height: 100vh;\n  background-color: #ffffff;\n  position: fixed;\n  overflow: hidden;\n  justify-content: center;\n  align-items: center;\n  flex-flow: column nowrap;\n  font-family: Helvetica;\n}\n#loading-page #welcome {\n  color: #797aa3;\n  font-size: 100px;\n}\n#loading-page #cargando {\n  width: 20vh;\n  height: 20vh;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-flow: column nowrap;\n  color: #797aa3;\n  font-size: 30px;\n  color: #7d9db6;\n}", "",{"version":3,"sources":["webpack://src/app/pages/LoadingPage/LoadingPage.scss"],"names":[],"mappings":"AAAA,gBAAgB;AAAhB;EACI,sBAAA;EACA,UAAA;EACA,SAAA;AAEJ;;AACA;;CAAA;AAWA;EACI,aAAA;EACA,aAAA;EACA,YAAA;EACA,aAAA;EACA,yBAbO;EAcP,eAAA;EACA,gBAAA;EACA,uBAAA;EACA,mBAAA;EACA,wBAAA;EACA,sBAAA;AANJ;AAOI;EACI,cApBK;EAqBL,gBAAA;AALR;AAOI;EACI,WAAA;EACA,YAAA;EAEA,aAAA;EACA,uBAAA;EACA,mBAAA;EACA,wBAAA;EACA,cA/BK;EAgCL,eAAA;EACA,cAhCQ;AA0BhB","sourcesContent":["* {\r\n    box-sizing: border-box;\r\n    padding: 0;\r\n    margin: 0;\r\n}\r\n\r\n/*\r\n*Declaración de los colores del documento\r\n*/\r\n$bg-color: #ffffff;\r\n$title-color:#797aa3;\r\n$subtitle-color:#7d9db6;\r\n$text-color:#000000; \r\n$separtor-color:#cae7b9;\r\n$ODetails-color: #eb9486;\r\n$YDetails-color: #f3de8a;\r\n\r\n#loading-page{\r\n    z-index: 1001;\r\n    display: flex;\r\n    width: 100vw;\r\n    height: 100vh;\r\n    background-color: $bg-color;\r\n    position: fixed;\r\n    overflow: hidden;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-flow: column nowrap;\r\n    font-family: Helvetica;\r\n    #welcome{\r\n        color: $title-color;\r\n        font-size: 100px;\r\n    }\r\n    #cargando{\r\n        width: 20vh;\r\n        height: 20vh;\r\n        // background-color: #fff;\r\n        display: flex;\r\n        justify-content: center;\r\n        align-items: center;\r\n        flex-flow: column nowrap;\r\n        color: $title-color;\r\n        font-size: 30px;\r\n        color: $subtitle-color;\r\n    }\r\n}"],"sourceRoot":""}]);
// Exports
/* harmony default export */ __webpack_exports__["default"] = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (useSourceMap) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item, useSourceMap);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join('');
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === 'string') {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, '']];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

function cssWithMappingToString(item, useSourceMap) {
  var content = item[1] || ''; // eslint-disable-next-line prefer-destructuring

  var cssMapping = item[3];

  if (!cssMapping) {
    return content;
  }

  if (useSourceMap && typeof btoa === 'function') {
    var sourceMapping = toComment(cssMapping);
    var sourceURLs = cssMapping.sources.map(function (source) {
      return "/*# sourceURL=".concat(cssMapping.sourceRoot || '').concat(source, " */");
    });
    return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
  }

  return [content].join('\n');
} // Adapted from convert-source-map (MIT)


function toComment(sourceMap) {
  // eslint-disable-next-line no-undef
  var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
  var data = "sourceMappingURL=data:application/json;charset=utf-8;base64,".concat(base64);
  return "/*# ".concat(data, " */");
}

/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/getUrl.js":
/*!********************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/getUrl.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url, options) {
  if (!options) {
    // eslint-disable-next-line no-param-reassign
    options = {};
  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign


  url = url && url.__esModule ? url.default : url;

  if (typeof url !== 'string') {
    return url;
  } // If url is already wrapped in quotes, remove them


  if (/^['"].*['"]$/.test(url)) {
    // eslint-disable-next-line no-param-reassign
    url = url.slice(1, -1);
  }

  if (options.hash) {
    // eslint-disable-next-line no-param-reassign
    url += options.hash;
  } // Should url be wrapped?
  // See https://drafts.csswg.org/css-values-3/#urls


  if (/["'() \t\n]/.test(url) || options.needQuotes) {
    return "\"".concat(url.replace(/"/g, '\\"').replace(/\n/g, '\\n'), "\"");
  }

  return url;
};

/***/ }),

/***/ "./node_modules/html-loader/dist/runtime/getUrl.js":
/*!*********************************************************!*\
  !*** ./node_modules/html-loader/dist/runtime/getUrl.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


module.exports = function (url, options) {
  if (!options) {
    // eslint-disable-next-line no-param-reassign
    options = {};
  } // eslint-disable-next-line no-underscore-dangle, no-param-reassign


  url = url && url.__esModule ? url.default : url;

  if (typeof url !== 'string') {
    return url;
  }

  if (options.hash) {
    // eslint-disable-next-line no-param-reassign
    url += options.hash;
  }

  if (options.maybeNeedQuotes && /[\t\n\f\r "'=<>`]/.test(url)) {
    return "\"".concat(url, "\"");
  }

  return url;
};

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : undefined;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && btoa) {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./src/app/app.ts":
/*!************************!*\
  !*** ./src/app/app.ts ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _pages_HomePage_HomePageController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages/HomePage/HomePageController */ "./src/app/pages/HomePage/HomePageController.ts");
/* harmony import */ var _pages_LoadingPage_LoadingPageController__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages/LoadingPage/LoadingPageController */ "./src/app/pages/LoadingPage/LoadingPageController.ts");


var App = /** @class */ (function () {
    function App() {
        this.mainPages = [
            _pages_LoadingPage_LoadingPageController__WEBPACK_IMPORTED_MODULE_1__["LoadingPageController"],
            _pages_HomePage_HomePageController__WEBPACK_IMPORTED_MODULE_0__["HomePageController"],
        ];
        this.loaded = [];
    }
    /**
     * Función que renderiza las páginas que están en el array de mainPages.
     * Esta función llama a varias funciones dentro de las páginas para, por ejemplo, obtener sus elementos.
     */
    App.prototype.render = function () {
        var _this = this;
        var component = this.component();
        this.mainPages.forEach(function (pageController) {
            var controller = new pageController();
            _this.loaded.push(controller);
            var _a = new pageController().getView(), elId = _a[0], element = _a[1];
            component.appendChild(_this.createPage(elId, element));
        });
        document.body.append(component);
        document.body.onload = function () {
            _this.loaded.forEach(function (controller) {
                if ('start' in controller.component) {
                    controller.component.start();
                }
            });
        };
    };
    /**
     * Función que crea un div dentro de la página. Este div tendrá todo el contenido de la página en su interior.
     *
     * @param id
     * @param element
     *
     * @return HTMLDivElement
     */
    App.prototype.createPage = function (id, element) {
        var page = document.createElement('div');
        page.id = id;
        page.appendChild(element);
        return page;
    };
    /**
     * Función que crea la div #app dentro de la página. Esta es la div principal de toda la aplicación, dentro de ella estarán las páginas creadas.
     *
     * @return HTMLDivElement
     */
    App.prototype.component = function () {
        var element = document.createElement('div');
        element.id = "app";
        return element;
    };
    return App;
}());
var app = new App();
app.render();


/***/ }),

/***/ "./src/app/pages/HomePage sync recursive ^\\.\\/.*$":
/*!**********************************************!*\
  !*** ./src/app/pages/HomePage sync ^\.\/.*$ ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./HomePage": "./src/app/pages/HomePage/HomePage.html",
	"./HomePage.html": "./src/app/pages/HomePage/HomePage.html",
	"./HomePage.scss": "./src/app/pages/HomePage/HomePage.scss",
	"./HomePageController": "./src/app/pages/HomePage/HomePageController.ts",
	"./HomePageController.ts": "./src/app/pages/HomePage/HomePageController.ts",
	"./counter": "./src/app/pages/HomePage/counter.ts",
	"./counter.ts": "./src/app/pages/HomePage/counter.ts"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./src/app/pages/HomePage sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/app/pages/HomePage/HomePage.html":
/*!**********************************************!*\
  !*** ./src/app/pages/HomePage/HomePage.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/html-loader/dist/runtime/getUrl.js */ "./node_modules/html-loader/dist/runtime/getUrl.js");
var ___HTML_LOADER_IMPORT_0___ = __webpack_require__(/*! ../../../assets/audio/Mp3_song.mp3 */ "./src/assets/audio/Mp3_song.mp3");
var ___HTML_LOADER_IMPORT_1___ = __webpack_require__(/*! ../../../assets/audio/Ogg_song.ogg */ "./src/assets/audio/Ogg_song.ogg");
var ___HTML_LOADER_IMPORT_2___ = __webpack_require__(/*! ../../../assets/img/competence2.png */ "./src/assets/img/competence2.png");
var ___HTML_LOADER_IMPORT_3___ = __webpack_require__(/*! ../../../assets/img/study2.png */ "./src/assets/img/study2.png");
var ___HTML_LOADER_IMPORT_4___ = __webpack_require__(/*! ../../../assets/img/3d-desing2.png */ "./src/assets/img/3d-desing2.png");
var ___HTML_LOADER_IMPORT_5___ = __webpack_require__(/*! ../../../assets/img/competence.png */ "./src/assets/img/competence.png");
var ___HTML_LOADER_IMPORT_6___ = __webpack_require__(/*! ../../../assets/img/study.png */ "./src/assets/img/study.png");
var ___HTML_LOADER_IMPORT_7___ = __webpack_require__(/*! ../../../assets/img/3d-design.png */ "./src/assets/img/3d-design.png");
var ___HTML_LOADER_IMPORT_8___ = __webpack_require__(/*! ../../../assets/img/projects1.jpg */ "./src/assets/img/projects1.jpg");
var ___HTML_LOADER_IMPORT_9___ = __webpack_require__(/*! ../../../assets/img/project2.jpg */ "./src/assets/img/project2.jpg");
var ___HTML_LOADER_IMPORT_10___ = __webpack_require__(/*! ../../../assets/img/project3.jpg */ "./src/assets/img/project3.jpg");
var ___HTML_LOADER_IMPORT_11___ = __webpack_require__(/*! ../../../assets/img/photo1.jpg */ "./src/assets/img/photo1.jpg");
var ___HTML_LOADER_IMPORT_12___ = __webpack_require__(/*! ../../../assets/img/photo2.jpg */ "./src/assets/img/photo2.jpg");
var ___HTML_LOADER_IMPORT_13___ = __webpack_require__(/*! ../../../assets/img/photo3.jpeg */ "./src/assets/img/photo3.jpeg");
var ___HTML_LOADER_IMPORT_14___ = __webpack_require__(/*! ../../../assets/img/photo4.jpg */ "./src/assets/img/photo4.jpg");
var ___HTML_LOADER_IMPORT_15___ = __webpack_require__(/*! ../../../assets/img/photo5.jpg */ "./src/assets/img/photo5.jpg");
var ___HTML_LOADER_IMPORT_16___ = __webpack_require__(/*! ../../../assets/video/test.mp4 */ "./src/assets/video/test.mp4");
var ___HTML_LOADER_IMPORT_17___ = __webpack_require__(/*! ../../../assets/img/facebook.svg */ "./src/assets/img/facebook.svg");
var ___HTML_LOADER_IMPORT_18___ = __webpack_require__(/*! ../../../assets/img/instagram.svg */ "./src/assets/img/instagram.svg");
var ___HTML_LOADER_IMPORT_19___ = __webpack_require__(/*! ../../../assets/img/twitter.svg */ "./src/assets/img/twitter.svg");
var ___HTML_LOADER_IMPORT_20___ = __webpack_require__(/*! ../../../assets/img/linkedin.svg */ "./src/assets/img/linkedin.svg");
// Module
var ___HTML_LOADER_REPLACEMENT_0___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_0___);
var ___HTML_LOADER_REPLACEMENT_1___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_1___);
var ___HTML_LOADER_REPLACEMENT_2___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_2___);
var ___HTML_LOADER_REPLACEMENT_3___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_3___);
var ___HTML_LOADER_REPLACEMENT_4___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_4___);
var ___HTML_LOADER_REPLACEMENT_5___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_5___);
var ___HTML_LOADER_REPLACEMENT_6___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_6___);
var ___HTML_LOADER_REPLACEMENT_7___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_7___);
var ___HTML_LOADER_REPLACEMENT_8___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_8___);
var ___HTML_LOADER_REPLACEMENT_9___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_9___);
var ___HTML_LOADER_REPLACEMENT_10___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_10___);
var ___HTML_LOADER_REPLACEMENT_11___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_11___);
var ___HTML_LOADER_REPLACEMENT_12___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_12___);
var ___HTML_LOADER_REPLACEMENT_13___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_13___);
var ___HTML_LOADER_REPLACEMENT_14___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_14___);
var ___HTML_LOADER_REPLACEMENT_15___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_15___);
var ___HTML_LOADER_REPLACEMENT_16___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_16___);
var ___HTML_LOADER_REPLACEMENT_17___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_17___);
var ___HTML_LOADER_REPLACEMENT_18___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_18___);
var ___HTML_LOADER_REPLACEMENT_19___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_19___);
var ___HTML_LOADER_REPLACEMENT_20___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_20___);
var code = "<body>\r\n    <!-- Audios -->\r\n    <audio id=\"Mp3\" src=\"" + ___HTML_LOADER_REPLACEMENT_0___ + "\" loop></audio>\r\n    <audio id=\"Ogg\" src=\"" + ___HTML_LOADER_REPLACEMENT_1___ + "\" loop></audio>\r\n\r\n    <!-- Header -->\r\n    <section id=\"header\">\r\n        <div class=\"header container\">\r\n            <div class=\"nav-bar\">\r\n                <div class=\"npage\">\r\n                    <a href=\"#presentation\">\r\n                        <h1>Portfolio template</h1>\r\n                    </a>\r\n                </div>\r\n                <div class=\"nav-list\">\r\n                    <ul>\r\n                        <li><a href=\"#presentation\" data-after=\"Presentation\">Presentation</a></li>\r\n                        <li><a href=\"#biography\" data-after=\"Biography\">Biography</a></li>\r\n                        <li><a href=\"#projects\" data-after=\"Projects\">Projects</a></li>\r\n                        <li><a href=\"#photos\" data-after=\"Photos\">Photos</a></li>\r\n                        <li><a href=\"#video\" data-after=\"Video\">Video</a></li>\r\n                    </ul>\r\n                </div>\r\n            </div>\r\n    </section>\r\n    <!-- Presentation Section  -->\r\n    <section id=\"presentation\">\r\n        <div class=\"presentation container\">\r\n            \r\n            <div class=\"parrafo\">\r\n                <div id=\"presentationTitle\">Hello! I'm [YourName]</div>\r\n                <div class=\"presentationIcon\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_2___ + "\" alt=\"\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_3___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"presentationIcon\" style=\"top:400px;left:120px\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_4___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"presentationIconColor\">\r\n                <img src=\"" + ___HTML_LOADER_REPLACEMENT_5___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"presentationIconColor\" style=\"left:194px\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_6___ + "\"  alt=\"\">\r\n                </div>\r\n                <div class=\"presentationIconColor\" style=\"top:400px;left:120px\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_7___ + "\" alt=\"\">\r\n                </div>\r\n            </div>\r\n            <div class=\"skills\">\r\n                <div class=\"skillTitle\">SKILLS</div>\r\n                <div class=\"prog-lang\">\r\n                    <div class=\"skill-title\">Programming</div>\r\n                    <div id=\"js\">JavaScript</div>\r\n                    <div id=\"js-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"py\">Python</div>\r\n                    <div id=\"py-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"go\">Go</div>\r\n                    <div id=\"go-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"java\">Java</div>\r\n                    <div id=\"java-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"adobe\">\r\n                    <div class=\"skill-title\">Adobe</div>\r\n                    <div id=\"ps\">Photoshop</div>\r\n                    <div id=\"ps-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"pr\">Premiere</div>\r\n                    <div id=\"pr-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"an\">Animate</div>\r\n                    <div id=\"an-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                    <div id=\"ae\">After Effects</div>\r\n                    <div id=\"ae-bar\" class=\"bar\">\r\n                        <div class=\"filler\"></div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!-- End Presentation Section  -->\r\n    <!-- Biography Section -->\r\n    <section id=\"biography\">\r\n        <div class=\"biography container\">\r\n\r\n            <div id=\"Titulo_Bio\">BIOGRAPHY</div>\r\n\r\n            <!-- Experiencia, educación -->\r\n\r\n            <!-- Experiencia -->\r\n            <div class=\"Exp_Educ\">\r\n\r\n                <h1 id=\"Titulo_Exp\">Job Experience</h1>\r\n\r\n                <div id=\"OrganizadorRombos\">\r\n\r\n                    <div class=\"RomboSolitario\">\r\n                        <span class=\"TextoInfo\">Experience info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\">\r\n                        <span class=\"TextoInfo\">Experience info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\">\r\n                        <span class=\"TextoInfo\">Experience info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\">\r\n                        <span class=\"TextoInfo\">Experience info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\">\r\n                        <span class=\"TextoInfo\">Experience info</span>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n            </div>\r\n\r\n            <!-- Educación -->\r\n            <div class=\"Exp_Educ\">\r\n\r\n                <h1 id=\"Titulo_Edu\">Education</h1>\r\n\r\n                <div id=\"OrganizadorRombos\">\r\n\r\n                    <div class=\"RomboSolitario\" style=\"background-color: #6ba0c8;\">\r\n                        <span class=\"TextoInfo\">Education info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\" style=\"background-color: #6ba0c8;\">\r\n                        <span class=\"TextoInfo\">Education info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\" style=\"background-color: #6ba0c8;\">\r\n                        <span class=\"TextoInfo\">Education info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\" style=\"background-color: #6ba0c8;\">\r\n                        <span class=\"TextoInfo\">Education info</span>\r\n                    </div>\r\n\r\n                    <div class=\"RomboInfo\" style=\"background-color: #6ba0c8;\">\r\n                        <span class=\"TextoInfo\">Education info</span>\r\n                    </div>\r\n\r\n                </div>\r\n\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n    <!-- End Biography Section -->\r\n    <!-- Projects Section -->\r\n    <section id=\"projects\">\r\n        <div class=\"projects container\">\r\n            <div class=\"projects-header\">\r\n                <h1 class=\"projects-title\">Recent <span>Projects</span></h1>\r\n            </div>\r\n            <div class=\"all-projects\">\r\n                <div class=\"project-item\">\r\n                    <div class=\"project-info\">\r\n                        <h1 class=\"ptitulo\">Project 1 <span class=\"animationP\"></span></h1>\r\n                        <h2 class=\"psubtitulo\">Coding is Love <span class=\"animationP\"></span></h2>\r\n                        <p class=\"ptexto\">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad, iusto cupiditate\r\n                            voluptatum\r\n                            impedit unde rem ipsa distinctio illum quae mollitia ut, accusantium eius odio ducimus illo\r\n                            neque atque libero non sunt harum? Ipsum repellat animi, fugit architecto voluptatum odit\r\n                            et! <span class=\"animationP\"></span></p>\r\n                    </div>\r\n                    <div class=\"project-img\">\r\n                        <img src=\"" + ___HTML_LOADER_REPLACEMENT_8___ + "\" alt=\"img\">\r\n                    </div>\r\n                </div>\r\n                <div class=\"project-item\">\r\n                    <div class=\"project-info\">\r\n                        <h1 class=\"ptitulo\">Project 2 <span class=\"animationP\"></span></h1>\r\n                        <h2 class=\"psubtitulo\">Coding is Love <span class=\"animationP\"></span></h2>\r\n                        <p class=\"ptexto\">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad, iusto cupiditate\r\n                            voluptatum\r\n                            impedit unde rem ipsa distinctio illum quae mollitia ut, accusantium eius odio ducimus illo\r\n                            neque atque libero non sunt harum? Ipsum repellat animi, fugit architecto voluptatum odit\r\n                            et! <span class=\"animationP\"></span></p>\r\n                    </div>\r\n                    <div class=\"project-img\">\r\n                        <img src=\"" + ___HTML_LOADER_REPLACEMENT_9___ + "\" alt=\"img\">\r\n                    </div>\r\n                </div>\r\n                <div class=\"project-item\">\r\n                    <div class=\"project-info\">\r\n                        <h1 class=\"ptitulo\">Project 3 <span class=\"animationP\"></span></h1>\r\n                        <h2 class=\"psubtitulo\">Coding is Love <span class=\"animationP\"></span></h2>\r\n                        <p class=\"ptexto\">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ad, iusto cupiditate\r\n                            voluptatum\r\n                            impedit unde rem ipsa distinctio illum quae mollitia ut, accusantium eius odio ducimus illo\r\n                            neque atque libero non sunt harum? Ipsum repellat animi, fugit architecto voluptatum odit\r\n                            et! <span class=\"animationP\"></span></p>\r\n                    </div>\r\n                    <div class=\"project-img\">\r\n                        <img src=\"" + ___HTML_LOADER_REPLACEMENT_10___ + "\" alt=\"img\">\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </section>\r\n    <!-- End Projects Section -->\r\n\r\n\r\n    <!-- Photos Section -->\r\n    <section id=\"photos\">\r\n        <div class=\"photos container\">\r\n            <div id=\"photoTitle\">PHOTOGRAPHIES</div>\r\n            <div id=\"photoSubTitle\">Some of my photos</div>\r\n            <div class=\"display\">\r\n                <div class=\"displayPhoto\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_11___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"displayPhotoBig\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_12___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"displayPhoto\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_13___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"displayPhoto\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_14___ + "\" alt=\"\">\r\n                </div>\r\n                <div class=\"displayPhoto\">\r\n                    <img src=\"" + ___HTML_LOADER_REPLACEMENT_15___ + "\" alt=\"\">\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n    <!-- End Photos Section -->\r\n\r\n\r\n    <!-- Video Section -->\r\n    <section id=\"video\">\r\n        <div class=\"video container\">\r\n            <div id=\"videoTitle\">VIDEO</div>\r\n            <div id=\"videoSubTitle\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor\r\n                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis </div>\r\n            <div class=\"player\">\r\n                <!-- <div id=\"btnVideoPrev\">&lt</div> -->\r\n                <video id=\"videoPlayer\" src=\"" + ___HTML_LOADER_REPLACEMENT_16___ + "\" controls></video>\r\n                <!-- <div id=\"btnVideoNext\">&gt</div> -->\r\n            </div>\r\n\r\n        </div>\r\n    </section>\r\n    <!-- End Video Section -->\r\n\r\n\r\n\r\n\r\n\r\n    <!-- Footer -->\r\n    <section id=\"footer\">\r\n        <div class=\"footer container\">\r\n            <h1 class=\"footer-title\">Portfolio <span>template</span></h1>\r\n            <h2>Your Complete Web Solution</h2>\r\n            <div class=\"social-icon\">\r\n                <div class=\"social-item\">\r\n                    <a href=\"#\"><img src=\"" + ___HTML_LOADER_REPLACEMENT_17___ + "\" /></a>\r\n                </div>\r\n                <div class=\"social-item\">\r\n                    <a href=\"#\"><img src=\"" + ___HTML_LOADER_REPLACEMENT_18___ + "\" /></a>\r\n                </div>\r\n                <div class=\"social-item\">\r\n                    <a href=\"#\"><img src=\"" + ___HTML_LOADER_REPLACEMENT_19___ + "\" /></a>\r\n                </div>\r\n                <div class=\"social-item\">\r\n                    <a href=\"#\"><img src=\"" + ___HTML_LOADER_REPLACEMENT_20___ + "\" /></a>\r\n                </div>\r\n            </div>\r\n            <p>Copyright © 2020. All rights reserved</p>\r\n        </div>\r\n    </section>\r\n    <!-- End Footer -->\r\n</body>";
// Exports
module.exports = code;

/***/ }),

/***/ "./src/app/pages/HomePage/HomePage.scss":
/*!**********************************************!*\
  !*** ./src/app/pages/HomePage/HomePage.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!../../../../node_modules/sass-loader/dist/cjs.js!./HomePage.scss */ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/HomePage/HomePage.scss");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./src/app/pages/HomePage/HomePageController.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/HomePage/HomePageController.ts ***!
  \******************************************************/
/*! exports provided: HomePageController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageController", function() { return HomePageController; });
var HomePageController = /** @class */ (function () {
    function HomePageController() {
        var _this = this;
        this.component = {
            id: 'home-page',
            view: 'HomePage.html',
            style: 'HomePage.scss',
            start: function () { _this.start(); }
        };
        this.loadView();
    }
    /**
     * Función que inicializa las animaciones de carga. Se llama desde el app.ts cuando se carga el body.
     */
    HomePageController.prototype.start = function () {
        var _this = this;
        window.setTimeout(function () {
            _this.fillSkillBars();
            _this.animateIcons();
        }, 5000);
        this.scrollEvents();
    };
    /**
     * Función que carga la vista (el html) de la página de manera dinámica según su atributo de component.
     */
    HomePageController.prototype.loadView = function () {
        __webpack_require__("./src/app/pages/HomePage sync recursive ^\\.\\/.*$")("./" + this.component.style);
        this.view = __webpack_require__("./src/app/pages/HomePage sync recursive ^\\.\\/.*$")("./" + this.component.view);
    };
    /**
     * Función que obtiene el id del componente y su fragmento de documento html y los retorna al app.ts para que puedan ser renderizados.
     *
     * @returns [string, DocumentFragment]
     */
    HomePageController.prototype.getView = function () {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    };
    /**
     * Función que se encarga de los eventos de scroll, llamando las funciones específicas para cada evento según la posición.
     */
    HomePageController.prototype.scrollEvents = function () {
        var _this = this;
        window.onscroll = function (_) {
            var scroll_position = window.scrollY;
            console.log(scroll_position);
            // Play y Pause audios
            if (scroll_position < 70) {
                _this.pauseAudio();
            }
            // rellenar y vaciar barras
            if (scroll_position >= 0 && scroll_position <= 450) {
                _this.fillSkillBars();
                _this.animateIcons();
            }
            else {
                _this.emptySkillBars();
                _this.animateIcons(true);
            }
            if (scroll_position >= 70 && scroll_position < 1560) { //Animaciones del header
                _this.animateHeader();
                if (scroll_position > 450 && scroll_position < 1450) { //Animaciones de la sección de biografía
                    _this.playAudio();
                    _this.animateBiography();
                }
                else {
                    _this.animateBiography(true);
                }
            }
            else {
                _this.animateHeader(true);
            }
            if (scroll_position > 1450 && scroll_position < 2938) { //Animaciones de la sección Projects
                _this.animateProjectSection();
            }
            else {
                _this.animateProjectSection(true);
            }
            if (scroll_position >= 3000 && scroll_position < 3900) {
                _this.animatePhotos();
            }
            else if (scroll_position < 3000) {
                _this.animatePhotos(true);
            }
            else {
                _this.animatePhotos(true, true);
            }
            if (scroll_position >= 4100) {
                _this.animatePlayer();
            }
            else {
                _this.animatePlayer(true);
            }
            if (scroll_position >= 1560 && scroll_position < 3110) {
                _this.animateHeader();
                _this.playAudio(false);
            }
            if (scroll_position >= 3110) {
                _this.animateHeader();
                _this.pauseAudio();
            }
        };
    };
    /**
     * Función que anima el header de la página. El parámetro de entrada define si la animación es de entrada o de salida.
     *
     * @param out
     */
    HomePageController.prototype.animateHeader = function (out) {
        if (out === void 0) { out = false; }
        var header = this.findInsideMe(".header");
        if (!out) {
            header.style.backgroundColor = '#cae7b9';
        }
        else {
            header.style.backgroundColor = 'transparent';
        }
    };
    /**
     * Función que anima los íconos de la sección de presentación. El parámetro de entrada define si la animación es de entrada o de salida.
     *
     * @param out
     */
    HomePageController.prototype.animateIcons = function (out) {
        if (out === void 0) { out = false; }
        var iconOpacity = this.findInsideMe(".presentationIconColor", true);
        if (!out) {
            for (var i = 0; i < iconOpacity.length; i++) {
                iconOpacity[i].style.opacity = 1;
            }
        }
        else {
            for (var i = 0; i < iconOpacity.length; i++) {
                iconOpacity[i].style.opacity = 0;
            }
        }
    };
    /**
     * Función que anima la sección de proyectos. El parámetro de entrada define si la animación es de entrada o de salida.
     *
     * @param out
     */
    HomePageController.prototype.animateProjectSection = function (out) {
        if (out === void 0) { out = false; }
        var tituloProject = this.findInsideMe('.ptitulo', true);
        var subtitleProject = this.findInsideMe('.psubtitulo', true);
        var textProject = this.findInsideMe('.ptexto', true);
        var animaProject = this.findInsideMe('.animationP', true);
        if (!out) {
            for (var i = 0; i < tituloProject.length; i++) {
                tituloProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (var i = 0; i < subtitleProject.length; i++) {
                subtitleProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (var i = 0; i < textProject.length; i++) {
                textProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (var i = 0; i < animaProject.length; i++) {
                animaProject[i].style.animation = 'text_reveal_box 1s ease';
            }
        }
        else {
            for (var i = 0; i < tituloProject.length; i++) {
                tituloProject[i].style.animation = '';
            }
            for (var i = 0; i < subtitleProject.length; i++) {
                subtitleProject[i].style.animation = '';
            }
            for (var i = 0; i < textProject.length; i++) {
                textProject[i].style.animation = '';
            }
            for (var i = 0; i < animaProject.length; i++) {
                animaProject[i].style.animation = '';
            }
        }
    };
    /**
     * Función que anima los rombos de la sección de Biografía. El parámetro de entrada define si la animación es de entrada o salida.
     *
     * @param out
     */
    HomePageController.prototype.animateBiography = function (out) {
        if (out === void 0) { out = false; }
        var romboInfo = this.findInsideMe('.RomboInfo', true);
        var romboSolitario = this.findInsideMe('.RomboSolitario', true);
        if (!out) {
            for (var i = 0; i < romboInfo.length; i++) {
                romboInfo[i].style.opacity = 1;
                romboInfo[i].style.transform = 'rotate(45deg) scale(1,1)';
            }
            for (var i = 0; i < romboSolitario.length; i++) {
                romboSolitario[i].style.opacity = 1;
                romboSolitario[i].style.transform = 'rotate(45deg) scale(1,1)';
            }
        }
        else {
            for (var i = 0; i < romboInfo.length; i++) {
                romboInfo[i].style.opacity = 0;
                romboInfo[i].style.transform = 'rotate(45deg) scale(0,0)';
            }
            for (var i = 0; i < romboSolitario.length; i++) {
                romboSolitario[i].style.opacity = 0;
                romboSolitario[i].style.transform = 'rotate(45deg) scale(0,0)';
            }
        }
    };
    /**
     * Función anima que el grid que muestra las fotos en la sección Photos.
     * El primer parámetro de entrada define si la animación es de entrada o salida.
     * El segundo parametro de entrada define si el scroll está arriba o abajo de la sección para
     * que dependiendo de esta se vaya a la derecha o a la izquierda.
     *
     * @param out
     * @param afert
     */
    HomePageController.prototype.animatePhotos = function (out, after) {
        if (out === void 0) { out = false; }
        if (after === void 0) { after = false; }
        var display = this.findInsideMe('.display', false);
        if (!out) {
            display.style.opacity = 1;
            display.style.left = "0px";
        }
        else if (!after) {
            display.style.opacity = 0;
            display.style.left = "900px";
        }
        else {
            display.style.opacity = 0;
            display.style.left = "-500px";
        }
    };
    /**
     * Función que anima la opacidad del vídeo. El parámetro de entrada define si la animación es de entrada o salida.
     *
     * @param out
     */
    HomePageController.prototype.animatePlayer = function (out) {
        if (out === void 0) { out = false; }
        var player = this.findInsideMe('.player', false);
        if (!out) {
            player.style.opacity = 1;
        }
        else {
            player.style.opacity = 0;
            // romboSolitario[i].style.transform = 'rotate(45deg) scale(0,0)';
        }
    };
    /**
     * Función que controla la reproducción de los audios. El parámetro de entrada define si se debe reproducir el mp3 o el ogg.
     *
     * @param mp3
     */
    HomePageController.prototype.playAudio = function (mp3) {
        if (mp3 === void 0) { mp3 = true; }
        var Mp3 = this.findInsideMe('#Mp3');
        var Ogg = this.findInsideMe('#Ogg');
        if (mp3) {
            Mp3.play().then().catch(function (err) { return console.log("Error al reproducir el archivo: " + err); });
            Mp3.volume = 0.15;
            Ogg.pause();
        }
        else {
            Mp3.pause();
            Ogg.play().then().catch(function (err) { return console.log("Error al reproducir el archivo: " + err); });
            Ogg.volume = 0.15;
        }
    };
    /**
     * Función que pausa todos los audios.
     */
    HomePageController.prototype.pauseAudio = function () {
        var Mp3 = this.findInsideMe('#Mp3');
        var Ogg = this.findInsideMe('#Ogg');
        Ogg.pause();
        Mp3.pause();
    };
    /**
     * Función que se encarga de llenar las barras de habilidades.
     */
    HomePageController.prototype.fillSkillBars = function () {
        var bars = this.findInsideMe('.filler', true);
        bars[0].style.width = '75%';
        bars[1].style.width = '40%';
        bars[2].style.width = '60%';
        bars[3].style.width = '80%';
        bars[4].style.width = '45%';
        bars[5].style.width = '70%';
        bars[6].style.width = '45%';
        bars[7].style.width = '85%';
    };
    /**
     * Función que se encarga de vaciar las barras de habilidades.
     */
    HomePageController.prototype.emptySkillBars = function () {
        var bars = this.findInsideMe('.filler', true);
        for (var i = 0; i < bars.length; i++)
            bars[i].style.width = '0%';
    };
    /**
     * Función que encuentra un elemento HTML dentro del documento según el selector que le entra como parámetro. El parámetro all define si
     * se busca por id o por clase.
     *
     * @param selector
     * @param all
     */
    HomePageController.prototype.findInsideMe = function (selector, all) {
        if (all === void 0) { all = false; }
        var query = "#app #" + this.component.id + " " + selector;
        if (!all) {
            return document.querySelector(query);
        }
        else {
            return document.querySelectorAll(query);
        }
    };
    return HomePageController;
}());



/***/ }),

/***/ "./src/app/pages/HomePage/counter.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/HomePage/counter.ts ***!
  \*******************************************/
/*! exports provided: count */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "count", function() { return count; });
function count(counterEl) {
    counterEl.innerHTML = "" + ((+counterEl.innerHTML) + 1);
}


/***/ }),

/***/ "./src/app/pages/LoadingPage sync recursive ^\\.\\/.*$":
/*!*************************************************!*\
  !*** ./src/app/pages/LoadingPage sync ^\.\/.*$ ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./LoadingPage": "./src/app/pages/LoadingPage/LoadingPage.html",
	"./LoadingPage.html": "./src/app/pages/LoadingPage/LoadingPage.html",
	"./LoadingPage.scss": "./src/app/pages/LoadingPage/LoadingPage.scss",
	"./LoadingPageController": "./src/app/pages/LoadingPage/LoadingPageController.ts",
	"./LoadingPageController.ts": "./src/app/pages/LoadingPage/LoadingPageController.ts"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./src/app/pages/LoadingPage sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/app/pages/LoadingPage/LoadingPage.html":
/*!****************************************************!*\
  !*** ./src/app/pages/LoadingPage/LoadingPage.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___ = __webpack_require__(/*! ../../../../node_modules/html-loader/dist/runtime/getUrl.js */ "./node_modules/html-loader/dist/runtime/getUrl.js");
var ___HTML_LOADER_IMPORT_0___ = __webpack_require__(/*! ../../../assets/img/loading.gif */ "./src/assets/img/loading.gif");
// Module
var ___HTML_LOADER_REPLACEMENT_0___ = ___HTML_LOADER_GET_SOURCE_FROM_IMPORT___(___HTML_LOADER_IMPORT_0___);
var code = "<h1 id=\"welcome\">\r\n    Welcome!\r\n</h1>\r\n<div id=\"cargando\">\r\n    <img src=\"" + ___HTML_LOADER_REPLACEMENT_0___ + "\" alt=\"\">\r\n    Loading...\r\n</div>";
// Exports
module.exports = code;

/***/ }),

/***/ "./src/app/pages/LoadingPage/LoadingPage.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/LoadingPage/LoadingPage.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var api = __webpack_require__(/*! ../../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
            var content = __webpack_require__(/*! !../../../../node_modules/css-loader/dist/cjs.js!../../../../node_modules/sass-loader/dist/cjs.js!./LoadingPage.scss */ "./node_modules/css-loader/dist/cjs.js!./node_modules/sass-loader/dist/cjs.js!./src/app/pages/LoadingPage/LoadingPage.scss");

            content = content.__esModule ? content.default : content;

            if (typeof content === 'string') {
              content = [[module.i, content, '']];
            }

var options = {};

options.insert = "head";
options.singleton = false;

var update = api(content, options);



module.exports = content.locals || {};

/***/ }),

/***/ "./src/app/pages/LoadingPage/LoadingPageController.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/LoadingPage/LoadingPageController.ts ***!
  \************************************************************/
/*! exports provided: LoadingPageController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingPageController", function() { return LoadingPageController; });
var LoadingPageController = /** @class */ (function () {
    function LoadingPageController() {
        var _this = this;
        this.component = {
            id: 'loading-page',
            view: 'LoadingPage.html',
            style: 'LoadingPage.scss',
            start: function () { _this.start(); }
        };
        this.loadView();
    }
    /**
     * Función que inicializa las animaciones de carga. Se llama desde el app.ts cuando se carga el body.
     */
    LoadingPageController.prototype.start = function () {
        var _this = this;
        /**
         * Función que controla la desaparición de la pantalla de carga. Está puesta para que a los 5 segundos, esta pantalla desaparezca.
         */
        window.setTimeout(function () {
            var pantalla = document.getElementById("" + _this.component.id);
            pantalla.style.display = 'none';
        }, 5000);
    };
    /**
     * Función que carga la vista (el html) de la página de manera dinámica según su atributo de component.
     */
    LoadingPageController.prototype.loadView = function () {
        __webpack_require__("./src/app/pages/LoadingPage sync recursive ^\\.\\/.*$")("./" + this.component.style);
        this.view = __webpack_require__("./src/app/pages/LoadingPage sync recursive ^\\.\\/.*$")("./" + this.component.view);
    };
    /**
     * Función que obtiene el id del componente y su fragmento de documento html y los retorna al app.ts para que puedan ser renderizados.
     *
     * @returns [string, DocumentFragment]
     */
    LoadingPageController.prototype.getView = function () {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    };
    /**
     * Función que encuentra un elemento HTML dentro del documento según el selector que le entra como parámetro. El parámetro all define si
     * se busca por id o por clase.
     *
     * @param selector
     * @param all
     */
    LoadingPageController.prototype.findInsideMe = function (selector, all) {
        if (all === void 0) { all = false; }
        var query = "#app #" + this.component.id + " " + selector;
        if (!all) {
            return document.querySelector(query);
        }
        else {
            return document.querySelectorAll(query);
        }
    };
    return LoadingPageController;
}());



/***/ }),

/***/ "./src/assets/audio/Mp3_song.mp3":
/*!***************************************!*\
  !*** ./src/assets/audio/Mp3_song.mp3 ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/audio/Mp3_song.mp3");

/***/ }),

/***/ "./src/assets/audio/Ogg_song.ogg":
/*!***************************************!*\
  !*** ./src/assets/audio/Ogg_song.ogg ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/audio/Ogg_song.ogg");

/***/ }),

/***/ "./src/assets/img/3d-design.png":
/*!**************************************!*\
  !*** ./src/assets/img/3d-design.png ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/3d-design.png");

/***/ }),

/***/ "./src/assets/img/3d-desing2.png":
/*!***************************************!*\
  !*** ./src/assets/img/3d-desing2.png ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/3d-desing2.png");

/***/ }),

/***/ "./src/assets/img/competence.png":
/*!***************************************!*\
  !*** ./src/assets/img/competence.png ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/competence.png");

/***/ }),

/***/ "./src/assets/img/competence2.png":
/*!****************************************!*\
  !*** ./src/assets/img/competence2.png ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/competence2.png");

/***/ }),

/***/ "./src/assets/img/facebook.svg":
/*!*************************************!*\
  !*** ./src/assets/img/facebook.svg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/facebook.svg");

/***/ }),

/***/ "./src/assets/img/instagram.svg":
/*!**************************************!*\
  !*** ./src/assets/img/instagram.svg ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/instagram.svg");

/***/ }),

/***/ "./src/assets/img/linkedin.svg":
/*!*************************************!*\
  !*** ./src/assets/img/linkedin.svg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/linkedin.svg");

/***/ }),

/***/ "./src/assets/img/loading.gif":
/*!************************************!*\
  !*** ./src/assets/img/loading.gif ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/loading.gif");

/***/ }),

/***/ "./src/assets/img/photo1.jpg":
/*!***********************************!*\
  !*** ./src/assets/img/photo1.jpg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/photo1.jpg");

/***/ }),

/***/ "./src/assets/img/photo2.jpg":
/*!***********************************!*\
  !*** ./src/assets/img/photo2.jpg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/photo2.jpg");

/***/ }),

/***/ "./src/assets/img/photo3.jpeg":
/*!************************************!*\
  !*** ./src/assets/img/photo3.jpeg ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/photo3.jpeg");

/***/ }),

/***/ "./src/assets/img/photo4.jpg":
/*!***********************************!*\
  !*** ./src/assets/img/photo4.jpg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/photo4.jpg");

/***/ }),

/***/ "./src/assets/img/photo5.jpg":
/*!***********************************!*\
  !*** ./src/assets/img/photo5.jpg ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/photo5.jpg");

/***/ }),

/***/ "./src/assets/img/project2.jpg":
/*!*************************************!*\
  !*** ./src/assets/img/project2.jpg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/project2.jpg");

/***/ }),

/***/ "./src/assets/img/project3.jpg":
/*!*************************************!*\
  !*** ./src/assets/img/project3.jpg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/project3.jpg");

/***/ }),

/***/ "./src/assets/img/projects1.jpg":
/*!**************************************!*\
  !*** ./src/assets/img/projects1.jpg ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/projects1.jpg");

/***/ }),

/***/ "./src/assets/img/study.png":
/*!**********************************!*\
  !*** ./src/assets/img/study.png ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/study.png");

/***/ }),

/***/ "./src/assets/img/study2.png":
/*!***********************************!*\
  !*** ./src/assets/img/study2.png ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/study2.png");

/***/ }),

/***/ "./src/assets/img/twitter.svg":
/*!************************************!*\
  !*** ./src/assets/img/twitter.svg ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/img/twitter.svg");

/***/ }),

/***/ "./src/assets/video/test.mp4":
/*!***********************************!*\
  !*** ./src/assets/video/test.mp4 ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("assets/video/test.mp4");

/***/ }),

/***/ "./src/fonts/EastmanTrial-Black.otf":
/*!******************************************!*\
  !*** ./src/fonts/EastmanTrial-Black.otf ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("fonts/EastmanTrial-Black.otf");

/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ./app/app */ "./src/app/app.ts");


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2FwcC9wYWdlcy9Ib21lUGFnZS9Ib21lUGFnZS5zY3NzIiwid2VicGFjazovLy8uL3NyYy9hcHAvcGFnZXMvTG9hZGluZ1BhZ2UvTG9hZGluZ1BhZ2Uuc2NzcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9nZXRVcmwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2h0bWwtbG9hZGVyL2Rpc3QvcnVudGltZS9nZXRVcmwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzIiwid2VicGFjazovLy8uL3NyYy9hcHAvYXBwLnRzIiwid2VicGFjazovLy8uL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2Ugc3luYyBeXFwuXFwvLiokIiwid2VicGFjazovLy8uL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2UvSG9tZVBhZ2UuaHRtbCIsIndlYnBhY2s6Ly8vLi9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL0hvbWVQYWdlLnNjc3M/YzRmNiIsIndlYnBhY2s6Ly8vLi9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL0hvbWVQYWdlQ29udHJvbGxlci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL2NvdW50ZXIudHMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2FwcC9wYWdlcy9Mb2FkaW5nUGFnZSBzeW5jIF5cXC5cXC8uKiQiLCJ3ZWJwYWNrOi8vLy4vc3JjL2FwcC9wYWdlcy9Mb2FkaW5nUGFnZS9Mb2FkaW5nUGFnZS5odG1sIiwid2VicGFjazovLy8uL3NyYy9hcHAvcGFnZXMvTG9hZGluZ1BhZ2UvTG9hZGluZ1BhZ2Uuc2Nzcz9iYjY5Iiwid2VicGFjazovLy8uL3NyYy9hcHAvcGFnZXMvTG9hZGluZ1BhZ2UvTG9hZGluZ1BhZ2VDb250cm9sbGVyLnRzIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvYXVkaW8vTXAzX3NvbmcubXAzIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvYXVkaW8vT2dnX3Nvbmcub2dnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nLzNkLWRlc2lnbi5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvM2QtZGVzaW5nMi5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvY29tcGV0ZW5jZS5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvY29tcGV0ZW5jZTIucG5nIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nL2ZhY2Vib29rLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9pbnN0YWdyYW0uc3ZnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nL2xpbmtlZGluLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9sb2FkaW5nLmdpZiIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9waG90bzEuanBnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nL3Bob3RvMi5qcGciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvcGhvdG8zLmpwZWciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvcGhvdG80LmpwZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9waG90bzUuanBnIiwid2VicGFjazovLy8uL3NyYy9hc3NldHMvaW1nL3Byb2plY3QyLmpwZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9wcm9qZWN0My5qcGciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvcHJvamVjdHMxLmpwZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy9zdHVkeS5wbmciLCJ3ZWJwYWNrOi8vLy4vc3JjL2Fzc2V0cy9pbWcvc3R1ZHkyLnBuZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL2ltZy90d2l0dGVyLnN2ZyIsIndlYnBhY2s6Ly8vLi9zcmMvYXNzZXRzL3ZpZGVvL3Rlc3QubXA0Iiwid2VicGFjazovLy8uL3NyYy9mb250cy9FYXN0bWFuVHJpYWwtQmxhY2sub3RmIiwid2VicGFjazovLy8uL3NyYy9tYWluLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDa0c7QUFDTztBQUN2QjtBQUNsRiw4QkFBOEIsbUZBQTJCO0FBQ3pELHlDQUF5QyxzRkFBK0IsQ0FBQyxxRUFBNkI7QUFDdEc7QUFDQSw4QkFBOEIsUUFBUyxzQkFBc0IsS0FBSywyQkFBMkIsZUFBZSxjQUFjLEdBQUcsa1RBQWtULDZCQUE2Qix5RUFBeUUscUJBQXFCLHVCQUF1QixHQUFHLFFBQVEsMkJBQTJCLDRCQUE0Qix1QkFBdUIsR0FBRyxPQUFPLDBCQUEwQixHQUFHLGdCQUFnQixzQkFBc0IsZ0JBQWdCLGtCQUFrQiw0QkFBNEIsd0JBQXdCLEdBQUcsZUFBZSw2QkFBNkIsb0JBQW9CLDhCQUE4QixtQkFBbUIsR0FBRyxhQUFhLG9CQUFvQixrQkFBa0IsWUFBWSxXQUFXLGlCQUFpQixpQkFBaUIsR0FBRyxtQkFBbUIsb0JBQW9CLGlDQUFpQywyQ0FBMkMsR0FBRyxvQkFBb0Isa0JBQWtCLHdCQUF3QixtQ0FBbUMsZ0JBQWdCLGlCQUFpQixzQkFBc0Isb0JBQW9CLEdBQUcsMkJBQTJCLG9CQUFvQixHQUFHLHdCQUF3QixxQkFBcUIsc0JBQXNCLHVCQUF1QixpQkFBaUIsZUFBZSxXQUFXLG1CQUFtQiwyQkFBMkIsNEJBQTRCLHdCQUF3QixlQUFlLHVCQUF1QiwrQkFBK0IsR0FBRywrQkFBK0IsYUFBYSxHQUFHLDJCQUEyQiwwQkFBMEIsR0FBRyxtQ0FBbUMsbUJBQW1CLEdBQUcseUNBQXlDLDhDQUE4Qyw0QkFBNEIsR0FBRyw2QkFBNkIsb0JBQW9CLHFCQUFxQix3QkFBd0IsMEJBQTBCLG1CQUFtQiw4QkFBOEIsa0JBQWtCLG1CQUFtQiwwQkFBMEIsR0FBRyxpQ0FBaUMsOEJBQThCLHNCQUFzQixtQkFBbUIsZUFBZSx1QkFBdUIsd0JBQXdCLEdBQUcsd0NBQXdDLGtCQUFrQix1QkFBdUIsdUJBQXVCLHdCQUF3Qix1QkFBdUIsaUJBQWlCLGtCQUFrQix3QkFBd0Isa0JBQWtCLGVBQWUscUJBQXFCLHdCQUF3QixHQUFHLDJEQUEyRCx1QkFBdUIsb0JBQW9CLGdCQUFnQixpQkFBaUIsbUJBQW1CLHVCQUF1QixHQUFHLCtEQUErRCx1QkFBdUIsZUFBZSxlQUFlLGVBQWUsMEJBQTBCLDhCQUE4QixHQUFHLG1FQUFtRSxrQkFBa0IsaUJBQWlCLEdBQUcscUVBQXFFLDZCQUE2QixHQUFHLDBEQUEwRCx1QkFBdUIsZUFBZSxlQUFlLEdBQUcsOERBQThELGtCQUFrQixpQkFBaUIsR0FBRyx1Q0FBdUMsdUJBQXVCLHdCQUF3QixxQkFBcUIsaUJBQWlCLGtCQUFrQixrQkFBa0Isc0JBQXNCLHVCQUF1QixnQkFBZ0IsR0FBRyxtREFBbUQsdUJBQXVCLGtCQUFrQiw0QkFBNEIsaUJBQWlCLGVBQWUseUJBQXlCLHdCQUF3QixzQkFBc0Isb0JBQW9CLEdBQUcsOEZBQThGLGtCQUFrQiw2QkFBNkIsZUFBZSxHQUFHLHdHQUF3RyxpQkFBaUIsd0JBQXdCLHdCQUF3QixlQUFlLEdBQUcsd0hBQXdILGlCQUFpQixjQUFjLDBCQUEwQixpQ0FBaUMsR0FBRyxvREFBb0Qsc0JBQXNCLG9CQUFvQixHQUFHLHVEQUF1RCw4QkFBOEIsR0FBRywrREFBK0QsOEJBQThCLEdBQUcsbURBQW1ELDhCQUE4QixHQUFHLDJEQUEyRCw4QkFBOEIsR0FBRywyQkFBMkIsa0JBQWtCLHVCQUF1Qiw0QkFBNEIsc0JBQXNCLG1CQUFtQixxQkFBcUIsR0FBRyxxQ0FBcUMsdUJBQXVCLHVCQUF1QixvQkFBb0Isa0JBQWtCLGlCQUFpQixjQUFjLHNCQUFzQixHQUFHLG1DQUFtQyxpQkFBaUIsaUJBQWlCLGtCQUFrQiw0QkFBNEIsdUJBQXVCLFlBQVksY0FBYyxrQkFBa0IsZ0JBQWdCLEdBQUcsc0RBQXNELGtCQUFrQixnQ0FBZ0Msb0JBQW9CLHVCQUF1QixlQUFlLGlCQUFpQixrQkFBa0IsR0FBRyxzRUFBc0UsZUFBZSxnQkFBZ0IsZUFBZSxrQkFBa0IsdUJBQXVCLHdCQUF3Qiw0QkFBNEIsdUJBQXVCLGlCQUFpQixrQkFBa0IseUNBQXlDLDhCQUE4QiwwQkFBMEIsR0FBRyw0RUFBNEUsZUFBZSx3Q0FBd0MsR0FBRyxpRkFBaUYsOEJBQThCLG9CQUFvQixHQUFHLGlFQUFpRSxlQUFlLFdBQVcsa0JBQWtCLHVCQUF1Qix3QkFBd0IsNEJBQTRCLGVBQWUsaUJBQWlCLHVCQUF1QixpQkFBaUIsa0JBQWtCLHlDQUF5Qyw4QkFBOEIsMEJBQTBCLGVBQWUsR0FBRyx1RUFBdUUsZUFBZSx3Q0FBd0MsR0FBRyw0RUFBNEUsOEJBQThCLG9CQUFvQixHQUFHLCtDQUErQyx1QkFBdUIsbUJBQW1CLG9CQUFvQixrQkFBa0IsaUJBQWlCLGNBQWMsZUFBZSx1QkFBdUIsR0FBRywrQ0FBK0MsdUJBQXVCLG1CQUFtQixvQkFBb0Isa0JBQWtCLGlCQUFpQixjQUFjLGdCQUFnQix1QkFBdUIsR0FBRyx5QkFBeUIsMkJBQTJCLHNCQUFzQixtQkFBbUIscUJBQXFCLHVCQUF1QixHQUFHLHdDQUF3Qyx1QkFBdUIsR0FBRywyQ0FBMkMsd0JBQXdCLEdBQUcsZ0RBQWdELG1CQUFtQixHQUFHLHVDQUF1QyxvQkFBb0Isc0JBQXNCLG1CQUFtQix3QkFBd0IsOEJBQThCLDJCQUEyQix1QkFBdUIsR0FBRyxxQ0FBcUMsa0JBQWtCLHdCQUF3Qiw0QkFBNEIsMkJBQTJCLEdBQUcsbURBQW1ELGtCQUFrQix3QkFBd0IsNEJBQTRCLHdCQUF3QixrQkFBa0IsZ0JBQWdCLGNBQWMscUJBQXFCLHFCQUFxQixHQUFHLG1FQUFtRSxnQ0FBZ0MsR0FBRyxpRUFBaUUsa0JBQWtCLG9CQUFvQixpQkFBaUIsa0JBQWtCLDRCQUE0Qiw0QkFBNEIsMkJBQTJCLHVFQUF1RSxtQkFBbUIsR0FBRywwRUFBMEUsbUJBQW1CLHVCQUF1QixvQkFBb0IscUJBQXFCLHVCQUF1Qix1QkFBdUIsK0NBQStDLDBCQUEwQixHQUFHLHNGQUFzRix1QkFBdUIsV0FBVyxZQUFZLGlCQUFpQixhQUFhLDhCQUE4Qix5Q0FBeUMsNEJBQTRCLEdBQUcsNkVBQTZFLG1CQUFtQix1QkFBdUIsc0JBQXNCLHFCQUFxQixxQkFBcUIsdUJBQXVCLHVCQUF1QiwrQ0FBK0MsMEJBQTBCLEdBQUcseUZBQXlGLHVCQUF1QixXQUFXLFlBQVksaUJBQWlCLGFBQWEsOEJBQThCLHlDQUF5Qyw0QkFBNEIsR0FBRyx5RUFBeUUsbUJBQW1CLHVCQUF1QixtQkFBbUIsc0JBQXNCLG9CQUFvQix3QkFBd0IscUJBQXFCLDRCQUE0Qix1QkFBdUIsdUJBQXVCLCtDQUErQywwQkFBMEIsR0FBRyxxRkFBcUYsdUJBQXVCLFdBQVcsWUFBWSxpQkFBaUIsYUFBYSw4QkFBOEIseUNBQXlDLDRCQUE0QixHQUFHLGdFQUFnRSxvQkFBb0IsaUJBQWlCLHFCQUFxQix1QkFBdUIsR0FBRyxzRUFBc0Usa0JBQWtCLHVCQUF1QixZQUFZLFdBQVcsaUJBQWlCLGdCQUFnQix1RUFBdUUsaUJBQWlCLEdBQUcsb0VBQW9FLGlCQUFpQixnQkFBZ0Isc0JBQXNCLG9DQUFvQyxHQUFHLDBFQUEwRSwwQkFBMEIsR0FBRyxxQkFBcUIsOEJBQThCLHVCQUF1QixzQkFBc0IsbUJBQW1CLHFCQUFxQixrQkFBa0IsNEJBQTRCLHdCQUF3QixHQUFHLCtCQUErQixpQkFBaUIsb0JBQW9CLHVCQUF1Qix1QkFBdUIsaUJBQWlCLHNCQUFzQixzQkFBc0IsR0FBRyxrQ0FBa0MsaUJBQWlCLG9CQUFvQix1QkFBdUIsc0JBQXNCLGlCQUFpQixzQkFBc0Isd0JBQXdCLEdBQUcsNEJBQTRCLHVCQUF1QixhQUFhLGNBQWMsa0JBQWtCLGlCQUFpQixrQkFBa0IsbUJBQW1CLDRDQUE0QyxvQ0FBb0MsMEJBQTBCLHVCQUF1QixrQkFBa0IsdUJBQXVCLGVBQWUsMEJBQTBCLEdBQUcsNkNBQTZDLHlCQUF5Qix1QkFBdUIsc0JBQXNCLG9CQUFvQix1QkFBdUIsR0FBRyxpREFBaUQsbUJBQW1CLG9CQUFvQixHQUFHLDBDQUEwQyx1QkFBdUIsaUJBQWlCLGdCQUFnQixHQUFHLDhDQUE4QyxtQkFBbUIsb0JBQW9CLEdBQUcsZ0NBQWdDLG1CQUFtQixvQkFBb0IsMENBQTBDLDBCQUEwQixHQUFHLHNDQUFzQyxpQ0FBaUMsd0NBQXdDLEdBQUcsbUJBQW1CLDhCQUE4QiwyQkFBMkIsd0JBQXdCLHVCQUF1QiwwQkFBMEIsd0JBQXdCLHNCQUFzQixtQkFBbUIscUJBQXFCLGtCQUFrQixHQUFHLDZCQUE2QixpQkFBaUIsb0JBQW9CLHVCQUF1Qix1QkFBdUIsd0JBQXdCLHFCQUFxQixzQkFBc0IsR0FBRyxnQ0FBZ0MsaUJBQWlCLG9CQUFvQix1QkFBdUIsd0JBQXdCLG9CQUFvQix3QkFBd0IsdUJBQXVCLEdBQUcseUJBQXlCLHVCQUF1QixpQkFBaUIsa0JBQWtCLDhCQUE4QixrQkFBa0IsNEJBQTRCLHdCQUF3QiwwQkFBMEIsZUFBZSx3QkFBd0Isb0JBQW9CLEdBQUcsK0JBQStCLGtCQUFrQixHQUFHLGFBQWEsdUVBQXVFLEdBQUcsbUJBQW1CLHNCQUFzQiwyQkFBMkIsc0JBQXNCLHlCQUF5QixHQUFHLHNCQUFzQixtQkFBbUIscUJBQXFCLHNCQUFzQiwyQkFBMkIscUJBQXFCLHdCQUF3QixHQUFHLGlDQUFpQyw2QkFBNkIsc0JBQXNCLHFCQUFxQixtQkFBbUIsd0JBQXdCLDhCQUE4QiwyQkFBMkIsdUJBQXVCLEdBQUcsZ0NBQWdDLGtCQUFrQix3QkFBd0IsR0FBRyw2Q0FBNkMsaUJBQWlCLGdCQUFnQixrQkFBa0IsR0FBRyxpREFBaUQseUJBQXlCLGlDQUFpQyxHQUFHLDJEQUEyRCx5QkFBeUIsR0FBRyxxQkFBcUIsbUJBQW1CLHNCQUFzQixHQUFHLCtDQUErQyxTQUFTLGtCQUFrQixjQUFjLEtBQUssVUFBVSxlQUFlLGlCQUFpQixLQUFLLEdBQUcsMEJBQTBCLFVBQVUscUJBQXFCLEtBQUssR0FBRywrQkFBK0IsVUFBVSxxQkFBcUIsdUJBQXVCLEtBQUssR0FBRyxPQUFPLDRHQUE0RyxNQUFNLFdBQVcsVUFBVSxVQUFVLE1BQU0sTUFBTSxLQUFLLFNBQVMsS0FBSyxLQUFLLFdBQVcsV0FBVyxXQUFXLFdBQVcsS0FBSyxNQUFNLFdBQVcsV0FBVyxXQUFXLE9BQU8sTUFBTSxXQUFXLE9BQU8sTUFBTSxXQUFXLFVBQVUsVUFBVSxXQUFXLFdBQVcsT0FBTyxNQUFNLFdBQVcsVUFBVSxXQUFXLFdBQVcsT0FBTyxNQUFNLFVBQVUsVUFBVSxVQUFVLFVBQVUsVUFBVSxVQUFVLE1BQU0sTUFBTSxVQUFVLFdBQVcsV0FBVyxNQUFNLE1BQU0sVUFBVSxXQUFXLFdBQVcsVUFBVSxVQUFVLFdBQVcsVUFBVSxNQUFNLE1BQU0sVUFBVSxNQUFNLE1BQU0sV0FBVyxXQUFXLFdBQVcsVUFBVSxVQUFVLFVBQVUsVUFBVSxXQUFXLFdBQVcsV0FBVyxVQUFVLFdBQVcsV0FBVyxNQUFNLE1BQU0sVUFBVSxPQUFPLE1BQU0sV0FBVyxPQUFPLE9BQU8sV0FBVyxNQUFNLE9BQU8sV0FBVyxXQUFXLE9BQU8sT0FBTyxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsWUFBWSxVQUFVLFVBQVUsV0FBVyxRQUFRLE1BQU0sWUFBWSxZQUFZLFVBQVUsVUFBVSxXQUFXLFdBQVcsTUFBTSxNQUFNLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxVQUFVLFVBQVUsV0FBVyxVQUFVLFVBQVUsV0FBVyxXQUFXLE1BQU0sTUFBTSxXQUFXLFVBQVUsVUFBVSxVQUFVLFdBQVcsWUFBWSxPQUFPLE1BQU0sV0FBVyxVQUFVLFVBQVUsVUFBVSxXQUFXLFlBQVksTUFBTSxPQUFPLFVBQVUsVUFBVSxPQUFPLE9BQU8sV0FBVyxPQUFPLE1BQU0sV0FBVyxVQUFVLFVBQVUsT0FBTyxPQUFPLFVBQVUsVUFBVSxPQUFPLE1BQU0sV0FBVyxXQUFXLFdBQVcsVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLFVBQVUsTUFBTSxNQUFNLFdBQVcsVUFBVSxXQUFXLFVBQVUsVUFBVSxXQUFXLFdBQVcsV0FBVyxVQUFVLE9BQU8sTUFBTSxVQUFVLFdBQVcsVUFBVSxPQUFPLE9BQU8sVUFBVSxXQUFXLFdBQVcsVUFBVSxPQUFPLE9BQU8sVUFBVSxVQUFVLFdBQVcsV0FBVyxPQUFPLE1BQU0sV0FBVyxVQUFVLE9BQU8sTUFBTSxXQUFXLE9BQU8sT0FBTyxZQUFZLE9BQU8sTUFBTSxXQUFXLE9BQU8sT0FBTyxZQUFZLFFBQVEsTUFBTSxVQUFVLFdBQVcsV0FBVyxXQUFXLFVBQVUsV0FBVyxNQUFNLE1BQU0sV0FBVyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsV0FBVyxNQUFNLE1BQU0sVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsTUFBTSxNQUFNLFVBQVUsV0FBVyxVQUFVLFdBQVcsVUFBVSxVQUFVLFVBQVUsT0FBTyxPQUFPLFVBQVUsVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxVQUFVLFVBQVUsV0FBVyxZQUFZLFlBQVksT0FBTyxPQUFPLFVBQVUsV0FBVyxPQUFPLE9BQU8sV0FBVyxVQUFVLE9BQU8sT0FBTyxVQUFVLFVBQVUsVUFBVSxXQUFXLFdBQVcsV0FBVyxVQUFVLFVBQVUsV0FBVyxVQUFVLFVBQVUsV0FBVyxZQUFZLFlBQVksVUFBVSxPQUFPLE9BQU8sVUFBVSxXQUFXLE9BQU8sT0FBTyxXQUFXLFVBQVUsT0FBTyxNQUFNLFdBQVcsV0FBVyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsV0FBVyxPQUFPLE1BQU0sV0FBVyxXQUFXLFdBQVcsVUFBVSxVQUFVLFVBQVUsVUFBVSxXQUFXLFFBQVEsTUFBTSxXQUFXLFdBQVcsVUFBVSxXQUFXLFdBQVcsTUFBTSxNQUFNLFdBQVcsTUFBTSxNQUFNLFdBQVcsT0FBTyxPQUFPLFdBQVcsT0FBTyxNQUFNLFVBQVUsV0FBVyxXQUFXLFlBQVksV0FBVyxXQUFXLFdBQVcsTUFBTSxNQUFNLFVBQVUsV0FBVyxXQUFXLFdBQVcsTUFBTSxNQUFNLFVBQVUsV0FBVyxXQUFXLFdBQVcsVUFBVSxVQUFVLFVBQVUsV0FBVyxXQUFXLE9BQU8sT0FBTyxXQUFXLE9BQU8sT0FBTyxVQUFVLFVBQVUsVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxNQUFNLE9BQU8sVUFBVSxXQUFXLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLE9BQU8sT0FBTyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsYUFBYSxhQUFhLFdBQVcsT0FBTyxPQUFPLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLE9BQU8sT0FBTyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsYUFBYSxhQUFhLFdBQVcsT0FBTyxPQUFPLFVBQVUsV0FBVyxXQUFXLFlBQVksVUFBVSxXQUFXLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLE9BQU8sT0FBTyxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsYUFBYSxhQUFhLFdBQVcsT0FBTyxPQUFPLFVBQVUsVUFBVSxXQUFXLFdBQVcsT0FBTyxPQUFPLFVBQVUsV0FBVyxVQUFVLFVBQVUsVUFBVSxVQUFVLFdBQVcsVUFBVSxPQUFPLE9BQU8sVUFBVSxVQUFVLFdBQVcsV0FBVyxPQUFPLE9BQU8sV0FBVyxRQUFRLE1BQU0sYUFBYSxhQUFhLFdBQVcsVUFBVSxXQUFXLFVBQVUsV0FBVyxXQUFXLE1BQU0sTUFBTSxVQUFVLFVBQVUsV0FBVyxXQUFXLFVBQVUsV0FBVyxXQUFXLE1BQU0sTUFBTSxVQUFVLFVBQVUsV0FBVyxXQUFXLFVBQVUsV0FBVyxXQUFXLE1BQU0sTUFBTSxXQUFXLFVBQVUsVUFBVSxVQUFVLFVBQVUsVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsVUFBVSxXQUFXLFVBQVUsV0FBVyxNQUFNLE1BQU0sV0FBVyxXQUFXLFdBQVcsVUFBVSxXQUFXLE9BQU8sT0FBTyxVQUFVLFVBQVUsT0FBTyxNQUFNLFdBQVcsVUFBVSxVQUFVLE9BQU8sT0FBTyxVQUFVLFVBQVUsT0FBTyxNQUFNLFVBQVUsVUFBVSxXQUFXLFdBQVcsT0FBTyxPQUFPLFdBQVcsV0FBVyxRQUFRLE1BQU0sYUFBYSxhQUFhLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxVQUFVLFdBQVcsVUFBVSxNQUFNLE1BQU0sVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxNQUFNLE1BQU0sVUFBVSxVQUFVLFdBQVcsV0FBVyxVQUFVLFdBQVcsV0FBVyxNQUFNLE1BQU0sV0FBVyxVQUFVLFVBQVUsYUFBYSxZQUFZLFdBQVcsV0FBVyxXQUFXLFVBQVUsV0FBVyxVQUFVLE1BQU0sTUFBTSxVQUFVLFFBQVEsTUFBTSxXQUFXLE1BQU0sTUFBTSxXQUFXLFdBQVcsV0FBVyxXQUFXLE1BQU0sTUFBTSxZQUFZLGFBQWEsV0FBVyxXQUFXLFdBQVcsV0FBVyxNQUFNLE1BQU0sV0FBVyxXQUFXLFdBQVcsWUFBWSxhQUFhLFdBQVcsV0FBVyxXQUFXLE1BQU0sTUFBTSxVQUFVLFdBQVcsTUFBTSxNQUFNLFVBQVUsVUFBVSxVQUFVLE9BQU8sT0FBTyxXQUFXLFdBQVcsT0FBTyxPQUFPLFdBQVcsT0FBTyxNQUFNLFlBQVksYUFBYSxPQUFPLFdBQVcsS0FBSyxLQUFLLFVBQVUsVUFBVSxNQUFNLE1BQU0sVUFBVSxVQUFVLE1BQU0sS0FBSyxNQUFNLEtBQUssWUFBWSxPQUFPLEtBQUssTUFBTSxLQUFLLFVBQVUsV0FBVyxNQUFNLDRCQUE0QiwrQkFBK0IsbUJBQW1CLGtCQUFrQixhQUFhLG9GQUFvRix5QkFBeUIsNEJBQTRCLHdCQUF3Qiw0QkFBNEIsNkJBQTZCLDZCQUE2Qix1UkFBdVIsaUNBQWlDLDRFQUE0RSx5QkFBeUIsMkJBQTJCLEtBQUssdUJBQXVCLHVDQUF1QyxzRkFBc0YsNEJBQTRCLDhCQUE4QixRQUFRLGNBQWMsK0JBQStCLHdDQUF3QywyQkFBMkIsS0FBSyxXQUFXLDhCQUE4QixLQUFLLG9CQUFvQiwwQkFBMEIsb0JBQW9CLHNCQUFzQixnQ0FBZ0MsNEJBQTRCLEtBQUssZUFBZSxXQUFXLG9DQUFvQyw0QkFBNEIsb0NBQW9DLDhCQUE4QixTQUFTLEtBQUssaUJBQWlCLHdCQUF3QixzQkFBc0IsZ0JBQWdCLGVBQWUscUJBQXFCLHFCQUFxQixpQkFBaUIsNEJBQTRCLGlEQUFpRCxrREFBa0QseURBQXlELFNBQVMsa0JBQWtCLDBCQUEwQixnQ0FBZ0MsMkNBQTJDLHdCQUF3Qix5QkFBeUIsOEJBQThCLDRCQUE0QixtQkFBbUIsZ0NBQWdDLGFBQWEsU0FBUyxrQkFBa0IsZUFBZSxpQ0FBaUMsZ0NBQWdDLGlDQUFpQywyQkFBMkIseUJBQXlCLHFCQUFxQiw2QkFBNkIscUNBQXFDLHNDQUFzQyxrQ0FBa0MseUJBQXlCLGlDQUFpQywwQ0FBMEMseUJBQXlCLDZCQUE2QixpQkFBaUIsbUJBQW1CLDBDQUEwQyw0QkFBNEIsMEJBQTBCLGdEQUFnRCxvQ0FBb0MsMEVBQTBFLHNEQUFzRCw2QkFBNkIseUJBQXlCLHFCQUFxQixzQkFBc0Isd0NBQXdDLHlDQUF5Qyw0Q0FBNEMsOENBQThDLCtDQUErQyxrREFBa0Qsc0NBQXNDLHVDQUF1Qyw4Q0FBOEMscUJBQXFCLGlCQUFpQixhQUFhLFNBQVMsS0FBSywyQkFBMkIsMkJBQTJCLG1EQUFtRCxpREFBaUQsOEJBQThCLDJCQUEyQix1QkFBdUIsK0JBQStCLGdDQUFnQyxpQ0FBaUMsOEJBQThCLG1DQUFtQyxtQ0FBbUMsb0NBQW9DLG1DQUFtQyw2QkFBNkIsOEJBQThCLG9DQUFvQyw4QkFBOEIsMkJBQTJCLGlDQUFpQyxvQ0FBb0MsMkNBQTJDLDBGQUEwRixtREFBbUQsdUNBQXVDLG9DQUFvQyxnQ0FBZ0MsaUNBQWlDLHdDQUF3Qyx1Q0FBdUMsaUJBQWlCLDJEQUEyRCx1Q0FBdUMsK0JBQStCLCtCQUErQiwrQkFBK0IsMkNBQTJDLHVEQUF1RCxnREFBZ0QsMkNBQTJDLHNDQUFzQyxxQ0FBcUMscUJBQXFCLFFBQVEsaURBQWlELDZDQUE2QyxpQkFBaUIsc0NBQXNDLHVDQUF1QywrQkFBK0IsK0JBQStCLDJDQUEyQyxzQ0FBc0MscUNBQXFDLHFCQUFxQixpQkFBaUIscUJBQXFCLHdCQUF3QixtQ0FBbUMsb0NBQW9DLGlDQUFpQyw2QkFBNkIsOEJBQThCLDhCQUE4QixrQ0FBa0MsbUNBQW1DLDRCQUE0QiwwQ0FBMEMsZ0NBQWdDLHVDQUF1QyxrQ0FBa0MsNENBQTRDLGlDQUFpQywrQkFBK0IseUNBQXlDLHdDQUF3QyxzQ0FBc0Msb0NBQW9DLHdEQUF3RCxpQkFBaUIsMERBQTBELGtDQUFrQyw2Q0FBNkMsK0JBQStCLDJEQUEyRCxxQ0FBcUMsNENBQTRDLDRDQUE0QyxtQ0FBbUMsZ0NBQWdDLHlDQUF5QyxzQ0FBc0Msa0RBQWtELHlEQUF5RCx5QkFBeUIscUJBQXFCLGlCQUFpQixpQ0FBaUMsc0NBQXNDLG9DQUFvQyxpQkFBaUIsb0NBQW9DLGtEQUFrRCw0QkFBNEIsMERBQTBELHFCQUFxQixpQkFBaUIsNEJBQTRCLGtEQUFrRCw0QkFBNEIsMERBQTBELHFCQUFxQixpQkFBaUIsYUFBYSxhQUFhLEtBQUssbUJBQW1CLG1CQUFtQiwwQkFBMEIsK0JBQStCLG9DQUFvQyw0QkFBNEIseUJBQXlCLDZCQUE2QixvQ0FBb0MsbUNBQW1DLG1DQUFtQyxnQ0FBZ0MsOEJBQThCLDZCQUE2QiwwQkFBMEIsa0NBQWtDLGFBQWEsMEJBQTBCLDZCQUE2Qiw2QkFBNkIsOEJBQThCLHdDQUF3QyxtQ0FBbUMsd0JBQXdCLDBCQUEwQiw4QkFBOEIsNEJBQTRCLHVDQUF1QyxrQ0FBa0MsZ0RBQWdELG9DQUFvQyx1Q0FBdUMsK0JBQStCLGlDQUFpQyxrQ0FBa0Msd0NBQXdDLG1DQUFtQyxvQ0FBb0MsbUNBQW1DLHNDQUFzQywyQ0FBMkMsNENBQTRDLGdEQUFnRCwyQ0FBMkMscUNBQXFDLHNDQUFzQyw0REFBNEQsMERBQTBELDhDQUE4QyxnQ0FBZ0MsdUNBQXVDLHFFQUFxRSxxREFBcUQsbUNBQW1DLHNEQUFzRCw0Q0FBNEMseUJBQXlCLHFCQUFxQixtQ0FBbUMsbUNBQW1DLCtCQUErQixzQ0FBc0MsMkNBQTJDLDRDQUE0QyxnREFBZ0QsbUNBQW1DLHFDQUFxQywyQ0FBMkMscUNBQXFDLHNDQUFzQyw0REFBNEQsMERBQTBELDhDQUE4QyxtQ0FBbUMsZ0NBQWdDLHVDQUF1QyxxRUFBcUUseUJBQXlCLG1DQUFtQyxzREFBc0QsNENBQTRDLHlCQUF5QixxQkFBcUIsaUJBQWlCLGdDQUFnQyx1Q0FBdUMsdUNBQXVDLG9DQUFvQyxrQ0FBa0MsaUNBQWlDLDhCQUE4QiwrQkFBK0IsdUNBQXVDLGlCQUFpQixnQ0FBZ0MsdUNBQXVDLHVDQUF1QyxvQ0FBb0Msa0NBQWtDLGlDQUFpQyw4QkFBOEIsZ0NBQWdDLHVDQUF1QyxpQkFBaUIsaUJBQWlCLGFBQWEsS0FBSyxrQkFBa0Isa0JBQWtCLG1DQUFtQyw0QkFBNEIseUJBQXlCLDZCQUE2QiwrQkFBK0IsNkJBQTZCLG1DQUFtQyxtQkFBbUIsd0NBQXdDLHlCQUF5QiwrQ0FBK0MscUJBQXFCLGlCQUFpQixhQUFhLDZCQUE2QixnQ0FBZ0Msa0NBQWtDLG1DQUFtQyxvQ0FBb0MsMENBQTBDLHNDQUFzQyxtQ0FBbUMsYUFBYSw4QkFBOEIsOEJBQThCLG9DQUFvQyx3Q0FBd0MsdUNBQXVDLDhCQUE4QixrQ0FBa0Msd0NBQXdDLDRDQUE0Qyx3Q0FBd0Msa0NBQWtDLGdDQUFnQyw4QkFBOEIscUNBQXFDLHFDQUFxQyxzQ0FBc0Msb0RBQW9ELHFCQUFxQixrQ0FBa0Msc0NBQXNDLHdDQUF3QyxxQ0FBcUMsc0NBQXNDLGdEQUFnRCxnREFBZ0QsK0NBQStDLHdHQUF3Ryx5Q0FBeUMsaUNBQWlDLDJDQUEyQywrQ0FBK0MsNENBQTRDLDZDQUE2QywrQ0FBK0MsK0NBQStDLHFFQUFxRSxnREFBZ0QseUNBQXlDLG1EQUFtRCx1Q0FBdUMsd0NBQXdDLDZDQUE2Qyx5Q0FBeUMsNERBQTRELHFFQUFxRSx1REFBdUQsNkJBQTZCLHlCQUF5QixvQ0FBb0MsMkNBQTJDLCtDQUErQyw4Q0FBOEMsNkNBQTZDLDZDQUE2QywrQ0FBK0MsK0NBQStDLHVFQUF1RSxnREFBZ0Qsd0NBQXdDLG1EQUFtRCx1Q0FBdUMsd0NBQXdDLDZDQUE2Qyx5Q0FBeUMsNERBQTRELHFFQUFxRSx1REFBdUQsNkJBQTZCLHlCQUF5QixnQ0FBZ0MsMkNBQTJDLCtDQUErQywrQ0FBK0MsOENBQThDLDRDQUE0QyxnREFBZ0QsNkNBQTZDLG1EQUFtRCwrQ0FBK0MsNkNBQTZDLHFFQUFxRSxnREFBZ0QseUNBQXlDLG1EQUFtRCx1Q0FBdUMsd0NBQXdDLDZDQUE2Qyx5Q0FBeUMsNERBQTRELHFFQUFxRSx1REFBdUQsNkJBQTZCLHlCQUF5QixxQkFBcUIsaUNBQWlDLHdDQUF3QyxxQ0FBcUMseUNBQXlDLDJDQUEyQyxnQ0FBZ0Msd0NBQXdDLCtDQUErQyxvQ0FBb0MsbUNBQW1DLHlDQUF5Qyx3Q0FBd0MsK0ZBQStGLHdDQUF3Qyx5QkFBeUIsNEJBQTRCLHlDQUF5Qyx3Q0FBd0MsOENBQThDLDJEQUEyRCx5QkFBeUIsZ0NBQWdDLGdDQUFnQyxzREFBc0QsNkJBQTZCLHlCQUF5QixxQkFBcUIsaUJBQWlCLGFBQWEsU0FBUyxLQUFLLGlCQUFpQix3QkFBd0Isd0NBQXdDLCtCQUErQiw4QkFBOEIsMkJBQTJCLDZCQUE2QiwwQkFBMEIsb0NBQW9DLGdDQUFnQyxtQ0FBbUMsNkJBQTZCLGdDQUFnQyxtQ0FBbUMsbUNBQW1DLDZCQUE2QixrQ0FBa0Msa0NBQWtDLHdDQUF3QywrQkFBK0IsNkJBQTZCLGdDQUFnQyxtQ0FBbUMsa0NBQWtDLDZCQUE2QixrQ0FBa0Msb0NBQW9DLGFBQWEseUJBQXlCLG1DQUFtQywwQ0FBMEMsNkJBQTZCLHdCQUF3QiwwQkFBMEIsOEJBQThCLDZCQUE2Qiw4QkFBOEIsK0JBQStCLHdEQUF3RCxnREFBZ0Qsc0NBQXNDLG1DQUFtQyw4QkFBOEIsbUNBQW1DLDJCQUEyQixzQ0FBc0MscURBQXFELHlDQUF5Qyx1Q0FBdUMsc0NBQXNDLG9DQUFvQyx1Q0FBdUMsd0JBQXdCLHVDQUF1Qyx3Q0FBd0MscUJBQXFCLHFCQUFxQixrQ0FBa0MsOENBQThDLHVDQUF1QyxpQ0FBaUMsZ0NBQWdDLHdCQUF3Qix1Q0FBdUMsd0NBQXdDLHFCQUFxQixpQkFBaUIsd0JBQXdCLG1DQUFtQyxvQ0FBb0MsK0RBQStELDBDQUEwQyw0QkFBNEIsb0RBQW9ELGlFQUFpRSxxQkFBcUIsaUJBQWlCLGFBQWEsU0FBUyxLQUFLLGdCQUFnQixtQ0FBbUMsZ0JBQWdCLHdDQUF3QyxtQ0FBbUMsZ0NBQWdDLCtCQUErQixrQ0FBa0MsZ0NBQWdDLDhCQUE4QiwyQkFBMkIsNkJBQTZCLDBCQUEwQiw0QkFBNEIsNkJBQTZCLGdDQUFnQyxtQ0FBbUMsbUNBQW1DLG9DQUFvQyxpQ0FBaUMsa0NBQWtDLDRCQUE0QiwrQkFBK0IsNkJBQTZCLGdDQUFnQyxtQ0FBbUMsb0NBQW9DLGdDQUFnQyxvQ0FBb0MsbUNBQW1DLDZCQUE2Qiw0QkFBNEIsbUNBQW1DLDZCQUE2Qiw4QkFBOEIsNENBQTRDLDhCQUE4Qix3Q0FBd0Msb0NBQW9DLHNDQUFzQywyQkFBMkIsb0NBQW9DLGdDQUFnQyxzQkFBc0Isa0NBQWtDLGlCQUFpQixhQUFhLFNBQVMsS0FBSyxnQkFBZ0IsMkZBQTJGLGdCQUFnQiw4QkFBOEIsbUNBQW1DLDhCQUE4QixpQ0FBaUMsZUFBZSxrQ0FBa0MsaUNBQWlDLGtDQUFrQyxzQ0FBc0MsaUNBQWlDLG9DQUFvQyxhQUFhLCtCQUErQix5Q0FBeUMsa0NBQWtDLGlDQUFpQyxtQ0FBbUMsb0NBQW9DLDBDQUEwQyxzQ0FBc0MsbUNBQW1DLDZCQUE2QiwwQ0FBMEMsOEJBQThCLG9DQUFvQyw4QkFBOEIsaUNBQWlDLGdDQUFnQyxrQ0FBa0MseUJBQXlCLDZDQUE2QyxvREFBb0QsZ0NBQWdDLGdDQUFnQyxxREFBcUQsNkJBQTZCLHlCQUF5QixxQkFBcUIsaUJBQWlCLGFBQWEsY0FBYyxrQ0FBa0Msa0NBQWtDLGFBQWEsU0FBUyxLQUFLLHlEQUF5RCxXQUFXLG9CQUFvQixnQkFBZ0IsT0FBTyxZQUFZLGlCQUFpQixtQkFBbUIsT0FBTyxLQUFLLDRCQUE0QixZQUFZLHlCQUF5QixPQUFPLEtBQUssaUNBQWlDLFlBQVksdUJBQXVCLHlCQUF5QixPQUFPLEtBQUssdUJBQXVCO0FBQ2hrM0M7QUFDZSxzRkFBdUIsRUFBQzs7Ozs7Ozs7Ozs7OztBQ1R2QztBQUFBO0FBQUE7QUFBQTtBQUNrRztBQUNsRyw4QkFBOEIsbUZBQTJCO0FBQ3pEO0FBQ0EsOEJBQThCLFFBQVMsc0JBQXNCLEtBQUssMkJBQTJCLGVBQWUsY0FBYyxHQUFHLHNFQUFzRSxrQkFBa0Isa0JBQWtCLGlCQUFpQixrQkFBa0IsOEJBQThCLG9CQUFvQixxQkFBcUIsNEJBQTRCLHdCQUF3Qiw2QkFBNkIsMkJBQTJCLEdBQUcsMEJBQTBCLG1CQUFtQixxQkFBcUIsR0FBRywyQkFBMkIsZ0JBQWdCLGlCQUFpQixrQkFBa0IsNEJBQTRCLHdCQUF3Qiw2QkFBNkIsbUJBQW1CLG9CQUFvQixtQkFBbUIsR0FBRyxPQUFPLGtIQUFrSCxNQUFNLFdBQVcsVUFBVSxVQUFVLE1BQU0sTUFBTSxLQUFLLEtBQUssVUFBVSxVQUFVLFVBQVUsVUFBVSxXQUFXLFVBQVUsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLEtBQUssS0FBSyxXQUFXLFlBQVksS0FBSyxLQUFLLFVBQVUsVUFBVSxVQUFVLFdBQVcsV0FBVyxXQUFXLFdBQVcsV0FBVyxXQUFXLDhCQUE4QiwrQkFBK0IsbUJBQW1CLGtCQUFrQixLQUFLLG9GQUFvRix5QkFBeUIsNEJBQTRCLHdCQUF3Qiw2QkFBNkIsNkJBQTZCLDZCQUE2QixzQkFBc0Isc0JBQXNCLHNCQUFzQixxQkFBcUIsc0JBQXNCLG9DQUFvQyx3QkFBd0IseUJBQXlCLGdDQUFnQyw0QkFBNEIsaUNBQWlDLCtCQUErQixpQkFBaUIsZ0NBQWdDLDZCQUE2QixTQUFTLGtCQUFrQix3QkFBd0IseUJBQXlCLHNDQUFzQywwQkFBMEIsb0NBQW9DLGdDQUFnQyxxQ0FBcUMsZ0NBQWdDLDRCQUE0QixtQ0FBbUMsU0FBUyxLQUFLLG1CQUFtQjtBQUNwdkU7QUFDZSxzRkFBdUIsRUFBQzs7Ozs7Ozs7Ozs7OztBQ04xQjs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjs7QUFFaEI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsNENBQTRDLHFCQUFxQjtBQUNqRTs7QUFFQTtBQUNBLEtBQUs7QUFDTCxJQUFJO0FBQ0o7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQSxxQkFBcUIsaUJBQWlCO0FBQ3RDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvQkFBb0IscUJBQXFCO0FBQ3pDOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSw4QkFBOEI7O0FBRTlCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7O0FBR0Q7QUFDQTtBQUNBO0FBQ0EscURBQXFELGNBQWM7QUFDbkU7QUFDQSxDOzs7Ozs7Ozs7Ozs7QUM3RmE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOzs7QUFHSDs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxFOzs7Ozs7Ozs7Ozs7QUNqQ2E7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOzs7QUFHSDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsRTs7Ozs7Ozs7Ozs7O0FDekJhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1REFBdUQ7O0FBRXZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBOztBQUVBLGlCQUFpQix3QkFBd0I7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsaUJBQWlCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxnQkFBZ0IsS0FBd0MsR0FBRyxzQkFBaUIsR0FBRyxTQUFJOztBQUVuRjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQSxxRUFBcUUscUJBQXFCLGFBQWE7O0FBRXZHOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQSx5REFBeUQ7QUFDekQsR0FBRzs7QUFFSDs7O0FBR0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDBCQUEwQjtBQUMxQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLG1CQUFtQiw0QkFBNEI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsb0JBQW9CLDZCQUE2QjtBQUNqRDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsRTs7Ozs7Ozs7Ozs7O0FDNVFBO0FBQUE7QUFBQTtBQUF5RTtBQUNTO0FBRWxGO0lBQUE7UUFFRSxjQUFTLEdBQUc7WUFDViw4RkFBcUI7WUFDckIscUZBQWtCO1NBQ25CLENBQUM7UUFFRixXQUFNLEdBQVEsRUFBRSxDQUFDO0lBbURuQixDQUFDO0lBakRDOzs7T0FHRztJQUNILG9CQUFNLEdBQU47UUFBQSxpQkFpQkM7UUFoQkMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLHdCQUFjO1lBQ25DLElBQU0sVUFBVSxHQUFHLElBQUksY0FBYyxFQUFFLENBQUM7WUFDeEMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdkIsU0FBa0IsSUFBSSxjQUFjLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBL0MsSUFBSSxVQUFFLE9BQU8sUUFBa0MsQ0FBQztZQUN2RCxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7UUFDeEQsQ0FBQyxDQUFDO1FBRUYsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUc7WUFDckIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxVQUFlO2dCQUNsQyxJQUFJLE9BQU8sSUFBSSxVQUFVLENBQUMsU0FBUyxFQUFFO29CQUNuQyxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUM5QjtZQUNILENBQUMsQ0FBQztRQUNKLENBQUM7SUFDSCxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNILHdCQUFVLEdBQVYsVUFBVyxFQUFVLEVBQUUsT0FBeUI7UUFDOUMsSUFBTSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztRQUNiLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILHVCQUFTLEdBQVQ7UUFDRSxJQUFNLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlDLE9BQU8sQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDO1FBQ25CLE9BQU8sT0FBTyxDQUFDO0lBQ2pCLENBQUM7SUFFSCxVQUFDO0FBQUQsQ0FBQztBQUVELElBQU0sR0FBRyxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7QUFDdEIsR0FBRyxDQUFDLE1BQU0sRUFBRSxDQUFDOzs7Ozs7Ozs7Ozs7QUNoRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUU7Ozs7Ozs7Ozs7O0FDNUJBO0FBQ0EsK0NBQStDLG1CQUFPLENBQUMsc0hBQTZEO0FBQ3BILGlDQUFpQyxtQkFBTyxDQUFDLDJFQUFvQztBQUM3RSxpQ0FBaUMsbUJBQU8sQ0FBQywyRUFBb0M7QUFDN0UsaUNBQWlDLG1CQUFPLENBQUMsNkVBQXFDO0FBQzlFLGlDQUFpQyxtQkFBTyxDQUFDLG1FQUFnQztBQUN6RSxpQ0FBaUMsbUJBQU8sQ0FBQywyRUFBb0M7QUFDN0UsaUNBQWlDLG1CQUFPLENBQUMsMkVBQW9DO0FBQzdFLGlDQUFpQyxtQkFBTyxDQUFDLGlFQUErQjtBQUN4RSxpQ0FBaUMsbUJBQU8sQ0FBQyx5RUFBbUM7QUFDNUUsaUNBQWlDLG1CQUFPLENBQUMseUVBQW1DO0FBQzVFLGlDQUFpQyxtQkFBTyxDQUFDLHVFQUFrQztBQUMzRSxrQ0FBa0MsbUJBQU8sQ0FBQyx1RUFBa0M7QUFDNUUsa0NBQWtDLG1CQUFPLENBQUMsbUVBQWdDO0FBQzFFLGtDQUFrQyxtQkFBTyxDQUFDLG1FQUFnQztBQUMxRSxrQ0FBa0MsbUJBQU8sQ0FBQyxxRUFBaUM7QUFDM0Usa0NBQWtDLG1CQUFPLENBQUMsbUVBQWdDO0FBQzFFLGtDQUFrQyxtQkFBTyxDQUFDLG1FQUFnQztBQUMxRSxrQ0FBa0MsbUJBQU8sQ0FBQyxtRUFBZ0M7QUFDMUUsa0NBQWtDLG1CQUFPLENBQUMsdUVBQWtDO0FBQzVFLGtDQUFrQyxtQkFBTyxDQUFDLHlFQUFtQztBQUM3RSxrQ0FBa0MsbUJBQU8sQ0FBQyxxRUFBaUM7QUFDM0Usa0NBQWtDLG1CQUFPLENBQUMsdUVBQWtDO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK3VEQUErdUQsNGlCQUE0aUIsNnBIQUE2cEgsbU1BQW1NLG1NQUFtTSxtTUFBbU0sbU1BQW1NO0FBQ3BzTjtBQUNBLHNCOzs7Ozs7Ozs7OztBQy9DQSxVQUFVLG1CQUFPLENBQUMsNEpBQWlGO0FBQ25HLDBCQUEwQixtQkFBTyxDQUFDLDhPQUFvSDs7QUFFdEo7O0FBRUE7QUFDQSwwQkFBMEIsUUFBUztBQUNuQzs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOzs7O0FBSUEsc0M7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUFBO0FBQUE7SUFVSTtRQUFBLGlCQUVDO1FBVEQsY0FBUyxHQUFHO1lBQ1IsRUFBRSxFQUFFLFdBQVc7WUFDZixJQUFJLEVBQUUsZUFBZTtZQUNyQixLQUFLLEVBQUUsZUFBZTtZQUN0QixLQUFLLEVBQUUsY0FBUSxLQUFJLENBQUMsS0FBSyxFQUFFLEVBQUMsQ0FBQztTQUNoQztRQUdHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxrQ0FBSyxHQUFMO1FBQUEsaUJBTUM7UUFMRyxNQUFNLENBQUMsVUFBVSxDQUFDO1lBQ2QsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN4QixDQUFDLEVBQUMsSUFBSSxDQUFDO1FBQ1AsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7T0FFRztJQUNILHFDQUFRLEdBQVI7UUFDSSwwRUFBUSxPQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBTyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLElBQUksR0FBRywwRUFBUSxPQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBTSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxvQ0FBTyxHQUFQO1FBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFFLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUMzRixDQUFDO0lBRUQ7O09BRUc7SUFDSCx5Q0FBWSxHQUFaO1FBQUEsaUJBa0VDO1FBakVHLE1BQU0sQ0FBQyxRQUFRLEdBQUcsVUFBQyxDQUFNO1lBQ3JCLElBQUksZUFBZSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztZQUU3QixzQkFBc0I7WUFDdEIsSUFBRyxlQUFlLEdBQUcsRUFBRSxFQUFDO2dCQUNwQixLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDckI7WUFFRCwyQkFBMkI7WUFDM0IsSUFBRyxlQUFlLElBQUksQ0FBQyxJQUFJLGVBQWUsSUFBSSxHQUFHLEVBQUM7Z0JBQzlDLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBRXZCO2lCQUNJO2dCQUNELEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdEIsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMzQjtZQUNELElBQUksZUFBZSxJQUFJLEVBQUUsSUFBSSxlQUFlLEdBQUcsSUFBSSxFQUFFLEVBQUMsd0JBQXdCO2dCQUMxRSxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwQixJQUFJLGVBQWUsR0FBRyxHQUFHLElBQUksZUFBZSxHQUFHLElBQUksRUFBRSxFQUFDLHdDQUF3QztvQkFDMUYsS0FBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUNqQixLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztpQkFDM0I7cUJBQ0k7b0JBQ0QsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUMvQjthQUNKO2lCQUNJO2dCQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUI7WUFFRCxJQUFJLGVBQWUsR0FBRyxJQUFJLElBQUksZUFBZSxHQUFHLElBQUksRUFBRSxFQUFDLG9DQUFvQztnQkFDdkYsS0FBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFFaEM7aUJBQ0k7Z0JBQ0QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3BDO1lBRUQsSUFBRyxlQUFlLElBQUksSUFBSSxJQUFJLGVBQWUsR0FBRSxJQUFJLEVBQUM7Z0JBQ2hELEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUN4QjtpQkFBSyxJQUFHLGVBQWUsR0FBQyxJQUFJLEVBQUM7Z0JBQzFCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDNUI7aUJBQUk7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUMsSUFBSSxDQUFDLENBQUM7YUFDakM7WUFFRCxJQUFHLGVBQWUsSUFBRSxJQUFJLEVBQUM7Z0JBQ3JCLEtBQUksQ0FBQyxhQUFhLEVBQUU7YUFDdkI7aUJBQUk7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM1QjtZQUVELElBQUcsZUFBZSxJQUFJLElBQUksSUFBSSxlQUFlLEdBQUUsSUFBSSxFQUFDO2dCQUNoRCxLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwQixLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pCO1lBRUQsSUFBSSxlQUFlLElBQUksSUFBSSxFQUFFO2dCQUN6QixLQUFJLENBQUMsYUFBYSxFQUFFO2dCQUNwQixLQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDckI7UUFDTCxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCwwQ0FBYSxHQUFiLFVBQWMsR0FBVztRQUFYLGlDQUFXO1FBQ3JCLElBQU0sTUFBTSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBRyxDQUFDLEdBQUcsRUFBQztZQUNKLE1BQU0sQ0FBQyxLQUFLLENBQUMsZUFBZSxHQUFHLFNBQVMsQ0FBQztTQUM1QzthQUNHO1lBQ0EsTUFBTSxDQUFDLEtBQUssQ0FBQyxlQUFlLEdBQUcsYUFBYSxDQUFDO1NBQ2hEO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCx5Q0FBWSxHQUFaLFVBQWEsR0FBVztRQUFYLGlDQUFXO1FBQ3BCLElBQU0sV0FBVyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFM0UsSUFBRyxDQUFDLEdBQUcsRUFBQztZQUNKLEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUNyQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7YUFDcEM7U0FDSjthQUNHO1lBQ0EsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3JDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQzthQUNwQztTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxrREFBcUIsR0FBckIsVUFBc0IsR0FBVztRQUFYLGlDQUFXO1FBQzdCLElBQU0sYUFBYSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9ELElBQU0sZUFBZSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BFLElBQU0sV0FBVyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVELElBQU0sWUFBWSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ2pFLElBQUcsQ0FBQyxHQUFHLEVBQUM7WUFDSixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDM0MsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsK0JBQStCLENBQUM7YUFDdEU7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsZUFBZSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDN0MsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsK0JBQStCLENBQUM7YUFDeEU7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDekMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsK0JBQStCLENBQUM7YUFDcEU7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsWUFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDMUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcseUJBQXlCLENBQUM7YUFDL0Q7U0FDSjthQUNHO1lBQ0EsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGFBQWEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzNDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzthQUN6QztZQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxlQUFlLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUM3QyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7YUFDM0M7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDekMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO2FBQ3ZDO1lBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQzthQUN4QztTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCw2Q0FBZ0IsR0FBaEIsVUFBaUIsR0FBVztRQUFYLGlDQUFXO1FBQ3hCLElBQU0sU0FBUyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzdELElBQU0sY0FBYyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDdkUsSUFBRyxDQUFDLEdBQUcsRUFBQztZQUNKLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN2QyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7Z0JBQy9CLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLDBCQUEwQixDQUFDO2FBQzdEO1lBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLGNBQWMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQzVDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztnQkFDcEMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsMEJBQTBCLENBQUM7YUFDbEU7U0FDSjthQUNHO1lBQ0EsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3ZDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQztnQkFDL0IsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsMEJBQTBCLENBQUM7YUFDN0Q7WUFDRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDNUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO2dCQUNwQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRywwQkFBMEIsQ0FBQzthQUNsRTtTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7OztPQVFHO0lBQ0gsMENBQWEsR0FBYixVQUFjLEdBQVcsRUFBQyxLQUFXO1FBQXZCLGlDQUFXO1FBQUMscUNBQVc7UUFDakMsSUFBTSxPQUFPLEdBQVEsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUQsSUFBRyxDQUFDLEdBQUcsRUFBQztZQUNKLE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFDLENBQUMsQ0FBQztZQUN4QixPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksR0FBQyxLQUFLLENBQUM7U0FDNUI7YUFDSSxJQUFJLENBQUMsS0FBSyxFQUFDO1lBQ1osT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFDLE9BQU8sQ0FBQztTQUM5QjthQUFJO1lBQ0QsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsQ0FBQyxDQUFDO1lBQ3hCLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFDLFFBQVEsQ0FBQztTQUMvQjtJQUNMLENBQUM7SUFHRDs7OztPQUlHO0lBQ0gsMENBQWEsR0FBYixVQUFjLEdBQVc7UUFBWCxpQ0FBVztRQUNyQixJQUFNLE1BQU0sR0FBUSxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBQyxLQUFLLENBQUMsQ0FBQztRQUN2RCxJQUFHLENBQUMsR0FBRyxFQUFDO1lBQ0osTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUMsQ0FBQyxDQUFDO1NBQzFCO2FBQ0c7WUFDQSxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBQyxDQUFDLENBQUM7WUFDdkIsa0VBQWtFO1NBQ3JFO0lBQ0wsQ0FBQztJQUlEOzs7O09BSUc7SUFDSCxzQ0FBUyxHQUFULFVBQVUsR0FBVTtRQUFWLGdDQUFVO1FBQ2hCLElBQU0sR0FBRyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsSUFBTSxHQUFHLEdBQVEsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQyxJQUFHLEdBQUcsRUFBQztZQUNILEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsVUFBQyxHQUFRLElBQUssY0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBbUMsR0FBSyxDQUFDLEVBQXJELENBQXFELENBQUMsQ0FBQztZQUM3RixHQUFHLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztZQUNsQixHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDZjthQUNHO1lBQ0EsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ1osR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxVQUFDLEdBQVEsSUFBSyxjQUFPLENBQUMsR0FBRyxDQUFDLHFDQUFtQyxHQUFLLENBQUMsRUFBckQsQ0FBcUQsQ0FBQyxDQUFDO1lBQzdGLEdBQUcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQztJQUVEOztPQUVHO0lBQ0gsdUNBQVUsR0FBVjtRQUNJLElBQU0sR0FBRyxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsSUFBTSxHQUFHLEdBQVEsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQyxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDWixHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsMENBQWEsR0FBYjtRQUNJLElBQU0sSUFBSSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUM1QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDaEMsQ0FBQztJQUdEOztPQUVHO0lBQ0gsMkNBQWMsR0FBZDtRQUNJLElBQU0sSUFBSSxHQUFRLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3JELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtZQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztJQUNwRSxDQUFDO0lBR0Q7Ozs7OztPQU1HO0lBQ0gseUNBQVksR0FBWixVQUFhLFFBQWdCLEVBQUUsR0FBVztRQUFYLGlDQUFXO1FBQ3RDLElBQU0sS0FBSyxHQUFHLFdBQVMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFNBQUksUUFBVSxDQUFDO1FBQ3ZELElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDTixPQUFPLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDeEM7YUFBTTtZQUNILE9BQU8sUUFBUSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxDQUFDO1NBRTNDO0lBQ0wsQ0FBQztJQUVMLHlCQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7QUM3VUQ7QUFBQTtBQUFPLFNBQVMsS0FBSyxDQUFDLFNBQXNCO0lBQ3hDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsTUFBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFDLENBQUMsQ0FBRSxDQUFDO0FBQ3hELENBQUM7Ozs7Ozs7Ozs7OztBQ0ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEU7Ozs7Ozs7Ozs7O0FDMUJBO0FBQ0EsK0NBQStDLG1CQUFPLENBQUMsc0hBQTZEO0FBQ3BILGlDQUFpQyxtQkFBTyxDQUFDLHFFQUFpQztBQUMxRTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCOzs7Ozs7Ozs7OztBQ1BBLFVBQVUsbUJBQU8sQ0FBQyw0SkFBaUY7QUFDbkcsMEJBQTBCLG1CQUFPLENBQUMsdVBBQXVIOztBQUV6Sjs7QUFFQTtBQUNBLDBCQUEwQixRQUFTO0FBQ25DOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7Ozs7QUFJQSxzQzs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQUE7QUFBQTtJQVVJO1FBQUEsaUJBRUM7UUFURCxjQUFTLEdBQUc7WUFDUixFQUFFLEVBQUUsY0FBYztZQUNsQixJQUFJLEVBQUUsa0JBQWtCO1lBQ3hCLEtBQUssRUFBRSxrQkFBa0I7WUFDekIsS0FBSyxFQUFFLGNBQVEsS0FBSSxDQUFDLEtBQUssRUFBRSxFQUFDLENBQUM7U0FDaEM7UUFHRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEIsQ0FBQztJQUVEOztPQUVHO0lBQ0gscUNBQUssR0FBTDtRQUFBLGlCQVFDO1FBUEc7O1dBRUc7UUFDSCxNQUFNLENBQUMsVUFBVSxDQUFDO1lBQ2QsSUFBTSxRQUFRLEdBQVEsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsRUFBSSxDQUFDLENBQUM7WUFDdEUsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ3BDLENBQUMsRUFBQyxJQUFJLENBQUM7SUFDWCxDQUFDO0lBRUQ7O09BRUc7SUFDSCx3Q0FBUSxHQUFSO1FBQ0ksNkVBQVEsT0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQU8sQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLEdBQUcsNkVBQVEsT0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQU0sQ0FBQyxDQUFDO0lBQ3BELENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsdUNBQU8sR0FBUDtRQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDM0YsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILDRDQUFZLEdBQVosVUFBYSxRQUFnQixFQUFFLEdBQVc7UUFBWCxpQ0FBVztRQUN0QyxJQUFNLEtBQUssR0FBRyxXQUFTLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxTQUFJLFFBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sT0FBTyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hDO2FBQU07WUFDSCxPQUFPLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUUzQztJQUNMLENBQUM7SUFFTCw0QkFBQztBQUFELENBQUM7Ozs7Ozs7Ozs7Ozs7O0FDN0REO0FBQWUsMEZBQTJCLEU7Ozs7Ozs7Ozs7OztBQ0ExQztBQUFlLDBGQUEyQixFOzs7Ozs7Ozs7Ozs7QUNBMUM7QUFBZSx5RkFBMEIsRTs7Ozs7Ozs7Ozs7O0FDQXpDO0FBQWUsMEZBQTJCLEU7Ozs7Ozs7Ozs7OztBQ0ExQztBQUFlLDBGQUEyQixFOzs7Ozs7Ozs7Ozs7QUNBMUM7QUFBZSwyRkFBNEIsRTs7Ozs7Ozs7Ozs7O0FDQTNDO0FBQWUsd0ZBQXlCLEU7Ozs7Ozs7Ozs7OztBQ0F4QztBQUFlLHlGQUEwQixFOzs7Ozs7Ozs7Ozs7QUNBekM7QUFBZSx3RkFBeUIsRTs7Ozs7Ozs7Ozs7O0FDQXhDO0FBQWUsdUZBQXdCLEU7Ozs7Ozs7Ozs7OztBQ0F2QztBQUFlLHNGQUF1QixFOzs7Ozs7Ozs7Ozs7QUNBdEM7QUFBZSxzRkFBdUIsRTs7Ozs7Ozs7Ozs7O0FDQXRDO0FBQWUsdUZBQXdCLEU7Ozs7Ozs7Ozs7OztBQ0F2QztBQUFlLHNGQUF1QixFOzs7Ozs7Ozs7Ozs7QUNBdEM7QUFBZSxzRkFBdUIsRTs7Ozs7Ozs7Ozs7O0FDQXRDO0FBQWUsd0ZBQXlCLEU7Ozs7Ozs7Ozs7OztBQ0F4QztBQUFlLHdGQUF5QixFOzs7Ozs7Ozs7Ozs7QUNBeEM7QUFBZSx5RkFBMEIsRTs7Ozs7Ozs7Ozs7O0FDQXpDO0FBQWUscUZBQXNCLEU7Ozs7Ozs7Ozs7OztBQ0FyQztBQUFlLHNGQUF1QixFOzs7Ozs7Ozs7Ozs7QUNBdEM7QUFBZSx1RkFBd0IsRTs7Ozs7Ozs7Ozs7O0FDQXZDO0FBQWUsc0ZBQXVCLEU7Ozs7Ozs7Ozs7OztBQ0F0QztBQUFlLDZGQUE4QixFOzs7Ozs7Ozs7OztBQ0E3QyxtQkFBTyxDQUFDLG1DQUFXLENBQUMsQ0FBQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvbWFpbi50c1wiKTtcbiIsIi8vIEltcG9ydHNcbmltcG9ydCBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18gZnJvbSBcIi4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvcnVudGltZS9hcGkuanNcIjtcbmltcG9ydCBfX19DU1NfTE9BREVSX0dFVF9VUkxfSU1QT1JUX19fIGZyb20gXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvZ2V0VXJsLmpzXCI7XG5pbXBvcnQgX19fQ1NTX0xPQURFUl9VUkxfSU1QT1JUXzBfX18gZnJvbSBcIi4uLy4uLy4uL2ZvbnRzL0Vhc3RtYW5UcmlhbC1CbGFjay5vdGZcIjtcbnZhciBfX19DU1NfTE9BREVSX0VYUE9SVF9fXyA9IF9fX0NTU19MT0FERVJfQVBJX0lNUE9SVF9fXyh0cnVlKTtcbnZhciBfX19DU1NfTE9BREVSX1VSTF9SRVBMQUNFTUVOVF8wX19fID0gX19fQ1NTX0xPQURFUl9HRVRfVVJMX0lNUE9SVF9fXyhfX19DU1NfTE9BREVSX1VSTF9JTVBPUlRfMF9fXyk7XG4vLyBNb2R1bGVcbl9fX0NTU19MT0FERVJfRVhQT1JUX19fLnB1c2goW21vZHVsZS5pZCwgXCJAY2hhcnNldCBcXFwiVVRGLThcXFwiO1xcbioge1xcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIHBhZGRpbmc6IDA7XFxuICBtYXJnaW46IDA7XFxufVxcblxcbi8qXFxuKkRlY2xhcmFjacOzbiBkZSBsb3MgY29sb3JlcyBkZWwgZG9jdW1lbnRvXFxuKi9cXG4vKlxcbiogRGVmaW5pY2nDs24gZGUgbGEgZnVlbnRlIGltcG9ydGFkYS5cXG4qIFxcbiogUGFyYSB1c2FybGEsIGNvcGlhciB5IHBlZ2FyIGxhIHByaW1lcmEgbMOtbmVhIGVuIGRvbmRlIHNlIHZheWEgYSB1c2FyIGRpY2hhIGZ1ZW50ZS5cXG4qIFNpIHNlIHZhbiBhIGltcG9ydGEgICByIG3DoXMgZnVlbnRlcywgZGViZW4gcG9uZXJzZSBlbiBsYSBjYXJwZXRhIGZvbnRzIHkgc2VndWlyIGVzdGUgbWlzbW8gcHJvY2Vzby5cXG4qL1xcbkBmb250LWZhY2Uge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJFYXN0bWFuXFxcIjtcXG4gIHNyYzogdXJsKFwiICsgX19fQ1NTX0xPQURFUl9VUkxfUkVQTEFDRU1FTlRfMF9fXyArIFwiKSBmb3JtYXQoXFxcIm90ZlxcXCIpO1xcbiAgZm9udC13ZWlnaHQ6IDYwMDtcXG4gIGZvbnQtc3R5bGU6IG5vcm1hbDtcXG59XFxuaHRtbCB7XFxuICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xcbiAgc2Nyb2xsLWJlaGF2aW9yOiBzbW9vdGg7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxufVxcblxcbmEge1xcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcbn1cXG5cXG4uY29udGFpbmVyIHtcXG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbn1cXG5cXG4ubnBhZ2UgaDEge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJFYXN0bWFuXFxcIjtcXG4gIGZvbnQtc2l6ZTogMzVweDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICBjb2xvcjogIzc5N2FhMztcXG59XFxuXFxuI2hlYWRlciB7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICB6LWluZGV4OiAxMDAwO1xcbiAgbGVmdDogMDtcXG4gIHRvcDogMDtcXG4gIHdpZHRoOiAxMDB2dztcXG4gIGhlaWdodDogYXV0bztcXG59XFxuI2hlYWRlciAuaGVhZGVyIHtcXG4gIG1pbi1oZWlnaHQ6IDh2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNjYWU3YjksIDA7XFxuICB0cmFuc2l0aW9uOiAwLjNzIGVhc2UgYmFja2dyb3VuZC1jb2xvcjtcXG59XFxuI2hlYWRlciAubmF2LWJhciB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgbWF4LXdpZHRoOiAxMzAwcHg7XFxuICBwYWRkaW5nOiAwIDEwcHg7XFxufVxcbiNoZWFkZXIgLm5hdi1iYXIgLm5wYWdlIHtcXG4gIGN1cnNvcjogcG9pbnRlcjtcXG59XFxuI2hlYWRlciAubmF2LWxpc3QgdWwge1xcbiAgbGlzdC1zdHlsZTogbm9uZTtcXG4gIHBvc2l0aW9uOiBpbml0aWFsO1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgaGVpZ2h0OiBhdXRvO1xcbiAgbGVmdDogMTAwJTtcXG4gIHRvcDogMDtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHotaW5kZXg6IDE7XFxuICBvdmVyZmxvdy14OiBoaWRkZW47XFxuICB0cmFuc2l0aW9uOiAwLjVzIGVhc2UgbGVmdDtcXG59XFxuI2hlYWRlciAubmF2LWxpc3QgdWw6YWN0aXZlIHtcXG4gIGxlZnQ6IDAlO1xcbn1cXG4jaGVhZGVyIC5uYXYtbGlzdCB1bCBsaSB7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XFxufVxcbiNoZWFkZXIgLm5hdi1saXN0IHVsIGxpOmhvdmVyIGEge1xcbiAgY29sb3I6ICM3OTdhYTM7XFxufVxcbiNoZWFkZXIgLm5hdi1saXN0IHVsIGxpOmhvdmVyIGE6YWZ0ZXIge1xcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSkgc2NhbGUoMSk7XFxuICBsZXR0ZXItc3BhY2luZzogaW5pdGlhbDtcXG59XFxuI2hlYWRlciAubmF2LWxpc3QgdWwgbGkgYSB7XFxuICBmb250LXNpemU6IDE3cHg7XFxuICBmb250LXdlaWdodDogNzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXG4gIGNvbG9yOiAjNmJhMGM4O1xcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXG4gIHBhZGRpbmc6IDIwcHg7XFxuICBkaXNwbGF5OiBibG9jaztcXG4gIHRyYW5zaXRpb246IDAuM3MgZWFzZTtcXG59XFxuXFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIHtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XFxuICBtYXgtd2lkdGg6IDEyMDBweDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcGFkZGluZzogMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGZsZXgtZmxvdzogd3JhcCByb3c7XFxufVxcbiNwcmVzZW50YXRpb24gLnByZXNlbnRhdGlvbiAucGFycmFmbyB7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHdpZHRoOiA0MTBweDtcXG4gIGhlaWdodDogNjUwcHg7XFxuICBmbGV4LWZsb3c6IHdyYXAgcm93O1xcbiAgcGFkZGluZzogMTBweDtcXG4gIGxlZnQ6IDYwcHg7XFxuICBtYXJnaW4tdG9wOiAxMHB4O1xcbiAgYm9yZGVyLXJhZGl1czogNDBweDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5wYXJyYWZvICNwcmVzZW50YXRpb25UaXRsZSB7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBmb250LXNpemU6IDcwcHg7XFxuICB3aWR0aDogMTAwJTtcXG4gIGhlaWdodDogNTBweDtcXG4gIGNvbG9yOiAjNzk3YWEzO1xcbiAgbWFyZ2luLXRvcDogLTQ0MHB4O1xcbn1cXG4jcHJlc2VudGF0aW9uIC5wcmVzZW50YXRpb24gLnBhcnJhZm8gLnByZXNlbnRhdGlvbkljb25Db2xvciB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDI1MHB4O1xcbiAgbGVmdDogMzBweDtcXG4gIG9wYWNpdHk6IDA7XFxuICB0cmFuc2l0aW9uOiAwLjVzIGVhc2U7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xcbn1cXG4jcHJlc2VudGF0aW9uIC5wcmVzZW50YXRpb24gLnBhcnJhZm8gLnByZXNlbnRhdGlvbkljb25Db2xvciBpbWcge1xcbiAgaGVpZ2h0OiAxMjBweDtcXG4gIG1hcmdpbjogMjBweDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5wYXJyYWZvIC5wcmVzZW50YXRpb25JY29uQ29sb3I6aG92ZXIge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoMTVkZWcpO1xcbn1cXG4jcHJlc2VudGF0aW9uIC5wcmVzZW50YXRpb24gLnBhcnJhZm8gLnByZXNlbnRhdGlvbkljb24ge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgdG9wOiAyNTBweDtcXG4gIGxlZnQ6IDMwcHg7XFxufVxcbiNwcmVzZW50YXRpb24gLnByZXNlbnRhdGlvbiAucGFycmFmbyAucHJlc2VudGF0aW9uSWNvbiBpbWcge1xcbiAgaGVpZ2h0OiAxMjBweDtcXG4gIG1hcmdpbjogMjBweDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMge1xcbiAgcG9zaXRpb246IGFic29sdXRlO1xcbiAgZmxleC1mbG93OiB3cmFwIHJvdztcXG4gIG1hcmdpbi10b3A6IDEwcHg7XFxuICB3aWR0aDogNjAwcHg7XFxuICBoZWlnaHQ6IDUwMHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtYWxpZ246IHN0YXJ0O1xcbiAgcGFkZGluZy1sZWZ0OiAxNXB4O1xcbiAgcmlnaHQ6IDYwcHg7XFxufVxcbiNwcmVzZW50YXRpb24gLnByZXNlbnRhdGlvbiAuc2tpbGxzIC5za2lsbFRpdGxlIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGhlaWdodDogNzBweDtcXG4gIHdpZHRoOiA5MCU7XFxuICBtYXJnaW4tYm90dG9tOiAtNzBweDtcXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XFxuICBsaW5lLWhlaWdodDogNzBweDtcXG4gIGZvbnQtc2l6ZTogNTBweDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLnByb2ctbGFuZywgI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLmFkb2JlIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBmbGV4LWZsb3c6IGNvbHVtbiBub3dyYXA7XFxuICB3aWR0aDogNTAlO1xcbn1cXG4jcHJlc2VudGF0aW9uIC5wcmVzZW50YXRpb24gLnNraWxscyAucHJvZy1sYW5nIC5iYXIsICNwcmVzZW50YXRpb24gLnByZXNlbnRhdGlvbiAuc2tpbGxzIC5hZG9iZSAuYmFyIHtcXG4gIGhlaWdodDogNDBweDtcXG4gIG1hcmdpbjogMnB4IDAgMnB4IDA7XFxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xcbiAgd2lkdGg6IDgwJTtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLnByb2ctbGFuZyAuYmFyIC5maWxsZXIsICNwcmVzZW50YXRpb24gLnByZXNlbnRhdGlvbiAuc2tpbGxzIC5hZG9iZSAuYmFyIC5maWxsZXIge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDAlO1xcbiAgdHJhbnNpdGlvbjogMC41cyBlYXNlO1xcbiAgYm9yZGVyLXJhZGl1czogMTJweCAwIDAgMTJweDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLnNraWxsLXRpdGxlIHtcXG4gIG1hcmdpbjogNyUgMCA4JSAwO1xcbiAgZm9udC1zaXplOiAzNXB4O1xcbn1cXG4jcHJlc2VudGF0aW9uIC5wcmVzZW50YXRpb24gLnNraWxscyAucHJvZy1sYW5nIC5iYXIge1xcbiAgYm9yZGVyOiAycHggIzAwMDAwMCBzb2xpZDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLnByb2ctbGFuZyAuYmFyIC5maWxsZXIge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogIzZiYTBjODtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLmFkb2JlIC5iYXIge1xcbiAgYm9yZGVyOiAycHggIzAwMDAwMCBzb2xpZDtcXG59XFxuI3ByZXNlbnRhdGlvbiAucHJlc2VudGF0aW9uIC5za2lsbHMgLmFkb2JlIC5iYXIgLmZpbGxlciB7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2FlN2I5O1xcbn1cXG5cXG4jYmlvZ3JhcGh5IC5iaW9ncmFwaHkge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgbWF4LXdpZHRoOiAxMjAwcHg7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIHBhZGRpbmc6IDEwMHB4IDA7XFxufVxcbiNiaW9ncmFwaHkgLmJpb2dyYXBoeSAjVGl0dWxvX0JpbyB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBmb250LXNpemU6IDcwcHg7XFxuICBib3R0b206IDQwMHB4O1xcbiAgd2lkdGg6IDEwMHZ3O1xcbiAgdG9wOiA5NXB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcbiNiaW9ncmFwaHkgLmJpb2dyYXBoeSAuRXhwX0VkdWMge1xcbiAgcGFkZGluZzogMHB4O1xcbiAgbWFyZ2luOiAxMHB4O1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgbGVmdDogMDtcXG4gIHRvcDogMzVweDtcXG4gIGhlaWdodDogNzUwcHg7XFxuICB3aWR0aDogNDV2dztcXG59XFxuI2Jpb2dyYXBoeSAuYmlvZ3JhcGh5IC5FeHBfRWR1YyAjT3JnYW5pemFkb3JSb21ib3Mge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXG4gIGZsZXgtd3JhcDogd3JhcDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHRvcDogMTcwcHg7XFxuICB3aWR0aDogNjAwcHg7XFxuICBoZWlnaHQ6IDYwMHB4O1xcbn1cXG4jYmlvZ3JhcGh5IC5iaW9ncmFwaHkgLkV4cF9FZHVjICNPcmdhbml6YWRvclJvbWJvcyAuUm9tYm9Tb2xpdGFyaW8ge1xcbiAgb3BhY2l0eTogMDtcXG4gIGxlZnQ6IDIwMHB4O1xcbiAgdG9wOiAyMDBweDtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB3aWR0aDogMTY1cHg7XFxuICBoZWlnaHQ6IDE2NXB4O1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpIHNjYWxlKDAsIDApO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ViOTQ4NjtcXG4gIHRyYW5zaXRpb246IDAuNXMgZWFzZTtcXG59XFxuI2Jpb2dyYXBoeSAuYmlvZ3JhcGh5IC5FeHBfRWR1YyAjT3JnYW5pemFkb3JSb21ib3MgLlJvbWJvU29saXRhcmlvOmhvdmVyIHtcXG4gIHRvcDogMTkwcHg7XFxuICBib3gtc2hhZG93OiA4cHggOHB4IDVweCAwcHggIzc5N2FhMztcXG59XFxuI2Jpb2dyYXBoeSAuYmlvZ3JhcGh5IC5FeHBfRWR1YyAjT3JnYW5pemFkb3JSb21ib3MgLlJvbWJvU29saXRhcmlvIC5UZXh0b0luZm8ge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcXG4gIGZvbnQtc2l6ZTogMThweDtcXG59XFxuI2Jpb2dyYXBoeSAuYmlvZ3JhcGh5IC5FeHBfRWR1YyAjT3JnYW5pemFkb3JSb21ib3MgLlJvbWJvSW5mbyB7XFxuICBsZWZ0OiAyMHB4O1xcbiAgdG9wOiAwO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIHBhZGRpbmc6IDA7XFxuICBtYXJnaW46IDUwcHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB3aWR0aDogMTY1cHg7XFxuICBoZWlnaHQ6IDE2NXB4O1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpIHNjYWxlKDAsIDApO1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ViOTQ4NjtcXG4gIHRyYW5zaXRpb246IDAuNXMgZWFzZTtcXG4gIG9wYWNpdHk6IDA7XFxufVxcbiNiaW9ncmFwaHkgLmJpb2dyYXBoeSAuRXhwX0VkdWMgI09yZ2FuaXphZG9yUm9tYm9zIC5Sb21ib0luZm86aG92ZXIge1xcbiAgdG9wOiAtMTBweDtcXG4gIGJveC1zaGFkb3c6IDhweCA4cHggNXB4IDBweCAjNzk3YWEzO1xcbn1cXG4jYmlvZ3JhcGh5IC5iaW9ncmFwaHkgLkV4cF9FZHVjICNPcmdhbml6YWRvclJvbWJvcyAuUm9tYm9JbmZvIC5UZXh0b0luZm8ge1xcbiAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcXG4gIGZvbnQtc2l6ZTogMThweDtcXG59XFxuI2Jpb2dyYXBoeSAuYmlvZ3JhcGh5IC5FeHBfRWR1YyAjVGl0dWxvX0V4cCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBjb2xvcjogIzAwMDAwMDtcXG4gIGZvbnQtc2l6ZTogNzBweDtcXG4gIGJvdHRvbTogNDAwcHg7XFxuICB3aWR0aDogMTAwdnc7XFxuICB0b3A6IDYwcHg7XFxuICBsZWZ0OiA0MHB4O1xcbiAgZm9udC13ZWlnaHQ6IHVuc2V0O1xcbn1cXG4jYmlvZ3JhcGh5IC5iaW9ncmFwaHkgLkV4cF9FZHVjICNUaXR1bG9fRWR1IHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIGNvbG9yOiAjMDAwMDAwO1xcbiAgZm9udC1zaXplOiA3MHB4O1xcbiAgYm90dG9tOiA0MDBweDtcXG4gIHdpZHRoOiAxMDB2dztcXG4gIHRvcDogNjBweDtcXG4gIGxlZnQ6IDExMHB4O1xcbiAgZm9udC13ZWlnaHQ6IHVuc2V0O1xcbn1cXG5cXG4jcHJvamVjdHMgLnByb2plY3RzIHtcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBtYXgtd2lkdGg6IDEyMDBweDtcXG4gIG1hcmdpbjogMCBhdXRvO1xcbiAgcGFkZGluZzogMTAwcHggMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuI3Byb2plY3RzIC5wcm9qZWN0cyAucHJvamVjdHMtaGVhZGVyIHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG59XFxuI3Byb2plY3RzIC5wcm9qZWN0cyAucHJvamVjdHMtaGVhZGVyIGgxIHtcXG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLnByb2plY3RzLWhlYWRlciBoMSBzcGFuIHtcXG4gIGNvbG9yOiAjY2FlN2I5O1xcbn1cXG4jcHJvamVjdHMgLnByb2plY3RzIC5wcm9qZWN0cy10aXRsZSB7XFxuICBmb250LXNpemU6IDcwcHg7XFxuICBmb250LXdlaWdodDogYm9sZDtcXG4gIGNvbG9yOiAjMDAwMDAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICBsZXR0ZXItc3BhY2luZzogMC4ycmVtO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4jcHJvamVjdHMgLnByb2plY3RzIC5hbGwtcHJvamVjdHMge1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xcbiAgaGVpZ2h0OiA0MDBweDtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgbWFyZ2luOiAwO1xcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcXG4gIGJvcmRlci1yYWRpdXM6IDA7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtOm50aC1jaGlsZChldmVuKSB7XFxuICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWluZm8ge1xcbiAgcGFkZGluZzogMzBweDtcXG4gIGZsZXgtYmFzaXM6IDUwJTtcXG4gIGhlaWdodDogMTAwJTtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCg2MGRlZywgIzZiYTBjOCAwJSwgIzc5N2FhMyAxMDAlKTtcXG4gIGNvbG9yOiAjZmZmZmZmO1xcbn1cXG4jcHJvamVjdHMgLnByb2plY3RzIC5hbGwtcHJvamVjdHMgLnByb2plY3QtaXRlbSAucHJvamVjdC1pbmZvIC5wdGl0dWxvIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgZm9udC1zaXplOiA0cmVtO1xcbiAgZm9udC13ZWlnaHQ6IDUwMDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIGNvbG9yOiB0cmFuc3BhcmVudDtcXG4gIC8qYW5pbWF0aW9uOiB0ZXh0X3JldmVhbCAuNXMgZWFzZSBmb3J3YXJkczsqL1xcbiAgYW5pbWF0aW9uLWRlbGF5OiAxcztcXG59XFxuI3Byb2plY3RzIC5wcm9qZWN0cyAuYWxsLXByb2plY3RzIC5wcm9qZWN0LWl0ZW0gLnByb2plY3QtaW5mbyAucHRpdHVsbyAuYW5pbWF0aW9uUCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xcbiAgLyphbmltYXRpb246IHRleHRfcmV2ZWFsX2JveCAxcyBlYXNlOyovXFxuICBhbmltYXRpb24tZGVsYXk6IDAuNXM7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWluZm8gLnBzdWJ0aXR1bG8ge1xcbiAgZGlzcGxheTogYmxvY2s7XFxuICB3aWR0aDogZml0LWNvbnRlbnQ7XFxuICBmb250LXNpemU6IDEuOHJlbTtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBtYXJnaW4tdG9wOiAxMHB4O1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgY29sb3I6IHRyYW5zcGFyZW50O1xcbiAgLyphbmltYXRpb246IHRleHRfcmV2ZWFsIC41cyBlYXNlIGZvcndhcmRzOyovXFxuICBhbmltYXRpb24tZGVsYXk6IDFzO1xcbn1cXG4jcHJvamVjdHMgLnByb2plY3RzIC5hbGwtcHJvamVjdHMgLnByb2plY3QtaXRlbSAucHJvamVjdC1pbmZvIC5wc3VidGl0dWxvIC5hbmltYXRpb25QIHtcXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXG4gIHRvcDogMDtcXG4gIGxlZnQ6IDA7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB3aWR0aDogMDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XFxuICAvKmFuaW1hdGlvbjogdGV4dF9yZXZlYWxfYm94IDFzIGVhc2U7Ki9cXG4gIGFuaW1hdGlvbi1kZWxheTogMC41cztcXG59XFxuI3Byb2plY3RzIC5wcm9qZWN0cyAuYWxsLXByb2plY3RzIC5wcm9qZWN0LWl0ZW0gLnByb2plY3QtaW5mbyAucHRleHRvIHtcXG4gIGRpc3BsYXk6IGJsb2NrO1xcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xcbiAgY29sb3I6ICNmZmZmZmY7XFxuICBmb250LXNpemU6IDEuNHJlbTtcXG4gIG1hcmdpbi10b3A6IDVweDtcXG4gIGxpbmUtaGVpZ2h0OiAyLjVyZW07XFxuICBmb250LXdlaWdodDogMzAwO1xcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMDVyZW07XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBjb2xvcjogdHJhbnNwYXJlbnQ7XFxuICAvKmFuaW1hdGlvbjogdGV4dF9yZXZlYWwgLjVzIGVhc2UgZm9yd2FyZHM7Ki9cXG4gIGFuaW1hdGlvbi1kZWxheTogMXM7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWluZm8gLnB0ZXh0byAuYW5pbWF0aW9uUCB7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB0b3A6IDA7XFxuICBsZWZ0OiAwO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDA7XFxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xcbiAgLyphbmltYXRpb246IHRleHRfcmV2ZWFsX2JveCAxcyBlYXNlOyovXFxuICBhbmltYXRpb24tZGVsYXk6IDAuNXM7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWltZyB7XFxuICBmbGV4LWJhc2lzOiA1MCU7XFxuICBoZWlnaHQ6IDEwMCU7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbn1cXG4jcHJvamVjdHMgLnByb2plY3RzIC5hbGwtcHJvamVjdHMgLnByb2plY3QtaXRlbSAucHJvamVjdC1pbWc6YWZ0ZXIge1xcbiAgY29udGVudDogXFxcIlxcXCI7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICBsZWZ0OiAwO1xcbiAgdG9wOiAwO1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNjBkZWcsICM0NThmZTQgMCUsICM0ODU1NjMgMTAwJSk7XFxuICBvcGFjaXR5OiAwLjQ7XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWltZyBpbWcge1xcbiAgaGVpZ2h0OiAxMDAlO1xcbiAgd2lkdGg6IDEwMCU7XFxuICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gIHRyYW5zaXRpb246IDAuM3MgZWFzZSB0cmFuc2Zvcm07XFxufVxcbiNwcm9qZWN0cyAucHJvamVjdHMgLmFsbC1wcm9qZWN0cyAucHJvamVjdC1pdGVtIC5wcm9qZWN0LWltZzpob3ZlciBpbWcge1xcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjEpO1xcbn1cXG5cXG4jcGhvdG9zIC5waG90b3Mge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1heC13aWR0aDogMTIwMHB4O1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwYWRkaW5nOiAxMDBweCAwO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgZmxleC1mbG93OiB3cmFwIHJvdztcXG59XFxuI3Bob3RvcyAucGhvdG9zICNwaG90b1RpdGxlIHtcXG4gIHdpZHRoOiAxMDB2dztcXG4gIGZvbnQtc2l6ZTogNjBweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcXG4gIG1hcmdpbjogMTBweDtcXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxufVxcbiNwaG90b3MgLnBob3RvcyAjcGhvdG9TdWJUaXRsZSB7XFxuICB3aWR0aDogMTAwdnc7XFxuICBmb250LXNpemU6IDMwcHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB0ZXh0LWFsaWduOiBzdGFydDtcXG4gIG1hcmdpbjogMTBweDtcXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcXG59XFxuI3Bob3RvcyAucGhvdG9zIC5kaXNwbGF5IHtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHRvcDogMHB4O1xcbiAgbGVmdDogMHB4O1xcbiAgaGVpZ2h0OiA2NTBweDtcXG4gIHdpZHRoOiAxMDB2dztcXG4gIGRpc3BsYXk6IGdyaWQ7XFxuICBncmlkLWNvbHVtbjogNDtcXG4gIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMzUwcHggYXV0byAzNTBweDtcXG4gIGdyaWQtdGVtcGxhdGUtcm93czogMzAwcHggMzAwcHg7XFxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XFxuICBncmlkLXJvdy1nYXA6IDUwcHg7XFxuICBwYWRkaW5nOiAxNXB4O1xcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xcbiAgb3BhY2l0eTogMDtcXG4gIHRyYW5zaXRpb246IDAuOHMgZWFzZTtcXG59XFxuI3Bob3RvcyAucGhvdG9zIC5kaXNwbGF5IC5kaXNwbGF5UGhvdG9CaWcge1xcbiAgZ3JpZC1jb2x1bW4tc3RhcnQ6IDI7XFxuICBncmlkLWNvbHVtbi1lbmQ6IDM7XFxuICBncmlkLXJvdy1zdGFydDogMTtcXG4gIGdyaWQtcm93LWVuZDogMztcXG4gIG92ZXJmbG93LXk6IGhpZGRlbjtcXG59XFxuI3Bob3RvcyAucGhvdG9zIC5kaXNwbGF5IC5kaXNwbGF5UGhvdG9CaWcgaW1nIHtcXG4gIG1heC13aWR0aDogOTYlO1xcbiAgbWF4LWhlaWdodDogOTglO1xcbn1cXG4jcGhvdG9zIC5waG90b3MgLmRpc3BsYXkgLmRpc3BsYXlQaG90byB7XFxuICBvdmVyZmxvdy15OiBoaWRkZW47XFxuICBoZWlnaHQ6IDEwMCU7XFxuICB3aWR0aDogMTAwJTtcXG59XFxuI3Bob3RvcyAucGhvdG9zIC5kaXNwbGF5IC5kaXNwbGF5UGhvdG8gaW1nIHtcXG4gIG1heC13aWR0aDogOTclO1xcbiAgbWF4LWhlaWdodDogOTclO1xcbn1cXG4jcGhvdG9zIC5waG90b3MgLmRpc3BsYXkgaW1nIHtcXG4gIG1heC13aWR0aDogOTclO1xcbiAgbWF4LWhlaWdodDogOTclO1xcbiAgYm94LXNoYWRvdzogMTBweCAxMHB4IDVweCAwcHggIzc5N2FhMztcXG4gIHRyYW5zaXRpb246IDAuNXMgZWFzZTtcXG59XFxuI3Bob3RvcyAucGhvdG9zIC5kaXNwbGF5IGltZzpob3ZlciB7XFxuICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUsIDAuOTUpO1xcbiAgYm94LXNoYWRvdzogNHB4IDRweCA1cHggMHB4ICM3OTdhYTM7XFxufVxcblxcbiN2aWRlbyAudmlkZW8ge1xcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxuICBmbGV4LWZsb3c6IHdyYXAgcm93O1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIG1heC13aWR0aDogMTIwMHB4O1xcbiAgbWFyZ2luOiAwIGF1dG87XFxuICBwYWRkaW5nOiAxMDBweCAwO1xcbiAgZGlzcGxheTogZmxleDtcXG59XFxuI3ZpZGVvIC52aWRlbyAjdmlkZW9UaXRsZSB7XFxuICB3aWR0aDogMTAwdnc7XFxuICBmb250LXNpemU6IDYwcHg7XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xcbiAgbWFyZ2luLXRvcDogMjBweDtcXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xcbn1cXG4jdmlkZW8gLnZpZGVvICN2aWRlb1N1YlRpdGxlIHtcXG4gIHdpZHRoOiAxMDB2dztcXG4gIGZvbnQtc2l6ZTogMzBweDtcXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcXG4gIHRleHQtYWxpZ246IGp1c3RpZnk7XFxuICBtYXJnaW4tdG9wOiAwcHg7XFxuICBwYWRkaW5nLXJpZ2h0OiA1MHB4O1xcbiAgcGFkZGluZy1sZWZ0OiA1MHB4O1xcbn1cXG4jdmlkZW8gLnZpZGVvIC5wbGF5ZXIge1xcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xcbiAgd2lkdGg6IDEwMHZ3O1xcbiAgaGVpZ2h0OiA2NTBweDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgdHJhbnNpdGlvbjogMC4ycyBlYXNlO1xcbiAgb3BhY2l0eTogMDtcXG4gIHRyYW5zaXRpb246IDFzIGVhc2U7XFxuICBjdXJzb3I6IHBvaW50ZXI7XFxufVxcbiN2aWRlbyAudmlkZW8gLnBsYXllciB2aWRlbyB7XFxuICBoZWlnaHQ6IDU4MHB4O1xcbn1cXG5cXG4jZm9vdGVyIHtcXG4gIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudCg2MGRlZywgI2NhZTdiOSAwJSwgI2ZmZWRhNSAxMDAlKTtcXG59XFxuI2Zvb3RlciAuZm9vdGVyIHtcXG4gIG1pbi1oZWlnaHQ6IDIwMHB4O1xcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXG4gIHBhZGRpbmctdG9wOiAzMHB4O1xcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XFxufVxcbiNmb290ZXIgLmZvb3RlciBoMiB7XFxuICBjb2xvcjogIzAwMDAwMDtcXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICBmb250LXNpemU6IDEuOHJlbTtcXG4gIGxldHRlci1zcGFjaW5nOiAwLjFyZW07XFxuICBtYXJnaW4tdG9wOiAxMHB4O1xcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcXG59XFxuI2Zvb3RlciAuZm9vdGVyIC5mb290ZXItdGl0bGUge1xcbiAgZm9udC1mYW1pbHk6IFxcXCJFYXN0bWFuXFxcIjtcXG4gIGZvbnQtc2l6ZTogMi44cmVtO1xcbiAgZm9udC13ZWlnaHQ6IDMwMDtcXG4gIGNvbG9yOiAjMDAwMDAwO1xcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcXG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxuICBsZXR0ZXItc3BhY2luZzogMC4ycmVtO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbn1cXG4jZm9vdGVyIC5mb290ZXIgLnNvY2lhbC1pY29uIHtcXG4gIGRpc3BsYXk6IGZsZXg7XFxuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xcbn1cXG4jZm9vdGVyIC5mb290ZXIgLnNvY2lhbC1pY29uIC5zb2NpYWwtaXRlbSB7XFxuICBoZWlnaHQ6IDUwcHg7XFxuICB3aWR0aDogNTBweDtcXG4gIG1hcmdpbjogMCA1cHg7XFxufVxcbiNmb290ZXIgLmZvb3RlciAuc29jaWFsLWljb24gLnNvY2lhbC1pdGVtIGltZyB7XFxuICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcXG4gIHRyYW5zaXRpb246IDAuM3MgZWFzZSBmaWx0ZXI7XFxufVxcbiNmb290ZXIgLmZvb3RlciAuc29jaWFsLWljb24gLnNvY2lhbC1pdGVtIGltZzpob3ZlciBpbWcge1xcbiAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XFxufVxcbiNmb290ZXIgLmZvb3RlciBwIHtcXG4gIGNvbG9yOiAjMDAwMDAwO1xcbiAgZm9udC1zaXplOiAxLjNyZW07XFxufVxcblxcbi8qa2V5ZnJhbWVzKi9cXG5Aa2V5ZnJhbWVzIHRleHRfcmV2ZWFsX2JveCB7XFxuICA1MCUge1xcbiAgICB3aWR0aDogMTAwJTtcXG4gICAgbGVmdDogMDtcXG4gIH1cXG4gIDEwMCUge1xcbiAgICB3aWR0aDogMDtcXG4gICAgbGVmdDogMTAwJTtcXG4gIH1cXG59XFxuQGtleWZyYW1lcyB0ZXh0X3JldmVhbCB7XFxuICAxMDAlIHtcXG4gICAgY29sb3I6ICNmZmZmZmY7XFxuICB9XFxufVxcbkBrZXlmcmFtZXMgdGV4dF9yZXZlYWxfbmFtZSB7XFxuICAxMDAlIHtcXG4gICAgY29sb3I6IGNyaW1zb247XFxuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XFxuICB9XFxufVwiLCBcIlwiLHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIndlYnBhY2s6Ly9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL0hvbWVQYWdlLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsZ0JBQWdCO0FBQWhCO0VBQ0ksc0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUVBOztDQUFBO0FBYUE7Ozs7O0NBQUE7QUFNQTtFQUNJLHNCQUFBO0VBQ0EsMERBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBVEo7QUFtQkE7RUFDSSxzQkFBQTtFQUVBLHVCQUFBO0VBQ0Esa0JBQUE7QUFsQko7O0FBcUJBO0VBQ0kscUJBQUE7QUFsQko7O0FBcUJBO0VBQ0ksaUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUFsQko7O0FBc0JJO0VBQ0ksc0JBQUE7RUFDQSxlQUFBO0VBQ0gseUJBQUE7RUFDQSxjQXJEUTtBQWtDYjs7QUF1QkE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLE9BQUE7RUFDQSxNQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUFwQko7QUFxQkk7RUFDSSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQ0FBQTtBQW5CUjtBQXNCSTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFwQlI7QUFxQlE7RUFDSSxlQUFBO0FBbkJaO0FBdUJRO0VBQ0ksZ0JBQUE7RUFDSCxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxNQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNHLDBCQUFBO0FBckJaO0FBc0JZO0VBQ0ksUUFBQTtBQXBCaEI7QUFzQlk7RUFDSSxxQkFBQTtBQXBCaEI7QUFzQm9CO0VBQ0ksY0F4R1g7QUFvRmI7QUFxQndCO0VBQ0kseUNBQUE7RUFDSCx1QkFBQTtBQW5CekI7QUF1QmdCO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBbkhKO0VBb0hJLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxxQkFBQTtBQXJCcEI7O0FBK0JJO0VBRUkseUJBcklHO0VBdUlILGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBOUJSO0FBZ0NRO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQTlCWjtBQWtDWTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsY0FqS0g7RUFrS0csa0JBQUE7QUFoQ2hCO0FBb0NZO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUVBLHlCQTlLTDtBQTJJWDtBQXFDZ0I7RUFDSSxhQUFBO0VBQ0EsWUFBQTtBQW5DcEI7QUFvQ2lCO0VBQ0csd0JBQUE7QUFsQ3BCO0FBdUNZO0VBQ0ksa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtBQXJDaEI7QUF1Q2dCO0VBQ0ksYUFBQTtFQUNBLFlBQUE7QUFyQ3BCO0FBMkNRO0VBQ0ksa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUF6Q1o7QUE0Q1k7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQTFDaEI7QUErQ1k7RUFDSSxhQUFBO0VBQ0Esd0JBQUE7RUFDQSxVQUFBO0FBN0NoQjtBQStDZ0I7RUFDSSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUE3Q3BCO0FBOENvQjtFQUNJLFlBQUE7RUFDQSxTQUFBO0VBQ0EscUJBQUE7RUFDQSw0QkFBQTtBQTVDeEI7QUFpRFk7RUFDSSxpQkFBQTtFQUNBLGVBQUE7QUEvQ2hCO0FBa0RZO0VBQ0kseUJBQUE7QUFoRGhCO0FBaURnQjtFQUNJLHlCQXpQSjtBQTBNaEI7QUFrRFk7RUFDSSx5QkFBQTtBQWhEaEI7QUFpRGdCO0VBQ0kseUJBN1BKO0FBOE1oQjs7QUF3REk7RUFDSSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNILGlCQUFBO0VBQ0EsY0FBQTtFQUNHLGdCQUFBO0FBckRSO0FBdURRO0VBQ0ksa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQXJEWjtBQXdEUTtFQUNJLFlBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLGFBQUE7RUFDQSxXQUFBO0FBdERaO0FBd0RZO0VBQ0ksYUFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FBdERoQjtBQXdEZ0I7RUFDSSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG9DQUFBO0VBQ0EseUJBdlRIO0VBd1RHLHFCQUFBO0FBdERwQjtBQXVEb0I7RUFDSSxVQUFBO0VBQ0EsbUNBQUE7QUFyRHhCO0FBd0RvQjtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtBQXREeEI7QUEwRGdCO0VBQ0ksVUFBQTtFQUNBLE1BQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esb0NBQUE7RUFDQSx5QkFqVkg7RUFrVkcscUJBQUE7RUFDQSxVQUFBO0FBeERwQjtBQXlEb0I7RUFDSSxVQUFBO0VBQ0EsbUNBQUE7QUF2RHhCO0FBeURvQjtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtBQXZEeEI7QUE0RFk7RUFDSSxrQkFBQTtFQUNBLGNBbldKO0VBb1dJLGVBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7QUExRGhCO0FBNkRZO0VBQ0ksa0JBQUE7RUFDQSxjQTlXSjtFQStXSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBM0RoQjs7QUFvRUk7RUFDSSxzQkFBQTtFQUNILGlCQUFBO0VBQ0EsY0FBQTtFQUNHLGdCQUFBO0VBQ0Esa0JBQUE7QUFqRVI7QUFrRVE7RUFDSSxrQkFBQTtBQWhFWjtBQWlFWTtFQUNJLG1CQUFBO0FBL0RoQjtBQWdFZ0I7RUFDSSxjQXZZSjtBQXlVaEI7QUFrRVE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQS9ZQTtFQWdaQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQWhFWjtBQW1FUTtFQUNJLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUFqRVo7QUFrRVk7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtBQWhFaEI7QUFpRWdCO0VBQ0ksMkJBQUE7QUEvRHBCO0FBaUVnQjtFQUNJLGFBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrRUFBQTtFQUNBLGNBcGJUO0FBcVhYO0FBZ0VvQjtFQUNJLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDSCw0Q0FBQTtFQUNBLG1CQUFBO0FBOURyQjtBQStEd0I7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSx5QkFwY2pCO0VBcWNpQixzQ0FBQTtFQUNBLHFCQUFBO0FBN0Q1QjtBQWdFb0I7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNENBQUE7RUFDSCxtQkFBQTtBQTlEckI7QUErRHdCO0VBQ0ksa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EseUJBemRqQjtFQTBkaUIsc0NBQUE7RUFDQSxxQkFBQTtBQTdENUI7QUFnRW9CO0VBQ0ksY0FBQTtFQUNBLGtCQUFBO0VBQ0EsY0FqZWI7RUFrZWEsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDSCxrQkFBQTtFQUNBLDRDQUFBO0VBQ0EsbUJBQUE7QUE5RHJCO0FBK0R3QjtFQUNJLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLHlCQWpmakI7RUFrZmlCLHNDQUFBO0VBQ0EscUJBQUE7QUE3RDVCO0FBaUVnQjtFQUNJLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQS9EcEI7QUFnRW9CO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtFQUFBO0VBQ0EsWUFBQTtBQTlEeEI7QUFnRW9CO0VBQ0ksWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLCtCQUFBO0FBOUR4QjtBQWlFd0I7RUFDSSxxQkFBQTtBQS9ENUI7O0FBMEVJO0VBQ0kseUJBMWhCRztFQTJoQkgsa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FBdkVSO0FBeUVRO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUF2RVo7QUEyRVE7RUFDSSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQXpFWjtBQTRFUTtFQUNJLGtCQUFBO0VBR0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0EsdUNBQUE7RUFDQSwrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7QUE1RVo7QUErRVk7RUFDSSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUE3RWhCO0FBOEVnQjtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBNUVwQjtBQWlGWTtFQUVJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFoRmhCO0FBaUZnQjtFQUNJLGNBQUE7RUFDQSxlQUFBO0FBL0VwQjtBQW1GWTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EscUNBQUE7RUFDQSxxQkFBQTtBQWpGaEI7QUFrRmdCO0VBQ0ksNEJBQUE7RUFDQSxtQ0FBQTtBQWhGcEI7O0FBeUZJO0VBQ0kseUJBcm5CRztFQXNuQkgsc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtBQXRGUjtBQXdGUTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQXRGWjtBQTBGUTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBeEZaO0FBNkZRO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHlCQTNwQkQ7RUE0cEJDLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBM0ZaO0FBNEZZO0VBQ0ksYUFBQTtBQTFGaEI7O0FBZ0dBO0VBQ0ksa0VBQUE7QUE3Rko7QUE4Rkk7RUFDSSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQTVGUjtBQTZGUTtFQUNJLGNBL3FCQTtFQWdyQkEsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQTNGWjtBQThGUTtFQUNJLHNCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBM3JCQTtFQTRyQkEsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7QUE1Rlo7QUFpR1E7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUEvRlo7QUFnR1k7RUFDSSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7QUE5RmhCO0FBK0ZnQjtFQUNJLG9CQUFBO0VBQ0EsNEJBQUE7QUE3RnBCO0FBK0Z3QjtFQUNJLG9CQUFBO0FBN0Y1QjtBQW1HUTtFQUNJLGNBdnRCQTtFQXd0QkEsaUJBQUE7QUFqR1o7O0FBdUdBLFlBQUE7QUFDQTtFQUNDO0lBQ0MsV0FBQTtJQUNBLE9BQUE7RUFwR0E7RUFzR0Q7SUFDQyxRQUFBO0lBQ0EsVUFBQTtFQXBHQTtBQUNGO0FBc0dBO0VBQ0M7SUFDQyxjQTl1QlM7RUEwb0JUO0FBQ0Y7QUFzR0E7RUFDQztJQUNDLGNBQUE7SUFDQSxnQkFBQTtFQXBHQTtBQUNGXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIioge1xcclxcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcclxcbiAgICBwYWRkaW5nOiAwO1xcclxcbiAgICBtYXJnaW46IDA7XFxyXFxuICAgIFxcclxcbn1cXHJcXG5cXHJcXG4vKlxcclxcbipEZWNsYXJhY2nDs24gZGUgbG9zIGNvbG9yZXMgZGVsIGRvY3VtZW50b1xcclxcbiovXFxyXFxuJGJnLWNvbG9yOiAjZmZmZmZmO1xcclxcbiR0aXRsZS1jb2xvcjojNzk3YWEzO1xcclxcbiRzdWJ0aXRsZS1jb2xvcjojNmJhMGM4O1xcclxcbiR0ZXh0LWNvbG9yOiMwMDAwMDA7XFxyXFxuJHNlcGFydG9yLWNvbG9yOiNjYWU3Yjk7XFxyXFxuJE9EZXRhaWxzLWNvbG9yOiAjZWI5NDg2O1xcclxcbiRZRGV0YWlscy1jb2xvcjogI2ZmZWRhNTtcXHJcXG5cXHJcXG5cXHJcXG5cXHJcXG4vKlxcclxcbiogRGVmaW5pY2nDs24gZGUgbGEgZnVlbnRlIGltcG9ydGFkYS5cXHJcXG4qIFxcclxcbiogUGFyYSB1c2FybGEsIGNvcGlhciB5IHBlZ2FyIGxhIHByaW1lcmEgbMOtbmVhIGVuIGRvbmRlIHNlIHZheWEgYSB1c2FyIGRpY2hhIGZ1ZW50ZS5cXHJcXG4qIFNpIHNlIHZhbiBhIGltcG9ydGEgICByIG3DoXMgZnVlbnRlcywgZGViZW4gcG9uZXJzZSBlbiBsYSBjYXJwZXRhIGZvbnRzIHkgc2VndWlyIGVzdGUgbWlzbW8gcHJvY2Vzby5cXHJcXG4qL1xcclxcbkBmb250LWZhY2Uge1xcclxcbiAgICBmb250LWZhbWlseTogXFxcIkVhc3RtYW5cXFwiO1xcclxcbiAgICBzcmM6IHVybChcXFwiLi4vLi4vLi4vZm9udHMvRWFzdG1hblRyaWFsLUJsYWNrLm90ZlxcXCIpIGZvcm1hdChcXFwib3RmXFxcIik7XFxyXFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxyXFxuICAgIGZvbnQtc3R5bGU6IG5vcm1hbDtcXHJcXG59XFxyXFxuXFxyXFxuLy8gQGZvbnQtZmFjZSB7XFxyXFxuLy8gICAgIGZvbnQtZmFtaWx5OiBcXFwiQ29vbHZldGljYVxcXCI7XFxyXFxuLy8gICAgIHNyYzogdXJsKFxcXCIuLi8uLi8uLi9mb250cy9jb29sdmV0aWNhXFxcXCBjcmFtbWVkXFxcXCByZy50dGZcXFwiKSBmb3JtYXQoXFxcInR0ZlxcXCIpO1xcclxcbi8vICAgICBmb250LXdlaWdodDogNjAwO1xcclxcbi8vICAgICBmb250LXN0eWxlOiBub3JtYWw7XFxyXFxuLy8gfVxcclxcblxcclxcbmh0bWwge1xcclxcbiAgICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xcclxcbiAgICBcXHJcXG4gICAgc2Nyb2xsLWJlaGF2aW9yOiBzbW9vdGg7XFxyXFxuICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcXHJcXG59XFxyXFxuXFxyXFxuYSB7XFxyXFxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcXHJcXG59XFxyXFxuXFxyXFxuLmNvbnRhaW5lciB7XFxyXFxuICAgIG1pbi1oZWlnaHQ6IDEwMHZoO1xcclxcbiAgICB3aWR0aDogMTAwJTtcXHJcXG4gICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxyXFxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxufVxcclxcblxcclxcbi5ucGFnZXtcXHJcXG4gICAgaDF7XFxyXFxuICAgICAgICBmb250LWZhbWlseTpcXFwiRWFzdG1hblxcXCI7XFxyXFxuICAgICAgICBmb250LXNpemU6IDM1cHg7XFxyXFxuXFx0ICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxyXFxuXFx0ICAgIGNvbG9yOiAkdGl0bGUtY29sb3I7XFxyXFxuICAgIH1cXHJcXG59XFxyXFxuXFxyXFxuI2hlYWRlciB7XFxyXFxuICAgIHBvc2l0aW9uOiBmaXhlZDtcXHJcXG4gICAgei1pbmRleDogMTAwMDtcXHJcXG4gICAgbGVmdDogMDtcXHJcXG4gICAgdG9wOiAwO1xcclxcbiAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgIGhlaWdodDogYXV0bztcXHJcXG4gICAgLmhlYWRlciB7XFxyXFxuICAgICAgICBtaW4taGVpZ2h0OiA4dmg7XFxyXFxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkc2VwYXJ0b3ItY29sb3IsIDA7XFxyXFxuICAgICAgICB0cmFuc2l0aW9uOiAuM3MgZWFzZSBiYWNrZ3JvdW5kLWNvbG9yO1xcclxcbiAgICAgICAgLy8gYm94LXNoYWRvdzogMTBweCA1cHggMzBweCAkc2VwYXJ0b3ItY29sb3I7XFxyXFxuICAgIH1cXHJcXG4gICAgLm5hdi1iYXIge1xcclxcbiAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XFxyXFxuICAgICAgICB3aWR0aDogMTAwJTtcXHJcXG4gICAgICAgIGhlaWdodDogMTAwJTtcXHJcXG4gICAgICAgIG1heC13aWR0aDogMTMwMHB4O1xcclxcbiAgICAgICAgcGFkZGluZzogMCAxMHB4O1xcclxcbiAgICAgICAgLm5wYWdle1xcclxcbiAgICAgICAgICAgIGN1cnNvcjogcG9pbnRlcjtcXHJcXG4gICAgICAgIH1cXHJcXG4gICAgfVxcclxcbiAgICAubmF2LWxpc3R7XFxyXFxuICAgICAgICB1bHtcXHJcXG4gICAgICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xcclxcblxcdCAgICAgICAgcG9zaXRpb246IGluaXRpYWw7XFxyXFxuXFx0ICAgICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XFxyXFxuXFx0ICAgICAgICBoZWlnaHQ6IGF1dG87XFxyXFxuXFx0ICAgICAgICBsZWZ0OiAxMDAlO1xcclxcblxcdCAgICAgICAgdG9wOiAwO1xcclxcblxcdCAgICAgICAgZGlzcGxheTogYmxvY2s7XFxyXFxuXFx0ICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcclxcblxcdCAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxyXFxuXFx0ICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcclxcblxcdCAgICAgICAgei1pbmRleDogMTtcXHJcXG5cXHQgICAgICAgIG92ZXJmbG93LXg6IGhpZGRlbjtcXHJcXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAuNXMgZWFzZSBsZWZ0O1xcclxcbiAgICAgICAgICAgICY6YWN0aXZle1xcclxcbiAgICAgICAgICAgICAgICBsZWZ0OiAwJTtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgbGl7XFxyXFxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcXHJcXG4gICAgICAgICAgICAgICAgJjpob3ZlcntcXHJcXG4gICAgICAgICAgICAgICAgICAgIGF7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6ICR0aXRsZS1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAmOmFmdGVye1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKSBzY2FsZSgxKTtcXHJcXG5cXHQgICAgICAgICAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogaW5pdGlhbDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgYXtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMTdweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA3MDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xcclxcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRzdWJ0aXRsZS1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XFxyXFxuICAgICAgICAgICAgICAgICAgICBwYWRkaW5nOiAyMHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjNzIGVhc2U7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICB9XFxyXFxuICAgIH1cXHJcXG59XFxyXFxuXFxyXFxuXFxyXFxuI3ByZXNlbnRhdGlvbiB7XFxyXFxuXFxyXFxuICAgIC5wcmVzZW50YXRpb24ge1xcclxcbiAgICAgICBcXHJcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRiZy1jb2xvcjtcXHJcXG4gICAgICAgIC8vIGJhY2tncm91bmQtY29sb3I6ICRZRGV0YWlscy1jb2xvcjtcXHJcXG4gICAgICAgIG1heC13aWR0aDogMTIwMHB4O1xcclxcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XFxyXFxuICAgICAgICBwYWRkaW5nOiAwO1xcclxcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcclxcbiAgICAgICAgZmxleC1mbG93OiB3cmFwIHJvdztcXHJcXG4gICAgICAgIFxcclxcbiAgICAgICAgLnBhcnJhZm97XFxyXFxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDUwcHg7XFxyXFxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgIHdpZHRoOiA0MTBweDtcXHJcXG4gICAgICAgICAgICBoZWlnaHQ6IDY1MHB4O1xcclxcbiAgICAgICAgICAgIGZsZXgtZmxvdzogd3JhcCByb3c7XFxyXFxuICAgICAgICAgICAgcGFkZGluZzogMTBweDtcXHJcXG4gICAgICAgICAgICBsZWZ0OiA2MHB4O1xcclxcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XFxyXFxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogNDBweDsgXFxyXFxuICAgICAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcXHJcXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoMGRlZywgIzZiYTBjODgwIDAlLCAjY2FlN2I5ODAgIDEwMCUpO1xcclxcbiAgICAgICAgICAgIFxcclxcbiAgICAgICAgICAgICNwcmVzZW50YXRpb25UaXRsZXtcXHJcXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcclxcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDcwcHg7XFxyXFxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xcclxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDUwcHg7XFxyXFxuICAgICAgICAgICAgICAgIGNvbG9yOiAkdGl0bGUtY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IC00NDBweDtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgXFxyXFxuXFxyXFxuICAgICAgICAgICAgLnByZXNlbnRhdGlvbkljb25Db2xvcntcXHJcXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgICAgICB0b3A6IDI1MHB4O1xcclxcbiAgICAgICAgICAgICAgICBsZWZ0OiAzMHB4O1xcclxcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwO1xcclxcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjVzIGVhc2UgO1xcclxcbiAgICAgICAgICAgICAgICAvLyB0cmFuc2l0aW9uOiAwLjNzIGVhc2UgdHJhbnNmb3JtO1xcclxcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmctY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgIGltZ3tcXHJcXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTIwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICBtYXJnaW46IDIwcHg7XFxyXFxuICAgICAgICAgICAgICAgIH0mOmhvdmVye1xcclxcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMTVkZWcpO1xcclxcbiAgICAgICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgLnByZXNlbnRhdGlvbkljb257XFxyXFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICAgICAgdG9wOiAyNTBweDtcXHJcXG4gICAgICAgICAgICAgICAgbGVmdDogMzBweDtcXHJcXG4gICAgICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAgICAgaW1ne1xcclxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMjBweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjogMjBweDtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgXFxyXFxuICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAuc2tpbGxze1xcclxcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICBmbGV4LWZsb3c6IHdyYXAgcm93O1xcclxcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XFxyXFxuICAgICAgICAgICAgd2lkdGg6IDYwMHB4O1xcclxcbiAgICAgICAgICAgIGhlaWdodDogNTAwcHg7XFxyXFxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBzdGFydDtcXHJcXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XFxyXFxuICAgICAgICAgICAgcmlnaHQ6IDYwcHg7XFxyXFxuICAgICAgICAgICAgLy8gYm9yZGVyOiAycHggc29saWQgYmx1ZTtcXHJcXG5cXHJcXG4gICAgICAgICAgICAuc2tpbGxUaXRsZXtcXHJcXG4gICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcclxcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA3MHB4O1xcclxcbiAgICAgICAgICAgICAgICB3aWR0aDogOTAlO1xcclxcbiAgICAgICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAtNzBweDtcXHJcXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMzBweDtcXHJcXG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDcwcHg7XFxyXFxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogNTBweDtcXHJcXG4gICAgICAgICAgICAgICAgLy9iYWNrZ3JvdW5kLWNvbG9yOiAkT0RldGFpbHMtY29sb3I7XFxyXFxuICAgICAgICAgICAgfVxcclxcbiAgICBcXHJcXG4gICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAucHJvZy1sYW5nLC5hZG9iZXtcXHJcXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICAgICAgZmxleC1mbG93OiBjb2x1bW4gbm93cmFwO1xcclxcbiAgICAgICAgICAgICAgICB3aWR0aDogNTAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIFxcclxcbiAgICAgICAgICAgICAgICAuYmFye1xcclxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAycHggMCAycHggMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogODAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgLmZpbGxlcntcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IDAuNXMgZWFzZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBib3JkZXItcmFkaXVzOiAxMnB4IDAgMCAxMnB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgIC5za2lsbC10aXRsZXtcXHJcXG4gICAgICAgICAgICAgICAgbWFyZ2luOiA3JSAwIDglIDA7XFxyXFxuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMzVweDtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgLnByb2ctbGFuZyAuYmFye1xcclxcbiAgICAgICAgICAgICAgICBib3JkZXI6IDJweCAkdGV4dC1jb2xvciBzb2xpZDtcXHJcXG4gICAgICAgICAgICAgICAgLmZpbGxlcntcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRzdWJ0aXRsZS1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAuYWRvYmUgLmJhcntcXHJcXG4gICAgICAgICAgICAgICAgYm9yZGVyOiAycHggJHRleHQtY29sb3Igc29saWQ7XFxyXFxuICAgICAgICAgICAgICAgIC5maWxsZXJ7XFxyXFxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkc2VwYXJ0b3ItY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICB9XFxyXFxuXFxyXFxuICAgIH1cXHJcXG59XFxyXFxuXFxyXFxuI2Jpb2dyYXBoeXtcXHJcXG4gICAgLmJpb2dyYXBoeXtcXHJcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxyXFxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG5cXHQgICAgbWF4LXdpZHRoOiAxMjAwcHg7XFxyXFxuXFx0ICAgIG1hcmdpbjogMCBhdXRvO1xcclxcbiAgICAgICAgcGFkZGluZzogMTAwcHggMDtcXHJcXG4gICAgICAgIFxcclxcbiAgICAgICAgI1RpdHVsb19CaW97XFxyXFxuICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICBmb250LXNpemU6IDcwcHg7XFxyXFxuICAgICAgICAgICAgYm90dG9tOiA0MDBweDtcXHJcXG4gICAgICAgICAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgICAgICAgICAgdG9wOiA5NXB4O1xcclxcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcclxcbiAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgLkV4cF9FZHVje1xcclxcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcXHJcXG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XFxyXFxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgbGVmdDogMDtcXHJcXG4gICAgICAgICAgICB0b3A6IDM1cHg7XFxyXFxuICAgICAgICAgICAgaGVpZ2h0OiA3NTBweDtcXHJcXG4gICAgICAgICAgICB3aWR0aDogNDV2dztcXHJcXG5cXHJcXG4gICAgICAgICAgICAjT3JnYW5pemFkb3JSb21ib3N7XFxyXFxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XFxyXFxuICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcXHJcXG4gICAgICAgICAgICAgICAgZmxleC13cmFwOiB3cmFwO1xcclxcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgICAgIHRvcDogMTcwcHg7XFxyXFxuICAgICAgICAgICAgICAgIHdpZHRoOiA2MDBweDtcXHJcXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA2MDBweDtcXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgLlJvbWJvU29saXRhcmlve1xcclxcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDIwMHB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgdG9wOiAyMDBweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcclxcbiAgICAgICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxyXFxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTY1cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDE2NXB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpIHNjYWxlKDAsMCk7XFxyXFxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkT0RldGFpbHMtY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjVzIGVhc2U7XFxyXFxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVye1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogMTkwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogOHB4IDhweCA1cHggMHB4ICR0aXRsZS1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBcXHJcXG4gICAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgICAgIC5UZXh0b0luZm97XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgLlJvbWJvSW5mb3tcXHJcXG4gICAgICAgICAgICAgICAgICAgIGxlZnQ6IDIwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0b3A6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcclxcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjogNTBweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxNjVweDtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTY1cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZykgc2NhbGUoMCwwKTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRPRGV0YWlscy1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IDAuNXMgZWFzZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICAmOmhvdmVye1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogLTEwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgYm94LXNoYWRvdzogOHB4IDhweCA1cHggMHB4ICR0aXRsZS1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgICAgIC5UZXh0b0luZm97XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDE4cHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAgICAgI1RpdHVsb19FeHB7XFxyXFxuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICAgICAgY29sb3I6ICR0ZXh0LWNvbG9yO1xcclxcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDcwcHg7XFxyXFxuICAgICAgICAgICAgICAgIGJvdHRvbTogNDAwcHg7XFxyXFxuICAgICAgICAgICAgICAgIHdpZHRoOiAxMDB2dztcXHJcXG4gICAgICAgICAgICAgICAgdG9wOiA2MHB4O1xcclxcbiAgICAgICAgICAgICAgICBsZWZ0OiA0MHB4O1xcclxcbiAgICAgICAgICAgICAgICBmb250LXdlaWdodDogdW5zZXQ7XFxyXFxuICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgICNUaXR1bG9fRWR1e1xcclxcbiAgICAgICAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XFxyXFxuICAgICAgICAgICAgICAgIGNvbG9yOiAkdGV4dC1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiA3MHB4O1xcclxcbiAgICAgICAgICAgICAgICBib3R0b206IDQwMHB4O1xcclxcbiAgICAgICAgICAgICAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgICAgICAgICAgICAgIHRvcDogNjBweDtcXHJcXG4gICAgICAgICAgICAgICAgbGVmdDogMTEwcHg7XFxyXFxuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiB1bnNldDtcXHJcXG4gICAgICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICB9XFxyXFxuXFxyXFxuICAgIH1cXHJcXG59XFxyXFxuXFxyXFxuI3Byb2plY3Rze1xcclxcbiAgICAucHJvamVjdHN7XFxyXFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcclxcblxcdCAgICBtYXgtd2lkdGg6IDEyMDBweDtcXHJcXG5cXHQgICAgbWFyZ2luOiAwIGF1dG87XFxyXFxuICAgICAgICBwYWRkaW5nOiAxMDBweCAwO1xcclxcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcclxcbiAgICAgICAgLnByb2plY3RzLWhlYWRlcntcXHJcXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgaDF7XFxyXFxuICAgICAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XFxyXFxuICAgICAgICAgICAgICAgIHNwYW57XFxyXFxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogJHNlcGFydG9yLWNvbG9yO1xcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcbiAgICAgICAgfVxcclxcbiAgICAgICAgLnByb2plY3RzLXRpdGxlIHtcXHJcXG4gICAgICAgICAgICBmb250LXNpemU6IDcwcHg7XFxyXFxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XFxyXFxuICAgICAgICAgICAgY29sb3I6ICR0ZXh0LWNvbG9yO1xcclxcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XFxyXFxuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcXHJcXG4gICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogLjJyZW07XFxyXFxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgLmFsbC1wcm9qZWN0c3tcXHJcXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXHJcXG4gICAgICAgICAgICAucHJvamVjdC1pdGVte1xcclxcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcclxcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcXHJcXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiA0MDBweDtcXHJcXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgIG1hcmdpbjogMDtcXHJcXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcXHJcXG4gICAgICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcXHJcXG4gICAgICAgICAgICAgICAgJjpudGgtY2hpbGQoZXZlbil7XFxyXFxuICAgICAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93LXJldmVyc2U7XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgLnByb2plY3QtaW5mb3tcXHJcXG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDMwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICBmbGV4LWJhc2lzOiA1MCU7XFxyXFxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XFxyXFxuICAgICAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XFxyXFxuICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNjBkZWcsICRzdWJ0aXRsZS1jb2xvciAwJSwgJHRpdGxlLWNvbG9yIDEwMCUpO1xcclxcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICRiZy1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIC5wdGl0dWxve1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDRyZW07XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I6IHRyYW5zcGFyZW50O1xcclxcblxcdCAgICAgICAgICAgICAgICAgICAgLyphbmltYXRpb246IHRleHRfcmV2ZWFsIC41cyBlYXNlIGZvcndhcmRzOyovXFxyXFxuXFx0ICAgICAgICAgICAgICAgICAgICBhbmltYXRpb24tZGVsYXk6IDFzO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIC5hbmltYXRpb25QIHtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0b3A6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRiZy1jb2xvcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLyphbmltYXRpb246IHRleHRfcmV2ZWFsX2JveCAxcyBlYXNlOyovXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFuaW1hdGlvbi1kZWxheTogLjVzO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgICAgIC5wc3VidGl0dWxve1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBmaXQtY29udGVudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDEuOHJlbTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAvKmFuaW1hdGlvbjogdGV4dF9yZXZlYWwgLjVzIGVhc2UgZm9yd2FyZHM7Ki9cXHJcXG5cXHQgICAgICAgICAgICAgICAgICAgIGFuaW1hdGlvbi1kZWxheTogMXM7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgLmFuaW1hdGlvblB7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmctY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qYW5pbWF0aW9uOiB0ZXh0X3JldmVhbF9ib3ggMXMgZWFzZTsqL1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmltYXRpb24tZGVsYXk6IC41cztcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICAucHRleHRve1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBmaXQtY29udGVudDsgIFxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiAkYmctY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAxLjRyZW07XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAyLjVyZW07XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogLjA1cmVtO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG5cXHQgICAgICAgICAgICAgICAgICAgIGNvbG9yOiB0cmFuc3BhcmVudDtcXHJcXG5cXHQgICAgICAgICAgICAgICAgICAgIC8qYW5pbWF0aW9uOiB0ZXh0X3JldmVhbCAuNXMgZWFzZSBmb3J3YXJkczsqL1xcclxcblxcdCAgICAgICAgICAgICAgICAgICAgYW5pbWF0aW9uLWRlbGF5OiAxcztcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAuYW5pbWF0aW9uUCB7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmctY29sb3I7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qYW5pbWF0aW9uOiB0ZXh0X3JldmVhbF9ib3ggMXMgZWFzZTsqL1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBhbmltYXRpb24tZGVsYXk6IC41cztcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgLnByb2plY3QtaW1ne1xcclxcbiAgICAgICAgICAgICAgICAgICAgZmxleC1iYXNpczogNTAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICY6YWZ0ZXJ7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGVudDogJyc7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGxlZnQ6IDA7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAwO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMTAwJTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNjBkZWcsICM0NThmZTQgMCUsICM0ODU1NjMgMTAwJSk7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogLjQ7XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICBpbWd7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIG9iamVjdC1maXQ6IGNvdmVyO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IC4zcyBlYXNlIHRyYW5zZm9ybTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXJ7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgaW1ne1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMSk7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgICAgICAgICAgfVxcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcbiAgICAgICAgfVxcclxcbiAgICB9XFxyXFxufVxcclxcblxcclxcbiNwaG90b3Mge1xcclxcbiAgICBcXHJcXG4gICAgLnBob3Rvc3tcXHJcXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICRiZy1jb2xvcjtcXHJcXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcXHJcXG4gICAgICAgIG1heC13aWR0aDogMTIwMHB4O1xcclxcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XFxyXFxuICAgICAgICBwYWRkaW5nOiAxMDBweCAwO1xcclxcbiAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcclxcbiAgICAgICAgZmxleC1mbG93OiB3cmFwIHJvdztcXHJcXG4gICAgICAgXFxyXFxuICAgICAgICAjcGhvdG9UaXRsZXtcXHJcXG4gICAgICAgICAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgICAgICAgICAgZm9udC1zaXplOiA2MHB4O1xcclxcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgbWFyZ2luOiAxMHB4O1xcclxcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xcclxcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgXFxyXFxuICAgICAgICB9XFxyXFxuXFxyXFxuICAgICAgICAjcGhvdG9TdWJUaXRsZXtcXHJcXG4gICAgICAgICAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgICAgICAgICAgZm9udC1zaXplOiAzMHB4O1xcclxcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBzdGFydDtcXHJcXG4gICAgICAgICAgICBtYXJnaW46IDEwcHg7XFxyXFxuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDUwcHg7XFxyXFxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMzBweDtcXHJcXG4gICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgIC5kaXNwbGF5e1xcclxcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhO1xcclxcbiAgICAgICAgICAgIC8vIHRvcDogNDBweDtcXHJcXG4gICAgICAgICAgICB0b3A6MHB4O1xcclxcbiAgICAgICAgICAgIGxlZnQ6IDBweDtcXHJcXG4gICAgICAgICAgICBoZWlnaHQ6IDY1MHB4O1xcclxcbiAgICAgICAgICAgIHdpZHRoOiAxMDB2dztcXHJcXG4gICAgICAgICAgICBkaXNwbGF5OiBncmlkO1xcclxcbiAgICAgICAgICAgIGdyaWQtY29sdW1uOiA0O1xcclxcbiAgICAgICAgICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogMzUwcHggYXV0byAzNTBweDtcXHJcXG4gICAgICAgICAgICBncmlkLXRlbXBsYXRlLXJvd3M6IDMwMHB4IDMwMHB4O1xcclxcbiAgICAgICAgICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICBncmlkLXJvdy1nYXA6IDUwcHg7XFxyXFxuICAgICAgICAgICAgcGFkZGluZzogMTVweDtcXHJcXG4gICAgICAgICAgICBvdmVyZmxvdy15OiBoaWRkZW47XFxyXFxuICAgICAgICAgICAgb3BhY2l0eTogMDtcXHJcXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjhzIGVhc2U7XFxyXFxuICAgICAgICAgICAgXFxyXFxuXFxyXFxuICAgICAgICAgICAgLmRpc3BsYXlQaG90b0JpZ3tcXHJcXG4gICAgICAgICAgICAgICAgZ3JpZC1jb2x1bW4tc3RhcnQ6IDI7XFxyXFxuICAgICAgICAgICAgICAgIGdyaWQtY29sdW1uLWVuZDogMztcXHJcXG4gICAgICAgICAgICAgICAgZ3JpZC1yb3ctc3RhcnQ6IDE7XFxyXFxuICAgICAgICAgICAgICAgIGdyaWQtcm93LWVuZDogMztcXHJcXG4gICAgICAgICAgICAgICAgb3ZlcmZsb3cteTogaGlkZGVuO1xcclxcbiAgICAgICAgICAgICAgICBpbWd7XFxyXFxuICAgICAgICAgICAgICAgICAgICBtYXgtd2lkdGg6IDk2JTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDk4JTtcXHJcXG4gICAgICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgICAgICAuZGlzcGxheVBob3Rve1xcclxcbiAgICAgICAgICAgICAgICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlO1xcclxcbiAgICAgICAgICAgICAgICBvdmVyZmxvdy15OiBoaWRkZW47XFxyXFxuICAgICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcXHJcXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XFxyXFxuICAgICAgICAgICAgICAgIGltZ3tcXHJcXG4gICAgICAgICAgICAgICAgICAgIG1heC13aWR0aDogOTclO1xcclxcbiAgICAgICAgICAgICAgICAgICAgbWF4LWhlaWdodDogOTclO1xcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgICAgIGltZ3tcXHJcXG4gICAgICAgICAgICAgICAgbWF4LXdpZHRoOiA5NyU7XFxyXFxuICAgICAgICAgICAgICAgIG1heC1oZWlnaHQ6IDk3JTtcXHJcXG4gICAgICAgICAgICAgICAgYm94LXNoYWRvdzogMTBweCAxMHB4IDVweCAwcHggJHRpdGxlLWNvbG9yO1xcclxcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiAwLjVzIGVhc2U7XFxyXFxuICAgICAgICAgICAgICAgICY6aG92ZXJ7XFxyXFxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOTUsMC45NSk7XFxyXFxuICAgICAgICAgICAgICAgICAgICBib3gtc2hhZG93OiA0cHggNHB4IDVweCAwcHggJHRpdGxlLWNvbG9yO1xcclxcbiAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgfVxcclxcbiAgICAgICAgfVxcclxcbiAgICB9XFxyXFxufVxcclxcblxcclxcbiN2aWRlbyB7XFxyXFxuICAgIC8vIGJhY2tncm91bmQtY29sb3I6IGdyZWVuO1xcclxcbiAgICAudmlkZW8ge1xcclxcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogJGJnLWNvbG9yO1xcclxcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcXHJcXG4gICAgICAgIGZsZXgtZmxvdzogd3JhcCByb3c7XFxyXFxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XFxyXFxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcclxcbiAgICAgICAgbWF4LXdpZHRoOiAxMjAwcHg7XFxyXFxuICAgICAgICBtYXJnaW46IDAgYXV0bztcXHJcXG4gICAgICAgIHBhZGRpbmc6IDEwMHB4IDA7XFxyXFxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xcclxcblxcclxcbiAgICAgICAgI3ZpZGVvVGl0bGV7XFxyXFxuICAgICAgICAgICAgd2lkdGg6IDEwMHZ3O1xcclxcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogNjBweDtcXHJcXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xcclxcbiAgICAgICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XFxyXFxuICAgICAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcXHJcXG4gICAgICAgICAgICBmb250LXdlaWdodDogYm9sZDtcXHJcXG4gICAgICAgICAgIFxcclxcbiAgICAgICAgfVxcclxcblxcclxcbiAgICAgICAgI3ZpZGVvU3ViVGl0bGV7XFxyXFxuICAgICAgICAgICAgd2lkdGg6IDEwMHZ3O1xcclxcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMzBweDtcXHJcXG4gICAgICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XFxyXFxuICAgICAgICAgICAgdGV4dC1hbGlnbjoganVzdGlmeTtcXHJcXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XFxyXFxuICAgICAgICAgICAgcGFkZGluZy1yaWdodDogNTBweDtcXHJcXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDUwcHg7XFxyXFxuICAgICAgICAgICAgXFxyXFxuICAgICAgICB9XFxyXFxuXFxyXFxuXFxyXFxuICAgICAgICAucGxheWVye1xcclxcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcXHJcXG4gICAgICAgICAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgICAgICAgICAgaGVpZ2h0OiA2NTBweDtcXHJcXG4gICAgICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkYmctY29sb3I7XFxyXFxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xcclxcbiAgICAgICAgICAgIHRyYW5zaXRpb246IDAuMnMgZWFzZTtcXHJcXG4gICAgICAgICAgICBvcGFjaXR5OiAwO1xcclxcbiAgICAgICAgICAgIHRyYW5zaXRpb246IDFzIGVhc2U7XFxyXFxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xcclxcbiAgICAgICAgICAgIHZpZGVve1xcclxcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDU4MHB4O1xcclxcbiAgICAgICAgICAgIH1cXHJcXG4gICAgICAgIH1cXHJcXG4gICAgfVxcclxcbn1cXHJcXG5cXHJcXG4jZm9vdGVye1xcclxcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNjBkZWcsICRzZXBhcnRvci1jb2xvciAwJSwgJFlEZXRhaWxzLWNvbG9yIDEwMCUpO1xcclxcbiAgICAuZm9vdGVye1xcclxcbiAgICAgICAgbWluLWhlaWdodDogMjAwcHg7XFxyXFxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xcclxcbiAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7XFxyXFxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcXHJcXG4gICAgICAgIGgye1xcclxcbiAgICAgICAgICAgIGNvbG9yOiR0ZXh0LWNvbG9yO1xcclxcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XFxyXFxuICAgICAgICAgICAgZm9udC1zaXplOiAxLjhyZW07XFxyXFxuICAgICAgICAgICAgbGV0dGVyLXNwYWNpbmc6IC4xcmVtO1xcclxcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XFxyXFxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcXHJcXG4gICAgICAgIH1cXHJcXG5cXHJcXG4gICAgICAgIC5mb290ZXItdGl0bGUge1xcclxcbiAgICAgICAgICAgIGZvbnQtZmFtaWx5OiBcXFwiRWFzdG1hblxcXCI7XFxyXFxuICAgICAgICAgICAgZm9udC1zaXplOiAyLjhyZW07XFxyXFxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDMwMDtcXHJcXG4gICAgICAgICAgICBjb2xvcjogJHRleHQtY29sb3I7XFxyXFxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcXHJcXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xcclxcbiAgICAgICAgICAgIGxldHRlci1zcGFjaW5nOiAuMnJlbTtcXHJcXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XFxyXFxuICAgICAgICAgICAgXFxyXFxuICAgICAgICB9XFxyXFxuICAgICAgICBcXHJcXG5cXHJcXG4gICAgICAgIC5zb2NpYWwtaWNvbiB7XFxyXFxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcXHJcXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xcclxcbiAgICAgICAgICAgIC5zb2NpYWwtaXRlbSB7XFxyXFxuICAgICAgICAgICAgICAgIGhlaWdodDogNTBweDtcXHJcXG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XFxyXFxuICAgICAgICAgICAgICAgIG1hcmdpbjogMCA1cHg7XFxyXFxuICAgICAgICAgICAgICAgIGltZyB7XFxyXFxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcXHJcXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IC4zcyBlYXNlIGZpbHRlcjtcXHJcXG4gICAgICAgICAgICAgICAgICAgICY6aG92ZXJ7XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgaW1ne1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWx0ZXI6IGdyYXlzY2FsZSgwKTtcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgICAgICB9XFxyXFxuICAgICAgICAgICAgICAgIH1cXHJcXG4gICAgICAgICAgICB9XFxyXFxuICAgICAgICB9XFxyXFxuICAgICAgICBwe1xcclxcbiAgICAgICAgICAgIGNvbG9yOiR0ZXh0LWNvbG9yO1xcclxcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xcclxcbiAgICAgICAgfVxcclxcbiAgICB9XFxyXFxufVxcclxcblxcclxcblxcclxcbi8qa2V5ZnJhbWVzKi9cXHJcXG5Aa2V5ZnJhbWVzIHRleHRfcmV2ZWFsX2JveCB7XFxyXFxuXFx0NTAlIHtcXHJcXG5cXHRcXHR3aWR0aDogMTAwJTtcXHJcXG5cXHRcXHRsZWZ0OiAwO1xcclxcblxcdH1cXHJcXG5cXHQxMDAlIHtcXHJcXG5cXHRcXHR3aWR0aDogMDtcXHJcXG5cXHRcXHRsZWZ0OiAxMDAlO1xcclxcblxcdH1cXHJcXG59XFxyXFxuQGtleWZyYW1lcyB0ZXh0X3JldmVhbCB7XFxyXFxuXFx0MTAwJSB7XFxyXFxuXFx0XFx0Y29sb3I6ICRiZy1jb2xvcjtcXHJcXG5cXHR9XFxyXFxufVxcclxcbkBrZXlmcmFtZXMgdGV4dF9yZXZlYWxfbmFtZSB7XFxyXFxuXFx0MTAwJSB7XFxyXFxuXFx0XFx0Y29sb3I6IGNyaW1zb247XFxyXFxuXFx0XFx0Zm9udC13ZWlnaHQ6IDUwMDtcXHJcXG5cXHR9XFxyXFxufVxcclxcblwiXSxcInNvdXJjZVJvb3RcIjpcIlwifV0pO1xuLy8gRXhwb3J0c1xuZXhwb3J0IGRlZmF1bHQgX19fQ1NTX0xPQURFUl9FWFBPUlRfX187XG4iLCIvLyBJbXBvcnRzXG5pbXBvcnQgX19fQ1NTX0xPQURFUl9BUElfSU1QT1JUX19fIGZyb20gXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L3J1bnRpbWUvYXBpLmpzXCI7XG52YXIgX19fQ1NTX0xPQURFUl9FWFBPUlRfX18gPSBfX19DU1NfTE9BREVSX0FQSV9JTVBPUlRfX18odHJ1ZSk7XG4vLyBNb2R1bGVcbl9fX0NTU19MT0FERVJfRVhQT1JUX19fLnB1c2goW21vZHVsZS5pZCwgXCJAY2hhcnNldCBcXFwiVVRGLThcXFwiO1xcbioge1xcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcXG4gIHBhZGRpbmc6IDA7XFxuICBtYXJnaW46IDA7XFxufVxcblxcbi8qXFxuKkRlY2xhcmFjacOzbiBkZSBsb3MgY29sb3JlcyBkZWwgZG9jdW1lbnRvXFxuKi9cXG4jbG9hZGluZy1wYWdlIHtcXG4gIHotaW5kZXg6IDEwMDE7XFxuICBkaXNwbGF5OiBmbGV4O1xcbiAgd2lkdGg6IDEwMHZ3O1xcbiAgaGVpZ2h0OiAxMDB2aDtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XFxuICBwb3NpdGlvbjogZml4ZWQ7XFxuICBvdmVyZmxvdzogaGlkZGVuO1xcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XFxuICBhbGlnbi1pdGVtczogY2VudGVyO1xcbiAgZmxleC1mbG93OiBjb2x1bW4gbm93cmFwO1xcbiAgZm9udC1mYW1pbHk6IEhlbHZldGljYTtcXG59XFxuI2xvYWRpbmctcGFnZSAjd2VsY29tZSB7XFxuICBjb2xvcjogIzc5N2FhMztcXG4gIGZvbnQtc2l6ZTogMTAwcHg7XFxufVxcbiNsb2FkaW5nLXBhZ2UgI2NhcmdhbmRvIHtcXG4gIHdpZHRoOiAyMHZoO1xcbiAgaGVpZ2h0OiAyMHZoO1xcbiAgZGlzcGxheTogZmxleDtcXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXG4gIGZsZXgtZmxvdzogY29sdW1uIG5vd3JhcDtcXG4gIGNvbG9yOiAjNzk3YWEzO1xcbiAgZm9udC1zaXplOiAzMHB4O1xcbiAgY29sb3I6ICM3ZDlkYjY7XFxufVwiLCBcIlwiLHtcInZlcnNpb25cIjozLFwic291cmNlc1wiOltcIndlYnBhY2s6Ly9zcmMvYXBwL3BhZ2VzL0xvYWRpbmdQYWdlL0xvYWRpbmdQYWdlLnNjc3NcIl0sXCJuYW1lc1wiOltdLFwibWFwcGluZ3NcIjpcIkFBQUEsZ0JBQWdCO0FBQWhCO0VBQ0ksc0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtBQUVKOztBQUNBOztDQUFBO0FBV0E7RUFDSSxhQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0EseUJBYk87RUFjUCxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSxzQkFBQTtBQU5KO0FBT0k7RUFDSSxjQXBCSztFQXFCTCxnQkFBQTtBQUxSO0FBT0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUVBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSxjQS9CSztFQWdDTCxlQUFBO0VBQ0EsY0FoQ1E7QUEwQmhCXCIsXCJzb3VyY2VzQ29udGVudFwiOltcIioge1xcclxcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xcclxcbiAgICBwYWRkaW5nOiAwO1xcclxcbiAgICBtYXJnaW46IDA7XFxyXFxufVxcclxcblxcclxcbi8qXFxyXFxuKkRlY2xhcmFjacOzbiBkZSBsb3MgY29sb3JlcyBkZWwgZG9jdW1lbnRvXFxyXFxuKi9cXHJcXG4kYmctY29sb3I6ICNmZmZmZmY7XFxyXFxuJHRpdGxlLWNvbG9yOiM3OTdhYTM7XFxyXFxuJHN1YnRpdGxlLWNvbG9yOiM3ZDlkYjY7XFxyXFxuJHRleHQtY29sb3I6IzAwMDAwMDsgXFxyXFxuJHNlcGFydG9yLWNvbG9yOiNjYWU3Yjk7XFxyXFxuJE9EZXRhaWxzLWNvbG9yOiAjZWI5NDg2O1xcclxcbiRZRGV0YWlscy1jb2xvcjogI2YzZGU4YTtcXHJcXG5cXHJcXG4jbG9hZGluZy1wYWdle1xcclxcbiAgICB6LWluZGV4OiAxMDAxO1xcclxcbiAgICBkaXNwbGF5OiBmbGV4O1xcclxcbiAgICB3aWR0aDogMTAwdnc7XFxyXFxuICAgIGhlaWdodDogMTAwdmg7XFxyXFxuICAgIGJhY2tncm91bmQtY29sb3I6ICRiZy1jb2xvcjtcXHJcXG4gICAgcG9zaXRpb246IGZpeGVkO1xcclxcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xcclxcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcXHJcXG4gICAgZmxleC1mbG93OiBjb2x1bW4gbm93cmFwO1xcclxcbiAgICBmb250LWZhbWlseTogSGVsdmV0aWNhO1xcclxcbiAgICAjd2VsY29tZXtcXHJcXG4gICAgICAgIGNvbG9yOiAkdGl0bGUtY29sb3I7XFxyXFxuICAgICAgICBmb250LXNpemU6IDEwMHB4O1xcclxcbiAgICB9XFxyXFxuICAgICNjYXJnYW5kb3tcXHJcXG4gICAgICAgIHdpZHRoOiAyMHZoO1xcclxcbiAgICAgICAgaGVpZ2h0OiAyMHZoO1xcclxcbiAgICAgICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcXHJcXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XFxyXFxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcXHJcXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XFxyXFxuICAgICAgICBmbGV4LWZsb3c6IGNvbHVtbiBub3dyYXA7XFxyXFxuICAgICAgICBjb2xvcjogJHRpdGxlLWNvbG9yO1xcclxcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xcclxcbiAgICAgICAgY29sb3I6ICRzdWJ0aXRsZS1jb2xvcjtcXHJcXG4gICAgfVxcclxcbn1cIl0sXCJzb3VyY2VSb290XCI6XCJcIn1dKTtcbi8vIEV4cG9ydHNcbmV4cG9ydCBkZWZhdWx0IF9fX0NTU19MT0FERVJfRVhQT1JUX19fO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbi8qXG4gIE1JVCBMaWNlbnNlIGh0dHA6Ly93d3cub3BlbnNvdXJjZS5vcmcvbGljZW5zZXMvbWl0LWxpY2Vuc2UucGhwXG4gIEF1dGhvciBUb2JpYXMgS29wcGVycyBAc29rcmFcbiovXG4vLyBjc3MgYmFzZSBjb2RlLCBpbmplY3RlZCBieSB0aGUgY3NzLWxvYWRlclxuLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGZ1bmMtbmFtZXNcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHVzZVNvdXJjZU1hcCkge1xuICB2YXIgbGlzdCA9IFtdOyAvLyByZXR1cm4gdGhlIGxpc3Qgb2YgbW9kdWxlcyBhcyBjc3Mgc3RyaW5nXG5cbiAgbGlzdC50b1N0cmluZyA9IGZ1bmN0aW9uIHRvU3RyaW5nKCkge1xuICAgIHJldHVybiB0aGlzLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgdmFyIGNvbnRlbnQgPSBjc3NXaXRoTWFwcGluZ1RvU3RyaW5nKGl0ZW0sIHVzZVNvdXJjZU1hcCk7XG5cbiAgICAgIGlmIChpdGVtWzJdKSB7XG4gICAgICAgIHJldHVybiBcIkBtZWRpYSBcIi5jb25jYXQoaXRlbVsyXSwgXCIge1wiKS5jb25jYXQoY29udGVudCwgXCJ9XCIpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY29udGVudDtcbiAgICB9KS5qb2luKCcnKTtcbiAgfTsgLy8gaW1wb3J0IGEgbGlzdCBvZiBtb2R1bGVzIGludG8gdGhlIGxpc3RcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIGZ1bmMtbmFtZXNcblxuXG4gIGxpc3QuaSA9IGZ1bmN0aW9uIChtb2R1bGVzLCBtZWRpYVF1ZXJ5LCBkZWR1cGUpIHtcbiAgICBpZiAodHlwZW9mIG1vZHVsZXMgPT09ICdzdHJpbmcnKSB7XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgICAgIG1vZHVsZXMgPSBbW251bGwsIG1vZHVsZXMsICcnXV07XG4gICAgfVxuXG4gICAgdmFyIGFscmVhZHlJbXBvcnRlZE1vZHVsZXMgPSB7fTtcblxuICAgIGlmIChkZWR1cGUpIHtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcHJlZmVyLWRlc3RydWN0dXJpbmdcbiAgICAgICAgdmFyIGlkID0gdGhpc1tpXVswXTtcblxuICAgICAgICBpZiAoaWQgIT0gbnVsbCkge1xuICAgICAgICAgIGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaWRdID0gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBtb2R1bGVzLmxlbmd0aDsgX2krKykge1xuICAgICAgdmFyIGl0ZW0gPSBbXS5jb25jYXQobW9kdWxlc1tfaV0pO1xuXG4gICAgICBpZiAoZGVkdXBlICYmIGFscmVhZHlJbXBvcnRlZE1vZHVsZXNbaXRlbVswXV0pIHtcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLWNvbnRpbnVlXG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAobWVkaWFRdWVyeSkge1xuICAgICAgICBpZiAoIWl0ZW1bMl0pIHtcbiAgICAgICAgICBpdGVtWzJdID0gbWVkaWFRdWVyeTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpdGVtWzJdID0gXCJcIi5jb25jYXQobWVkaWFRdWVyeSwgXCIgYW5kIFwiKS5jb25jYXQoaXRlbVsyXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbGlzdC5wdXNoKGl0ZW0pO1xuICAgIH1cbiAgfTtcblxuICByZXR1cm4gbGlzdDtcbn07XG5cbmZ1bmN0aW9uIGNzc1dpdGhNYXBwaW5nVG9TdHJpbmcoaXRlbSwgdXNlU291cmNlTWFwKSB7XG4gIHZhciBjb250ZW50ID0gaXRlbVsxXSB8fCAnJzsgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHByZWZlci1kZXN0cnVjdHVyaW5nXG5cbiAgdmFyIGNzc01hcHBpbmcgPSBpdGVtWzNdO1xuXG4gIGlmICghY3NzTWFwcGluZykge1xuICAgIHJldHVybiBjb250ZW50O1xuICB9XG5cbiAgaWYgKHVzZVNvdXJjZU1hcCAmJiB0eXBlb2YgYnRvYSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIHZhciBzb3VyY2VNYXBwaW5nID0gdG9Db21tZW50KGNzc01hcHBpbmcpO1xuICAgIHZhciBzb3VyY2VVUkxzID0gY3NzTWFwcGluZy5zb3VyY2VzLm1hcChmdW5jdGlvbiAoc291cmNlKSB7XG4gICAgICByZXR1cm4gXCIvKiMgc291cmNlVVJMPVwiLmNvbmNhdChjc3NNYXBwaW5nLnNvdXJjZVJvb3QgfHwgJycpLmNvbmNhdChzb3VyY2UsIFwiICovXCIpO1xuICAgIH0pO1xuICAgIHJldHVybiBbY29udGVudF0uY29uY2F0KHNvdXJjZVVSTHMpLmNvbmNhdChbc291cmNlTWFwcGluZ10pLmpvaW4oJ1xcbicpO1xuICB9XG5cbiAgcmV0dXJuIFtjb250ZW50XS5qb2luKCdcXG4nKTtcbn0gLy8gQWRhcHRlZCBmcm9tIGNvbnZlcnQtc291cmNlLW1hcCAoTUlUKVxuXG5cbmZ1bmN0aW9uIHRvQ29tbWVudChzb3VyY2VNYXApIHtcbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVmXG4gIHZhciBiYXNlNjQgPSBidG9hKHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChKU09OLnN0cmluZ2lmeShzb3VyY2VNYXApKSkpO1xuICB2YXIgZGF0YSA9IFwic291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247Y2hhcnNldD11dGYtODtiYXNlNjQsXCIuY29uY2F0KGJhc2U2NCk7XG4gIHJldHVybiBcIi8qIyBcIi5jb25jYXQoZGF0YSwgXCIgKi9cIik7XG59IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHVybCwgb3B0aW9ucykge1xuICBpZiAoIW9wdGlvbnMpIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgICBvcHRpb25zID0ge307XG4gIH0gLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVyc2NvcmUtZGFuZ2xlLCBuby1wYXJhbS1yZWFzc2lnblxuXG5cbiAgdXJsID0gdXJsICYmIHVybC5fX2VzTW9kdWxlID8gdXJsLmRlZmF1bHQgOiB1cmw7XG5cbiAgaWYgKHR5cGVvZiB1cmwgIT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHVybDtcbiAgfSAvLyBJZiB1cmwgaXMgYWxyZWFkeSB3cmFwcGVkIGluIHF1b3RlcywgcmVtb3ZlIHRoZW1cblxuXG4gIGlmICgvXlsnXCJdLipbJ1wiXSQvLnRlc3QodXJsKSkge1xuICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICAgIHVybCA9IHVybC5zbGljZSgxLCAtMSk7XG4gIH1cblxuICBpZiAob3B0aW9ucy5oYXNoKSB7XG4gICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gICAgdXJsICs9IG9wdGlvbnMuaGFzaDtcbiAgfSAvLyBTaG91bGQgdXJsIGJlIHdyYXBwZWQ/XG4gIC8vIFNlZSBodHRwczovL2RyYWZ0cy5jc3N3Zy5vcmcvY3NzLXZhbHVlcy0zLyN1cmxzXG5cblxuICBpZiAoL1tcIicoKSBcXHRcXG5dLy50ZXN0KHVybCkgfHwgb3B0aW9ucy5uZWVkUXVvdGVzKSB7XG4gICAgcmV0dXJuIFwiXFxcIlwiLmNvbmNhdCh1cmwucmVwbGFjZSgvXCIvZywgJ1xcXFxcIicpLnJlcGxhY2UoL1xcbi9nLCAnXFxcXG4nKSwgXCJcXFwiXCIpO1xuICB9XG5cbiAgcmV0dXJuIHVybDtcbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKHVybCwgb3B0aW9ucykge1xuICBpZiAoIW9wdGlvbnMpIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgICBvcHRpb25zID0ge307XG4gIH0gLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVuZGVyc2NvcmUtZGFuZ2xlLCBuby1wYXJhbS1yZWFzc2lnblxuXG5cbiAgdXJsID0gdXJsICYmIHVybC5fX2VzTW9kdWxlID8gdXJsLmRlZmF1bHQgOiB1cmw7XG5cbiAgaWYgKHR5cGVvZiB1cmwgIT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIHVybDtcbiAgfVxuXG4gIGlmIChvcHRpb25zLmhhc2gpIHtcbiAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgICB1cmwgKz0gb3B0aW9ucy5oYXNoO1xuICB9XG5cbiAgaWYgKG9wdGlvbnMubWF5YmVOZWVkUXVvdGVzICYmIC9bXFx0XFxuXFxmXFxyIFwiJz08PmBdLy50ZXN0KHVybCkpIHtcbiAgICByZXR1cm4gXCJcXFwiXCIuY29uY2F0KHVybCwgXCJcXFwiXCIpO1xuICB9XG5cbiAgcmV0dXJuIHVybDtcbn07IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBpc09sZElFID0gZnVuY3Rpb24gaXNPbGRJRSgpIHtcbiAgdmFyIG1lbW87XG4gIHJldHVybiBmdW5jdGlvbiBtZW1vcml6ZSgpIHtcbiAgICBpZiAodHlwZW9mIG1lbW8gPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAvLyBUZXN0IGZvciBJRSA8PSA5IGFzIHByb3Bvc2VkIGJ5IEJyb3dzZXJoYWNrc1xuICAgICAgLy8gQHNlZSBodHRwOi8vYnJvd3NlcmhhY2tzLmNvbS8jaGFjay1lNzFkODY5MmY2NTMzNDE3M2ZlZTcxNWMyMjJjYjgwNVxuICAgICAgLy8gVGVzdHMgZm9yIGV4aXN0ZW5jZSBvZiBzdGFuZGFyZCBnbG9iYWxzIGlzIHRvIGFsbG93IHN0eWxlLWxvYWRlclxuICAgICAgLy8gdG8gb3BlcmF0ZSBjb3JyZWN0bHkgaW50byBub24tc3RhbmRhcmQgZW52aXJvbm1lbnRzXG4gICAgICAvLyBAc2VlIGh0dHBzOi8vZ2l0aHViLmNvbS93ZWJwYWNrLWNvbnRyaWIvc3R5bGUtbG9hZGVyL2lzc3Vlcy8xNzdcbiAgICAgIG1lbW8gPSBCb29sZWFuKHdpbmRvdyAmJiBkb2N1bWVudCAmJiBkb2N1bWVudC5hbGwgJiYgIXdpbmRvdy5hdG9iKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbWVtbztcbiAgfTtcbn0oKTtcblxudmFyIGdldFRhcmdldCA9IGZ1bmN0aW9uIGdldFRhcmdldCgpIHtcbiAgdmFyIG1lbW8gPSB7fTtcbiAgcmV0dXJuIGZ1bmN0aW9uIG1lbW9yaXplKHRhcmdldCkge1xuICAgIGlmICh0eXBlb2YgbWVtb1t0YXJnZXRdID09PSAndW5kZWZpbmVkJykge1xuICAgICAgdmFyIHN0eWxlVGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXQpOyAvLyBTcGVjaWFsIGNhc2UgdG8gcmV0dXJuIGhlYWQgb2YgaWZyYW1lIGluc3RlYWQgb2YgaWZyYW1lIGl0c2VsZlxuXG4gICAgICBpZiAod2luZG93LkhUTUxJRnJhbWVFbGVtZW50ICYmIHN0eWxlVGFyZ2V0IGluc3RhbmNlb2Ygd2luZG93LkhUTUxJRnJhbWVFbGVtZW50KSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgLy8gVGhpcyB3aWxsIHRocm93IGFuIGV4Y2VwdGlvbiBpZiBhY2Nlc3MgdG8gaWZyYW1lIGlzIGJsb2NrZWRcbiAgICAgICAgICAvLyBkdWUgdG8gY3Jvc3Mtb3JpZ2luIHJlc3RyaWN0aW9uc1xuICAgICAgICAgIHN0eWxlVGFyZ2V0ID0gc3R5bGVUYXJnZXQuY29udGVudERvY3VtZW50LmhlYWQ7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgICAvLyBpc3RhbmJ1bCBpZ25vcmUgbmV4dFxuICAgICAgICAgIHN0eWxlVGFyZ2V0ID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBtZW1vW3RhcmdldF0gPSBzdHlsZVRhcmdldDtcbiAgICB9XG5cbiAgICByZXR1cm4gbWVtb1t0YXJnZXRdO1xuICB9O1xufSgpO1xuXG52YXIgc3R5bGVzSW5Eb20gPSBbXTtcblxuZnVuY3Rpb24gZ2V0SW5kZXhCeUlkZW50aWZpZXIoaWRlbnRpZmllcikge1xuICB2YXIgcmVzdWx0ID0gLTE7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHlsZXNJbkRvbS5sZW5ndGg7IGkrKykge1xuICAgIGlmIChzdHlsZXNJbkRvbVtpXS5pZGVudGlmaWVyID09PSBpZGVudGlmaWVyKSB7XG4gICAgICByZXN1bHQgPSBpO1xuICAgICAgYnJlYWs7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxuZnVuY3Rpb24gbW9kdWxlc1RvRG9tKGxpc3QsIG9wdGlvbnMpIHtcbiAgdmFyIGlkQ291bnRNYXAgPSB7fTtcbiAgdmFyIGlkZW50aWZpZXJzID0gW107XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0Lmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGl0ZW0gPSBsaXN0W2ldO1xuICAgIHZhciBpZCA9IG9wdGlvbnMuYmFzZSA/IGl0ZW1bMF0gKyBvcHRpb25zLmJhc2UgOiBpdGVtWzBdO1xuICAgIHZhciBjb3VudCA9IGlkQ291bnRNYXBbaWRdIHx8IDA7XG4gICAgdmFyIGlkZW50aWZpZXIgPSBcIlwiLmNvbmNhdChpZCwgXCIgXCIpLmNvbmNhdChjb3VudCk7XG4gICAgaWRDb3VudE1hcFtpZF0gPSBjb3VudCArIDE7XG4gICAgdmFyIGluZGV4ID0gZ2V0SW5kZXhCeUlkZW50aWZpZXIoaWRlbnRpZmllcik7XG4gICAgdmFyIG9iaiA9IHtcbiAgICAgIGNzczogaXRlbVsxXSxcbiAgICAgIG1lZGlhOiBpdGVtWzJdLFxuICAgICAgc291cmNlTWFwOiBpdGVtWzNdXG4gICAgfTtcblxuICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgIHN0eWxlc0luRG9tW2luZGV4XS5yZWZlcmVuY2VzKys7XG4gICAgICBzdHlsZXNJbkRvbVtpbmRleF0udXBkYXRlcihvYmopO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZXNJbkRvbS5wdXNoKHtcbiAgICAgICAgaWRlbnRpZmllcjogaWRlbnRpZmllcixcbiAgICAgICAgdXBkYXRlcjogYWRkU3R5bGUob2JqLCBvcHRpb25zKSxcbiAgICAgICAgcmVmZXJlbmNlczogMVxuICAgICAgfSk7XG4gICAgfVxuXG4gICAgaWRlbnRpZmllcnMucHVzaChpZGVudGlmaWVyKTtcbiAgfVxuXG4gIHJldHVybiBpZGVudGlmaWVycztcbn1cblxuZnVuY3Rpb24gaW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMpIHtcbiAgdmFyIHN0eWxlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcbiAgdmFyIGF0dHJpYnV0ZXMgPSBvcHRpb25zLmF0dHJpYnV0ZXMgfHwge307XG5cbiAgaWYgKHR5cGVvZiBhdHRyaWJ1dGVzLm5vbmNlID09PSAndW5kZWZpbmVkJykge1xuICAgIHZhciBub25jZSA9IHR5cGVvZiBfX3dlYnBhY2tfbm9uY2VfXyAhPT0gJ3VuZGVmaW5lZCcgPyBfX3dlYnBhY2tfbm9uY2VfXyA6IG51bGw7XG5cbiAgICBpZiAobm9uY2UpIHtcbiAgICAgIGF0dHJpYnV0ZXMubm9uY2UgPSBub25jZTtcbiAgICB9XG4gIH1cblxuICBPYmplY3Qua2V5cyhhdHRyaWJ1dGVzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICBzdHlsZS5zZXRBdHRyaWJ1dGUoa2V5LCBhdHRyaWJ1dGVzW2tleV0pO1xuICB9KTtcblxuICBpZiAodHlwZW9mIG9wdGlvbnMuaW5zZXJ0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgb3B0aW9ucy5pbnNlcnQoc3R5bGUpO1xuICB9IGVsc2Uge1xuICAgIHZhciB0YXJnZXQgPSBnZXRUYXJnZXQob3B0aW9ucy5pbnNlcnQgfHwgJ2hlYWQnKTtcblxuICAgIGlmICghdGFyZ2V0KSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDb3VsZG4ndCBmaW5kIGEgc3R5bGUgdGFyZ2V0LiBUaGlzIHByb2JhYmx5IG1lYW5zIHRoYXQgdGhlIHZhbHVlIGZvciB0aGUgJ2luc2VydCcgcGFyYW1ldGVyIGlzIGludmFsaWQuXCIpO1xuICAgIH1cblxuICAgIHRhcmdldC5hcHBlbmRDaGlsZChzdHlsZSk7XG4gIH1cblxuICByZXR1cm4gc3R5bGU7XG59XG5cbmZ1bmN0aW9uIHJlbW92ZVN0eWxlRWxlbWVudChzdHlsZSkge1xuICAvLyBpc3RhbmJ1bCBpZ25vcmUgaWZcbiAgaWYgKHN0eWxlLnBhcmVudE5vZGUgPT09IG51bGwpIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBzdHlsZS5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHN0eWxlKTtcbn1cbi8qIGlzdGFuYnVsIGlnbm9yZSBuZXh0ICAqL1xuXG5cbnZhciByZXBsYWNlVGV4dCA9IGZ1bmN0aW9uIHJlcGxhY2VUZXh0KCkge1xuICB2YXIgdGV4dFN0b3JlID0gW107XG4gIHJldHVybiBmdW5jdGlvbiByZXBsYWNlKGluZGV4LCByZXBsYWNlbWVudCkge1xuICAgIHRleHRTdG9yZVtpbmRleF0gPSByZXBsYWNlbWVudDtcbiAgICByZXR1cm4gdGV4dFN0b3JlLmZpbHRlcihCb29sZWFuKS5qb2luKCdcXG4nKTtcbiAgfTtcbn0oKTtcblxuZnVuY3Rpb24gYXBwbHlUb1NpbmdsZXRvblRhZyhzdHlsZSwgaW5kZXgsIHJlbW92ZSwgb2JqKSB7XG4gIHZhciBjc3MgPSByZW1vdmUgPyAnJyA6IG9iai5tZWRpYSA/IFwiQG1lZGlhIFwiLmNvbmNhdChvYmoubWVkaWEsIFwiIHtcIikuY29uY2F0KG9iai5jc3MsIFwifVwiKSA6IG9iai5jc3M7IC8vIEZvciBvbGQgSUVcblxuICAvKiBpc3RhbmJ1bCBpZ25vcmUgaWYgICovXG5cbiAgaWYgKHN0eWxlLnN0eWxlU2hlZXQpIHtcbiAgICBzdHlsZS5zdHlsZVNoZWV0LmNzc1RleHQgPSByZXBsYWNlVGV4dChpbmRleCwgY3NzKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgY3NzTm9kZSA9IGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKGNzcyk7XG4gICAgdmFyIGNoaWxkTm9kZXMgPSBzdHlsZS5jaGlsZE5vZGVzO1xuXG4gICAgaWYgKGNoaWxkTm9kZXNbaW5kZXhdKSB7XG4gICAgICBzdHlsZS5yZW1vdmVDaGlsZChjaGlsZE5vZGVzW2luZGV4XSk7XG4gICAgfVxuXG4gICAgaWYgKGNoaWxkTm9kZXMubGVuZ3RoKSB7XG4gICAgICBzdHlsZS5pbnNlcnRCZWZvcmUoY3NzTm9kZSwgY2hpbGROb2Rlc1tpbmRleF0pO1xuICAgIH0gZWxzZSB7XG4gICAgICBzdHlsZS5hcHBlbmRDaGlsZChjc3NOb2RlKTtcbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gYXBwbHlUb1RhZyhzdHlsZSwgb3B0aW9ucywgb2JqKSB7XG4gIHZhciBjc3MgPSBvYmouY3NzO1xuICB2YXIgbWVkaWEgPSBvYmoubWVkaWE7XG4gIHZhciBzb3VyY2VNYXAgPSBvYmouc291cmNlTWFwO1xuXG4gIGlmIChtZWRpYSkge1xuICAgIHN0eWxlLnNldEF0dHJpYnV0ZSgnbWVkaWEnLCBtZWRpYSk7XG4gIH0gZWxzZSB7XG4gICAgc3R5bGUucmVtb3ZlQXR0cmlidXRlKCdtZWRpYScpO1xuICB9XG5cbiAgaWYgKHNvdXJjZU1hcCAmJiBidG9hKSB7XG4gICAgY3NzICs9IFwiXFxuLyojIHNvdXJjZU1hcHBpbmdVUkw9ZGF0YTphcHBsaWNhdGlvbi9qc29uO2Jhc2U2NCxcIi5jb25jYXQoYnRvYSh1bmVzY2FwZShlbmNvZGVVUklDb21wb25lbnQoSlNPTi5zdHJpbmdpZnkoc291cmNlTWFwKSkpKSwgXCIgKi9cIik7XG4gIH0gLy8gRm9yIG9sZCBJRVxuXG4gIC8qIGlzdGFuYnVsIGlnbm9yZSBpZiAgKi9cblxuXG4gIGlmIChzdHlsZS5zdHlsZVNoZWV0KSB7XG4gICAgc3R5bGUuc3R5bGVTaGVldC5jc3NUZXh0ID0gY3NzO1xuICB9IGVsc2Uge1xuICAgIHdoaWxlIChzdHlsZS5maXJzdENoaWxkKSB7XG4gICAgICBzdHlsZS5yZW1vdmVDaGlsZChzdHlsZS5maXJzdENoaWxkKTtcbiAgICB9XG5cbiAgICBzdHlsZS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjc3MpKTtcbiAgfVxufVxuXG52YXIgc2luZ2xldG9uID0gbnVsbDtcbnZhciBzaW5nbGV0b25Db3VudGVyID0gMDtcblxuZnVuY3Rpb24gYWRkU3R5bGUob2JqLCBvcHRpb25zKSB7XG4gIHZhciBzdHlsZTtcbiAgdmFyIHVwZGF0ZTtcbiAgdmFyIHJlbW92ZTtcblxuICBpZiAob3B0aW9ucy5zaW5nbGV0b24pIHtcbiAgICB2YXIgc3R5bGVJbmRleCA9IHNpbmdsZXRvbkNvdW50ZXIrKztcbiAgICBzdHlsZSA9IHNpbmdsZXRvbiB8fCAoc2luZ2xldG9uID0gaW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMpKTtcbiAgICB1cGRhdGUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGUsIHN0eWxlSW5kZXgsIGZhbHNlKTtcbiAgICByZW1vdmUgPSBhcHBseVRvU2luZ2xldG9uVGFnLmJpbmQobnVsbCwgc3R5bGUsIHN0eWxlSW5kZXgsIHRydWUpO1xuICB9IGVsc2Uge1xuICAgIHN0eWxlID0gaW5zZXJ0U3R5bGVFbGVtZW50KG9wdGlvbnMpO1xuICAgIHVwZGF0ZSA9IGFwcGx5VG9UYWcuYmluZChudWxsLCBzdHlsZSwgb3B0aW9ucyk7XG5cbiAgICByZW1vdmUgPSBmdW5jdGlvbiByZW1vdmUoKSB7XG4gICAgICByZW1vdmVTdHlsZUVsZW1lbnQoc3R5bGUpO1xuICAgIH07XG4gIH1cblxuICB1cGRhdGUob2JqKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIHVwZGF0ZVN0eWxlKG5ld09iaikge1xuICAgIGlmIChuZXdPYmopIHtcbiAgICAgIGlmIChuZXdPYmouY3NzID09PSBvYmouY3NzICYmIG5ld09iai5tZWRpYSA9PT0gb2JqLm1lZGlhICYmIG5ld09iai5zb3VyY2VNYXAgPT09IG9iai5zb3VyY2VNYXApIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB1cGRhdGUob2JqID0gbmV3T2JqKTtcbiAgICB9IGVsc2Uge1xuICAgICAgcmVtb3ZlKCk7XG4gICAgfVxuICB9O1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChsaXN0LCBvcHRpb25zKSB7XG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9OyAvLyBGb3JjZSBzaW5nbGUtdGFnIHNvbHV0aW9uIG9uIElFNi05LCB3aGljaCBoYXMgYSBoYXJkIGxpbWl0IG9uIHRoZSAjIG9mIDxzdHlsZT5cbiAgLy8gdGFncyBpdCB3aWxsIGFsbG93IG9uIGEgcGFnZVxuXG4gIGlmICghb3B0aW9ucy5zaW5nbGV0b24gJiYgdHlwZW9mIG9wdGlvbnMuc2luZ2xldG9uICE9PSAnYm9vbGVhbicpIHtcbiAgICBvcHRpb25zLnNpbmdsZXRvbiA9IGlzT2xkSUUoKTtcbiAgfVxuXG4gIGxpc3QgPSBsaXN0IHx8IFtdO1xuICB2YXIgbGFzdElkZW50aWZpZXJzID0gbW9kdWxlc1RvRG9tKGxpc3QsIG9wdGlvbnMpO1xuICByZXR1cm4gZnVuY3Rpb24gdXBkYXRlKG5ld0xpc3QpIHtcbiAgICBuZXdMaXN0ID0gbmV3TGlzdCB8fCBbXTtcblxuICAgIGlmIChPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobmV3TGlzdCkgIT09ICdbb2JqZWN0IEFycmF5XScpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxhc3RJZGVudGlmaWVycy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGlkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbaV07XG4gICAgICB2YXIgaW5kZXggPSBnZXRJbmRleEJ5SWRlbnRpZmllcihpZGVudGlmaWVyKTtcbiAgICAgIHN0eWxlc0luRG9tW2luZGV4XS5yZWZlcmVuY2VzLS07XG4gICAgfVxuXG4gICAgdmFyIG5ld0xhc3RJZGVudGlmaWVycyA9IG1vZHVsZXNUb0RvbShuZXdMaXN0LCBvcHRpb25zKTtcblxuICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBsYXN0SWRlbnRpZmllcnMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICB2YXIgX2lkZW50aWZpZXIgPSBsYXN0SWRlbnRpZmllcnNbX2ldO1xuXG4gICAgICB2YXIgX2luZGV4ID0gZ2V0SW5kZXhCeUlkZW50aWZpZXIoX2lkZW50aWZpZXIpO1xuXG4gICAgICBpZiAoc3R5bGVzSW5Eb21bX2luZGV4XS5yZWZlcmVuY2VzID09PSAwKSB7XG4gICAgICAgIHN0eWxlc0luRG9tW19pbmRleF0udXBkYXRlcigpO1xuXG4gICAgICAgIHN0eWxlc0luRG9tLnNwbGljZShfaW5kZXgsIDEpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGxhc3RJZGVudGlmaWVycyA9IG5ld0xhc3RJZGVudGlmaWVycztcbiAgfTtcbn07IiwiaW1wb3J0IHsgSG9tZVBhZ2VDb250cm9sbGVyIH0gZnJvbSAnLi9wYWdlcy9Ib21lUGFnZS9Ib21lUGFnZUNvbnRyb2xsZXInO1xyXG5pbXBvcnQgeyBMb2FkaW5nUGFnZUNvbnRyb2xsZXIgfSBmcm9tICcuL3BhZ2VzL0xvYWRpbmdQYWdlL0xvYWRpbmdQYWdlQ29udHJvbGxlcic7XHJcblxyXG5jbGFzcyBBcHAge1xyXG5cclxuICBtYWluUGFnZXMgPSBbXHJcbiAgICBMb2FkaW5nUGFnZUNvbnRyb2xsZXIsXHJcbiAgICBIb21lUGFnZUNvbnRyb2xsZXIsXHJcbiAgXTtcclxuXHJcbiAgbG9hZGVkOiBhbnkgPSBbXTtcclxuXHJcbiAgLyoqXHJcbiAgICogRnVuY2nDs24gcXVlIHJlbmRlcml6YSBsYXMgcMOhZ2luYXMgcXVlIGVzdMOhbiBlbiBlbCBhcnJheSBkZSBtYWluUGFnZXMuXHJcbiAgICogRXN0YSBmdW5jacOzbiBsbGFtYSBhIHZhcmlhcyBmdW5jaW9uZXMgZGVudHJvIGRlIGxhcyBww6FnaW5hcyBwYXJhLCBwb3IgZWplbXBsbywgb2J0ZW5lciBzdXMgZWxlbWVudG9zLlxyXG4gICAqL1xyXG4gIHJlbmRlcigpIHtcclxuICAgIGNvbnN0IGNvbXBvbmVudCA9IHRoaXMuY29tcG9uZW50KCk7XHJcbiAgICB0aGlzLm1haW5QYWdlcy5mb3JFYWNoKHBhZ2VDb250cm9sbGVyID0+IHtcclxuICAgICAgY29uc3QgY29udHJvbGxlciA9IG5ldyBwYWdlQ29udHJvbGxlcigpO1xyXG4gICAgICB0aGlzLmxvYWRlZC5wdXNoKGNvbnRyb2xsZXIpO1xyXG4gICAgICBjb25zdCBbZWxJZCwgZWxlbWVudF0gPSBuZXcgcGFnZUNvbnRyb2xsZXIoKS5nZXRWaWV3KCk7XHJcbiAgICAgIGNvbXBvbmVudC5hcHBlbmRDaGlsZCh0aGlzLmNyZWF0ZVBhZ2UoZWxJZCwgZWxlbWVudCkpO1xyXG4gICAgfSlcclxuICAgIFxyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmQoY29tcG9uZW50KTtcclxuICAgIGRvY3VtZW50LmJvZHkub25sb2FkID0gKCkgPT4ge1xyXG4gICAgICB0aGlzLmxvYWRlZC5mb3JFYWNoKChjb250cm9sbGVyOiBhbnkpID0+IHtcclxuICAgICAgICBpZiAoJ3N0YXJ0JyBpbiBjb250cm9sbGVyLmNvbXBvbmVudCkge1xyXG4gICAgICAgICAgY29udHJvbGxlci5jb21wb25lbnQuc3RhcnQoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBGdW5jacOzbiBxdWUgY3JlYSB1biBkaXYgZGVudHJvIGRlIGxhIHDDoWdpbmEuIEVzdGUgZGl2IHRlbmRyw6EgdG9kbyBlbCBjb250ZW5pZG8gZGUgbGEgcMOhZ2luYSBlbiBzdSBpbnRlcmlvci5cclxuICAgKiBcclxuICAgKiBAcGFyYW0gaWQgXHJcbiAgICogQHBhcmFtIGVsZW1lbnQgXHJcbiAgICogXHJcbiAgICogQHJldHVybiBIVE1MRGl2RWxlbWVudFxyXG4gICAqL1xyXG4gIGNyZWF0ZVBhZ2UoaWQ6IHN0cmluZywgZWxlbWVudDogRG9jdW1lbnRGcmFnbWVudCkge1xyXG4gICAgY29uc3QgcGFnZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgcGFnZS5pZCA9IGlkO1xyXG4gICAgcGFnZS5hcHBlbmRDaGlsZChlbGVtZW50KTtcclxuICAgIHJldHVybiBwYWdlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRnVuY2nDs24gcXVlIGNyZWEgbGEgZGl2ICNhcHAgZGVudHJvIGRlIGxhIHDDoWdpbmEuIEVzdGEgZXMgbGEgZGl2IHByaW5jaXBhbCBkZSB0b2RhIGxhIGFwbGljYWNpw7NuLCBkZW50cm8gZGUgZWxsYSBlc3RhcsOhbiBsYXMgcMOhZ2luYXMgY3JlYWRhcy5cclxuICAgKiBcclxuICAgKiBAcmV0dXJuIEhUTUxEaXZFbGVtZW50XHJcbiAgICovXHJcbiAgY29tcG9uZW50KCkge1xyXG4gICAgY29uc3QgZWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgZWxlbWVudC5pZCA9IFwiYXBwXCI7XHJcbiAgICByZXR1cm4gZWxlbWVudDtcclxuICB9XHJcblxyXG59XHJcblxyXG5jb25zdCBhcHAgPSBuZXcgQXBwKCk7XHJcbmFwcC5yZW5kZXIoKTtcclxuIiwidmFyIG1hcCA9IHtcblx0XCIuL0hvbWVQYWdlXCI6IFwiLi9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL0hvbWVQYWdlLmh0bWxcIixcblx0XCIuL0hvbWVQYWdlLmh0bWxcIjogXCIuL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2UvSG9tZVBhZ2UuaHRtbFwiLFxuXHRcIi4vSG9tZVBhZ2Uuc2Nzc1wiOiBcIi4vc3JjL2FwcC9wYWdlcy9Ib21lUGFnZS9Ib21lUGFnZS5zY3NzXCIsXG5cdFwiLi9Ib21lUGFnZUNvbnRyb2xsZXJcIjogXCIuL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2UvSG9tZVBhZ2VDb250cm9sbGVyLnRzXCIsXG5cdFwiLi9Ib21lUGFnZUNvbnRyb2xsZXIudHNcIjogXCIuL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2UvSG9tZVBhZ2VDb250cm9sbGVyLnRzXCIsXG5cdFwiLi9jb3VudGVyXCI6IFwiLi9zcmMvYXBwL3BhZ2VzL0hvbWVQYWdlL2NvdW50ZXIudHNcIixcblx0XCIuL2NvdW50ZXIudHNcIjogXCIuL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2UvY291bnRlci50c1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL3NyYy9hcHAvcGFnZXMvSG9tZVBhZ2Ugc3luYyByZWN1cnNpdmUgXlxcXFwuXFxcXC8uKiRcIjsiLCIvLyBJbXBvcnRzXG52YXIgX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaHRtbC1sb2FkZXIvZGlzdC9ydW50aW1lL2dldFVybC5qc1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfMF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvYXVkaW8vTXAzX3NvbmcubXAzXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xX19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9hdWRpby9PZ2dfc29uZy5vZ2dcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzJfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9jb21wZXRlbmNlMi5wbmdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzNfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9zdHVkeTIucG5nXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF80X19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvM2QtZGVzaW5nMi5wbmdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzVfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9jb21wZXRlbmNlLnBuZ1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfNl9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nL3N0dWR5LnBuZ1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfN19fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nLzNkLWRlc2lnbi5wbmdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzhfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9wcm9qZWN0czEuanBnXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF85X19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvcHJvamVjdDIuanBnXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xMF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nL3Byb2plY3QzLmpwZ1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfMTFfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9waG90bzEuanBnXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xMl9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nL3Bob3RvMi5qcGdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzEzX19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvcGhvdG8zLmpwZWdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzE0X19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvcGhvdG80LmpwZ1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfMTVfX18gPSByZXF1aXJlKFwiLi4vLi4vLi4vYXNzZXRzL2ltZy9waG90bzUuanBnXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xNl9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvdmlkZW8vdGVzdC5tcDRcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzE3X19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvZmFjZWJvb2suc3ZnXCIpO1xudmFyIF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xOF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nL2luc3RhZ3JhbS5zdmdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzE5X19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvdHdpdHRlci5zdmdcIik7XG52YXIgX19fSFRNTF9MT0FERVJfSU1QT1JUXzIwX19fID0gcmVxdWlyZShcIi4uLy4uLy4uL2Fzc2V0cy9pbWcvbGlua2VkaW4uc3ZnXCIpO1xuLy8gTW9kdWxlXG52YXIgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMF9fXyA9IF9fX0hUTUxfTE9BREVSX0dFVF9TT1VSQ0VfRlJPTV9JTVBPUlRfX18oX19fSFRNTF9MT0FERVJfSU1QT1JUXzBfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzFfX18gPSBfX19IVE1MX0xPQURFUl9HRVRfU09VUkNFX0ZST01fSU1QT1JUX19fKF9fX0hUTUxfTE9BREVSX0lNUE9SVF8xX19fKTtcbnZhciBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8yX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMl9fXyk7XG52YXIgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfM19fXyA9IF9fX0hUTUxfTE9BREVSX0dFVF9TT1VSQ0VfRlJPTV9JTVBPUlRfX18oX19fSFRNTF9MT0FERVJfSU1QT1JUXzNfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzRfX18gPSBfX19IVE1MX0xPQURFUl9HRVRfU09VUkNFX0ZST01fSU1QT1JUX19fKF9fX0hUTUxfTE9BREVSX0lNUE9SVF80X19fKTtcbnZhciBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF81X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfNV9fXyk7XG52YXIgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfNl9fXyA9IF9fX0hUTUxfTE9BREVSX0dFVF9TT1VSQ0VfRlJPTV9JTVBPUlRfX18oX19fSFRNTF9MT0FERVJfSU1QT1JUXzZfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzdfX18gPSBfX19IVE1MX0xPQURFUl9HRVRfU09VUkNFX0ZST01fSU1QT1JUX19fKF9fX0hUTUxfTE9BREVSX0lNUE9SVF83X19fKTtcbnZhciBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF84X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfOF9fXyk7XG52YXIgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfOV9fXyA9IF9fX0hUTUxfTE9BREVSX0dFVF9TT1VSQ0VfRlJPTV9JTVBPUlRfX18oX19fSFRNTF9MT0FERVJfSU1QT1JUXzlfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzEwX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTBfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzExX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTFfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzEyX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTJfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzEzX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTNfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE0X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTRfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE1X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTVfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE2X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTZfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE3X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTdfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE4X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMThfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE5X19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMTlfX18pO1xudmFyIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzIwX19fID0gX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyhfX19IVE1MX0xPQURFUl9JTVBPUlRfMjBfX18pO1xudmFyIGNvZGUgPSBcIjxib2R5PlxcclxcbiAgICA8IS0tIEF1ZGlvcyAtLT5cXHJcXG4gICAgPGF1ZGlvIGlkPVxcXCJNcDNcXFwiIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8wX19fICsgXCJcXFwiIGxvb3A+PC9hdWRpbz5cXHJcXG4gICAgPGF1ZGlvIGlkPVxcXCJPZ2dcXFwiIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8xX19fICsgXCJcXFwiIGxvb3A+PC9hdWRpbz5cXHJcXG5cXHJcXG4gICAgPCEtLSBIZWFkZXIgLS0+XFxyXFxuICAgIDxzZWN0aW9uIGlkPVxcXCJoZWFkZXJcXFwiPlxcclxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwiaGVhZGVyIGNvbnRhaW5lclxcXCI+XFxyXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwibmF2LWJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcIm5wYWdlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XFxcIiNwcmVzZW50YXRpb25cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMT5Qb3J0Zm9saW8gdGVtcGxhdGU8L2gxPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9hPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwibmF2LWxpc3RcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPHVsPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVxcXCIjcHJlc2VudGF0aW9uXFxcIiBkYXRhLWFmdGVyPVxcXCJQcmVzZW50YXRpb25cXFwiPlByZXNlbnRhdGlvbjwvYT48L2xpPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVxcXCIjYmlvZ3JhcGh5XFxcIiBkYXRhLWFmdGVyPVxcXCJCaW9ncmFwaHlcXFwiPkJpb2dyYXBoeTwvYT48L2xpPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVxcXCIjcHJvamVjdHNcXFwiIGRhdGEtYWZ0ZXI9XFxcIlByb2plY3RzXFxcIj5Qcm9qZWN0czwvYT48L2xpPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVxcXCIjcGhvdG9zXFxcIiBkYXRhLWFmdGVyPVxcXCJQaG90b3NcXFwiPlBob3RvczwvYT48L2xpPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPVxcXCIjdmlkZW9cXFwiIGRhdGEtYWZ0ZXI9XFxcIlZpZGVvXFxcIj5WaWRlbzwvYT48L2xpPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC91bD5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgIDwvc2VjdGlvbj5cXHJcXG4gICAgPCEtLSBQcmVzZW50YXRpb24gU2VjdGlvbiAgLS0+XFxyXFxuICAgIDxzZWN0aW9uIGlkPVxcXCJwcmVzZW50YXRpb25cXFwiPlxcclxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJlc2VudGF0aW9uIGNvbnRhaW5lclxcXCI+XFxyXFxuICAgICAgICAgICAgXFxyXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicGFycmFmb1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcInByZXNlbnRhdGlvblRpdGxlXFxcIj5IZWxsbyEgSSdtIFtZb3VyTmFtZV08L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJlc2VudGF0aW9uSWNvblxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8yX19fICsgXCJcXFwiIGFsdD1cXFwiXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzNfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJlc2VudGF0aW9uSWNvblxcXCIgc3R5bGU9XFxcInRvcDo0MDBweDtsZWZ0OjEyMHB4XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzRfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJlc2VudGF0aW9uSWNvbkNvbG9yXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfNV9fXyArIFwiXFxcIiBhbHQ9XFxcIlxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwcmVzZW50YXRpb25JY29uQ29sb3JcXFwiIHN0eWxlPVxcXCJsZWZ0OjE5NHB4XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzZfX18gKyBcIlxcXCIgIGFsdD1cXFwiXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInByZXNlbnRhdGlvbkljb25Db2xvclxcXCIgc3R5bGU9XFxcInRvcDo0MDBweDtsZWZ0OjEyMHB4XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzdfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJza2lsbHNcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJza2lsbFRpdGxlXFxcIj5TS0lMTFM8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJvZy1sYW5nXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNraWxsLXRpdGxlXFxcIj5Qcm9ncmFtbWluZzwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwianNcXFwiPkphdmFTY3JpcHQ8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcImpzLWJhclxcXCIgY2xhc3M9XFxcImJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZmlsbGVyXFxcIj48L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwicHlcXFwiPlB5dGhvbjwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwicHktYmFyXFxcIiBjbGFzcz1cXFwiYmFyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJmaWxsZXJcXFwiPjwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVxcXCJnb1xcXCI+R288L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcImdvLWJhclxcXCIgY2xhc3M9XFxcImJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZmlsbGVyXFxcIj48L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwiamF2YVxcXCI+SmF2YTwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwiamF2YS1iYXJcXFwiIGNsYXNzPVxcXCJiYXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImZpbGxlclxcXCI+PC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImFkb2JlXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNraWxsLXRpdGxlXFxcIj5BZG9iZTwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwicHNcXFwiPlBob3Rvc2hvcDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwicHMtYmFyXFxcIiBjbGFzcz1cXFwiYmFyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJmaWxsZXJcXFwiPjwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGlkPVxcXCJwclxcXCI+UHJlbWllcmU8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcInByLWJhclxcXCIgY2xhc3M9XFxcImJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZmlsbGVyXFxcIj48L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwiYW5cXFwiPkFuaW1hdGU8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcImFuLWJhclxcXCIgY2xhc3M9XFxcImJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZmlsbGVyXFxcIj48L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwiYWVcXFwiPkFmdGVyIEVmZmVjdHM8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgaWQ9XFxcImFlLWJhclxcXCIgY2xhc3M9XFxcImJhclxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZmlsbGVyXFxcIj48L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgIDwvZGl2PlxcclxcbiAgICA8L3NlY3Rpb24+XFxyXFxuICAgIDwhLS0gRW5kIFByZXNlbnRhdGlvbiBTZWN0aW9uICAtLT5cXHJcXG4gICAgPCEtLSBCaW9ncmFwaHkgU2VjdGlvbiAtLT5cXHJcXG4gICAgPHNlY3Rpb24gaWQ9XFxcImJpb2dyYXBoeVxcXCI+XFxyXFxuICAgICAgICA8ZGl2IGNsYXNzPVxcXCJiaW9ncmFwaHkgY29udGFpbmVyXFxcIj5cXHJcXG5cXHJcXG4gICAgICAgICAgICA8ZGl2IGlkPVxcXCJUaXR1bG9fQmlvXFxcIj5CSU9HUkFQSFk8L2Rpdj5cXHJcXG5cXHJcXG4gICAgICAgICAgICA8IS0tIEV4cGVyaWVuY2lhLCBlZHVjYWNpw7NuIC0tPlxcclxcblxcclxcbiAgICAgICAgICAgIDwhLS0gRXhwZXJpZW5jaWEgLS0+XFxyXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiRXhwX0VkdWNcXFwiPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICA8aDEgaWQ9XFxcIlRpdHVsb19FeHBcXFwiPkpvYiBFeHBlcmllbmNlPC9oMT5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBpZD1cXFwiT3JnYW5pemFkb3JSb21ib3NcXFwiPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiUm9tYm9Tb2xpdGFyaW9cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJUZXh0b0luZm9cXFwiPkV4cGVyaWVuY2UgaW5mbzwvc3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiUm9tYm9JbmZvXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiVGV4dG9JbmZvXFxcIj5FeHBlcmllbmNlIGluZm88L3NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcIlJvbWJvSW5mb1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcIlRleHRvSW5mb1xcXCI+RXhwZXJpZW5jZSBpbmZvPC9zcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJSb21ib0luZm9cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJUZXh0b0luZm9cXFwiPkV4cGVyaWVuY2UgaW5mbzwvc3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiUm9tYm9JbmZvXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiVGV4dG9JbmZvXFxcIj5FeHBlcmllbmNlIGluZm88L3NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICAgICAgPCEtLSBFZHVjYWNpw7NuIC0tPlxcclxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcIkV4cF9FZHVjXFxcIj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgPGgxIGlkPVxcXCJUaXR1bG9fRWR1XFxcIj5FZHVjYXRpb248L2gxPlxcclxcblxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGlkPVxcXCJPcmdhbml6YWRvclJvbWJvc1xcXCI+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJSb21ib1NvbGl0YXJpb1xcXCIgc3R5bGU9XFxcImJhY2tncm91bmQtY29sb3I6ICM2YmEwYzg7XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiVGV4dG9JbmZvXFxcIj5FZHVjYXRpb24gaW5mbzwvc3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiUm9tYm9JbmZvXFxcIiBzdHlsZT1cXFwiYmFja2dyb3VuZC1jb2xvcjogIzZiYTBjODtcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJUZXh0b0luZm9cXFwiPkVkdWNhdGlvbiBpbmZvPC9zcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJSb21ib0luZm9cXFwiIHN0eWxlPVxcXCJiYWNrZ3JvdW5kLWNvbG9yOiAjNmJhMGM4O1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XFxcIlRleHRvSW5mb1xcXCI+RWR1Y2F0aW9uIGluZm88L3NwYW4+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcIlJvbWJvSW5mb1xcXCIgc3R5bGU9XFxcImJhY2tncm91bmQtY29sb3I6ICM2YmEwYzg7XFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cXFwiVGV4dG9JbmZvXFxcIj5FZHVjYXRpb24gaW5mbzwvc3Bhbj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiUm9tYm9JbmZvXFxcIiBzdHlsZT1cXFwiYmFja2dyb3VuZC1jb2xvcjogIzZiYTBjODtcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVxcXCJUZXh0b0luZm9cXFwiPkVkdWNhdGlvbiBpbmZvPC9zcGFuPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgICAgIDwvZGl2PlxcclxcblxcclxcbiAgICAgICAgPC9kaXY+XFxyXFxuICAgIDwvc2VjdGlvbj5cXHJcXG4gICAgPCEtLSBFbmQgQmlvZ3JhcGh5IFNlY3Rpb24gLS0+XFxyXFxuICAgIDwhLS0gUHJvamVjdHMgU2VjdGlvbiAtLT5cXHJcXG4gICAgPHNlY3Rpb24gaWQ9XFxcInByb2plY3RzXFxcIj5cXHJcXG4gICAgICAgIDxkaXYgY2xhc3M9XFxcInByb2plY3RzIGNvbnRhaW5lclxcXCI+XFxyXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJvamVjdHMtaGVhZGVyXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPGgxIGNsYXNzPVxcXCJwcm9qZWN0cy10aXRsZVxcXCI+UmVjZW50IDxzcGFuPlByb2plY3RzPC9zcGFuPjwvaDE+XFxyXFxuICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiYWxsLXByb2plY3RzXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJvamVjdC1pdGVtXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInByb2plY3QtaW5mb1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGgxIGNsYXNzPVxcXCJwdGl0dWxvXFxcIj5Qcm9qZWN0IDEgPHNwYW4gY2xhc3M9XFxcImFuaW1hdGlvblBcXFwiPjwvc3Bhbj48L2gxPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMiBjbGFzcz1cXFwicHN1YnRpdHVsb1xcXCI+Q29kaW5nIGlzIExvdmUgPHNwYW4gY2xhc3M9XFxcImFuaW1hdGlvblBcXFwiPjwvc3Bhbj48L2gyPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxwIGNsYXNzPVxcXCJwdGV4dG9cXFwiPkxvcmVtLCBpcHN1bSBkb2xvciBzaXQgYW1ldCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBBZCwgaXVzdG8gY3VwaWRpdGF0ZVxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB2b2x1cHRhdHVtXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGltcGVkaXQgdW5kZSByZW0gaXBzYSBkaXN0aW5jdGlvIGlsbHVtIHF1YWUgbW9sbGl0aWEgdXQsIGFjY3VzYW50aXVtIGVpdXMgb2RpbyBkdWNpbXVzIGlsbG9cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmVxdWUgYXRxdWUgbGliZXJvIG5vbiBzdW50IGhhcnVtPyBJcHN1bSByZXBlbGxhdCBhbmltaSwgZnVnaXQgYXJjaGl0ZWN0byB2b2x1cHRhdHVtIG9kaXRcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXQhIDxzcGFuIGNsYXNzPVxcXCJhbmltYXRpb25QXFxcIj48L3NwYW4+PC9wPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwcm9qZWN0LWltZ1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfOF9fXyArIFwiXFxcIiBhbHQ9XFxcImltZ1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInByb2plY3QtaXRlbVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwcm9qZWN0LWluZm9cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxoMSBjbGFzcz1cXFwicHRpdHVsb1xcXCI+UHJvamVjdCAyIDxzcGFuIGNsYXNzPVxcXCJhbmltYXRpb25QXFxcIj48L3NwYW4+PC9oMT5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDIgY2xhc3M9XFxcInBzdWJ0aXR1bG9cXFwiPkNvZGluZyBpcyBMb3ZlIDxzcGFuIGNsYXNzPVxcXCJhbmltYXRpb25QXFxcIj48L3NwYW4+PC9oMj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8cCBjbGFzcz1cXFwicHRleHRvXFxcIj5Mb3JlbSwgaXBzdW0gZG9sb3Igc2l0IGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gQWQsIGl1c3RvIGN1cGlkaXRhdGVcXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdm9sdXB0YXR1bVxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpbXBlZGl0IHVuZGUgcmVtIGlwc2EgZGlzdGluY3RpbyBpbGx1bSBxdWFlIG1vbGxpdGlhIHV0LCBhY2N1c2FudGl1bSBlaXVzIG9kaW8gZHVjaW11cyBpbGxvXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5lcXVlIGF0cXVlIGxpYmVybyBub24gc3VudCBoYXJ1bT8gSXBzdW0gcmVwZWxsYXQgYW5pbWksIGZ1Z2l0IGFyY2hpdGVjdG8gdm9sdXB0YXR1bSBvZGl0XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV0ISA8c3BhbiBjbGFzcz1cXFwiYW5pbWF0aW9uUFxcXCI+PC9zcGFuPjwvcD5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJvamVjdC1pbWdcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzlfX18gKyBcIlxcXCIgYWx0PVxcXCJpbWdcXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwcm9qZWN0LWl0ZW1cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwicHJvamVjdC1pbmZvXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8aDEgY2xhc3M9XFxcInB0aXR1bG9cXFwiPlByb2plY3QgMyA8c3BhbiBjbGFzcz1cXFwiYW5pbWF0aW9uUFxcXCI+PC9zcGFuPjwvaDE+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPGgyIGNsYXNzPVxcXCJwc3VidGl0dWxvXFxcIj5Db2RpbmcgaXMgTG92ZSA8c3BhbiBjbGFzcz1cXFwiYW5pbWF0aW9uUFxcXCI+PC9zcGFuPjwvaDI+XFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgPHAgY2xhc3M9XFxcInB0ZXh0b1xcXCI+TG9yZW0sIGlwc3VtIGRvbG9yIHNpdCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIEFkLCBpdXN0byBjdXBpZGl0YXRlXFxyXFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZvbHVwdGF0dW1cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaW1wZWRpdCB1bmRlIHJlbSBpcHNhIGRpc3RpbmN0aW8gaWxsdW0gcXVhZSBtb2xsaXRpYSB1dCwgYWNjdXNhbnRpdW0gZWl1cyBvZGlvIGR1Y2ltdXMgaWxsb1xcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXF1ZSBhdHF1ZSBsaWJlcm8gbm9uIHN1bnQgaGFydW0/IElwc3VtIHJlcGVsbGF0IGFuaW1pLCBmdWdpdCBhcmNoaXRlY3RvIHZvbHVwdGF0dW0gb2RpdFxcclxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBldCEgPHNwYW4gY2xhc3M9XFxcImFuaW1hdGlvblBcXFwiPjwvc3Bhbj48L3A+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInByb2plY3QtaW1nXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8xMF9fXyArIFwiXFxcIiBhbHQ9XFxcImltZ1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICA8L2Rpdj5cXHJcXG4gICAgPC9zZWN0aW9uPlxcclxcbiAgICA8IS0tIEVuZCBQcm9qZWN0cyBTZWN0aW9uIC0tPlxcclxcblxcclxcblxcclxcbiAgICA8IS0tIFBob3RvcyBTZWN0aW9uIC0tPlxcclxcbiAgICA8c2VjdGlvbiBpZD1cXFwicGhvdG9zXFxcIj5cXHJcXG4gICAgICAgIDxkaXYgY2xhc3M9XFxcInBob3RvcyBjb250YWluZXJcXFwiPlxcclxcbiAgICAgICAgICAgIDxkaXYgaWQ9XFxcInBob3RvVGl0bGVcXFwiPlBIT1RPR1JBUEhJRVM8L2Rpdj5cXHJcXG4gICAgICAgICAgICA8ZGl2IGlkPVxcXCJwaG90b1N1YlRpdGxlXFxcIj5Tb21lIG9mIG15IHBob3RvczwvZGl2PlxcclxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImRpc3BsYXlcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJkaXNwbGF5UGhvdG9cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMTFfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZGlzcGxheVBob3RvQmlnXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzEyX19fICsgXCJcXFwiIGFsdD1cXFwiXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcImRpc3BsYXlQaG90b1xcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8aW1nIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8xM19fXyArIFwiXFxcIiBhbHQ9XFxcIlxcXCI+XFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJkaXNwbGF5UGhvdG9cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMTRfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwiZGlzcGxheVBob3RvXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE1X19fICsgXCJcXFwiIGFsdD1cXFwiXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgPC9kaXY+XFxyXFxuXFxyXFxuICAgICAgICA8L2Rpdj5cXHJcXG4gICAgPC9zZWN0aW9uPlxcclxcbiAgICA8IS0tIEVuZCBQaG90b3MgU2VjdGlvbiAtLT5cXHJcXG5cXHJcXG5cXHJcXG4gICAgPCEtLSBWaWRlbyBTZWN0aW9uIC0tPlxcclxcbiAgICA8c2VjdGlvbiBpZD1cXFwidmlkZW9cXFwiPlxcclxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwidmlkZW8gY29udGFpbmVyXFxcIj5cXHJcXG4gICAgICAgICAgICA8ZGl2IGlkPVxcXCJ2aWRlb1RpdGxlXFxcIj5WSURFTzwvZGl2PlxcclxcbiAgICAgICAgICAgIDxkaXYgaWQ9XFxcInZpZGVvU3ViVGl0bGVcXFwiPkxvcmVtIGlwc3VtIGRvbG9yIHNpdCBhbWV0LCBjb25zZWN0ZXR1ciBhZGlwaXNjaW5nIGVsaXQsIHNlZCBkbyBlaXVzbW9kIHRlbXBvclxcclxcbiAgICAgICAgICAgICAgICBpbmNpZGlkdW50IHV0IGxhYm9yZSBldCBkb2xvcmUgbWFnbmEgYWxpcXVhLiBVdCBlbmltIGFkIG1pbmltIHZlbmlhbSwgcXVpcyA8L2Rpdj5cXHJcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJwbGF5ZXJcXFwiPlxcclxcbiAgICAgICAgICAgICAgICA8IS0tIDxkaXYgaWQ9XFxcImJ0blZpZGVvUHJldlxcXCI+Jmx0PC9kaXY+IC0tPlxcclxcbiAgICAgICAgICAgICAgICA8dmlkZW8gaWQ9XFxcInZpZGVvUGxheWVyXFxcIiBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMTZfX18gKyBcIlxcXCIgY29udHJvbHM+PC92aWRlbz5cXHJcXG4gICAgICAgICAgICAgICAgPCEtLSA8ZGl2IGlkPVxcXCJidG5WaWRlb05leHRcXFwiPiZndDwvZGl2PiAtLT5cXHJcXG4gICAgICAgICAgICA8L2Rpdj5cXHJcXG5cXHJcXG4gICAgICAgIDwvZGl2PlxcclxcbiAgICA8L3NlY3Rpb24+XFxyXFxuICAgIDwhLS0gRW5kIFZpZGVvIFNlY3Rpb24gLS0+XFxyXFxuXFxyXFxuXFxyXFxuXFxyXFxuXFxyXFxuXFxyXFxuICAgIDwhLS0gRm9vdGVyIC0tPlxcclxcbiAgICA8c2VjdGlvbiBpZD1cXFwiZm9vdGVyXFxcIj5cXHJcXG4gICAgICAgIDxkaXYgY2xhc3M9XFxcImZvb3RlciBjb250YWluZXJcXFwiPlxcclxcbiAgICAgICAgICAgIDxoMSBjbGFzcz1cXFwiZm9vdGVyLXRpdGxlXFxcIj5Qb3J0Zm9saW8gPHNwYW4+dGVtcGxhdGU8L3NwYW4+PC9oMT5cXHJcXG4gICAgICAgICAgICA8aDI+WW91ciBDb21wbGV0ZSBXZWIgU29sdXRpb248L2gyPlxcclxcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNvY2lhbC1pY29uXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic29jaWFsLWl0ZW1cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cXFwiI1xcXCI+PGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMTdfX18gKyBcIlxcXCIgLz48L2E+XFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJzb2NpYWwtaXRlbVxcXCI+XFxyXFxuICAgICAgICAgICAgICAgICAgICA8YSBocmVmPVxcXCIjXFxcIj48aW1nIHNyYz1cXFwiXCIgKyBfX19IVE1MX0xPQURFUl9SRVBMQUNFTUVOVF8xOF9fXyArIFwiXFxcIiAvPjwvYT5cXHJcXG4gICAgICAgICAgICAgICAgPC9kaXY+XFxyXFxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XFxcInNvY2lhbC1pdGVtXFxcIj5cXHJcXG4gICAgICAgICAgICAgICAgICAgIDxhIGhyZWY9XFxcIiNcXFwiPjxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzE5X19fICsgXCJcXFwiIC8+PC9hPlxcclxcbiAgICAgICAgICAgICAgICA8L2Rpdj5cXHJcXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cXFwic29jaWFsLWl0ZW1cXFwiPlxcclxcbiAgICAgICAgICAgICAgICAgICAgPGEgaHJlZj1cXFwiI1xcXCI+PGltZyBzcmM9XFxcIlwiICsgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMjBfX18gKyBcIlxcXCIgLz48L2E+XFxyXFxuICAgICAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgIDwvZGl2PlxcclxcbiAgICAgICAgICAgIDxwPkNvcHlyaWdodCDCqSAyMDIwLiBBbGwgcmlnaHRzIHJlc2VydmVkPC9wPlxcclxcbiAgICAgICAgPC9kaXY+XFxyXFxuICAgIDwvc2VjdGlvbj5cXHJcXG4gICAgPCEtLSBFbmQgRm9vdGVyIC0tPlxcclxcbjwvYm9keT5cIjtcbi8vIEV4cG9ydHNcbm1vZHVsZS5leHBvcnRzID0gY29kZTsiLCJ2YXIgYXBpID0gcmVxdWlyZShcIiEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc3R5bGUtbG9hZGVyL2Rpc3QvcnVudGltZS9pbmplY3RTdHlsZXNJbnRvU3R5bGVUYWcuanNcIik7XG4gICAgICAgICAgICB2YXIgY29udGVudCA9IHJlcXVpcmUoXCIhIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9jc3MtbG9hZGVyL2Rpc3QvY2pzLmpzIS4uLy4uLy4uLy4uL25vZGVfbW9kdWxlcy9zYXNzLWxvYWRlci9kaXN0L2Nqcy5qcyEuL0hvbWVQYWdlLnNjc3NcIik7XG5cbiAgICAgICAgICAgIGNvbnRlbnQgPSBjb250ZW50Ll9fZXNNb2R1bGUgPyBjb250ZW50LmRlZmF1bHQgOiBjb250ZW50O1xuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICAgIGNvbnRlbnQgPSBbW21vZHVsZS5pZCwgY29udGVudCwgJyddXTtcbiAgICAgICAgICAgIH1cblxudmFyIG9wdGlvbnMgPSB7fTtcblxub3B0aW9ucy5pbnNlcnQgPSBcImhlYWRcIjtcbm9wdGlvbnMuc2luZ2xldG9uID0gZmFsc2U7XG5cbnZhciB1cGRhdGUgPSBhcGkoY29udGVudCwgb3B0aW9ucyk7XG5cblxuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbnRlbnQubG9jYWxzIHx8IHt9OyIsImV4cG9ydCBjbGFzcyBIb21lUGFnZUNvbnRyb2xsZXIge1xyXG5cclxuICAgIHZpZXc6IGFueTtcclxuICAgIGNvbXBvbmVudCA9IHtcclxuICAgICAgICBpZDogJ2hvbWUtcGFnZScsXHJcbiAgICAgICAgdmlldzogJ0hvbWVQYWdlLmh0bWwnLFxyXG4gICAgICAgIHN0eWxlOiAnSG9tZVBhZ2Uuc2NzcycsXHJcbiAgICAgICAgc3RhcnQ6ICgpID0+IHsgdGhpcy5zdGFydCgpIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLmxvYWRWaWV3KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgaW5pY2lhbGl6YSBsYXMgYW5pbWFjaW9uZXMgZGUgY2FyZ2EuIFNlIGxsYW1hIGRlc2RlIGVsIGFwcC50cyBjdWFuZG8gc2UgY2FyZ2EgZWwgYm9keS5cclxuICAgICAqL1xyXG4gICAgc3RhcnQoKSB7XHJcbiAgICAgICAgd2luZG93LnNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZpbGxTa2lsbEJhcnMoKTtcclxuICAgICAgICAgICAgdGhpcy5hbmltYXRlSWNvbnMoKTtcclxuICAgICAgICB9LDUwMDApXHJcbiAgICAgICAgdGhpcy5zY3JvbGxFdmVudHMoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmNpw7NuIHF1ZSBjYXJnYSBsYSB2aXN0YSAoZWwgaHRtbCkgZGUgbGEgcMOhZ2luYSBkZSBtYW5lcmEgZGluw6FtaWNhIHNlZ8O6biBzdSBhdHJpYnV0byBkZSBjb21wb25lbnQuIFxyXG4gICAgICovXHJcbiAgICBsb2FkVmlldygpIHtcclxuICAgICAgICByZXF1aXJlKGAuLyR7dGhpcy5jb21wb25lbnQuc3R5bGV9YCk7XHJcbiAgICAgICAgdGhpcy52aWV3ID0gcmVxdWlyZShgLi8ke3RoaXMuY29tcG9uZW50LnZpZXd9YCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgb2J0aWVuZSBlbCBpZCBkZWwgY29tcG9uZW50ZSB5IHN1IGZyYWdtZW50byBkZSBkb2N1bWVudG8gaHRtbCB5IGxvcyByZXRvcm5hIGFsIGFwcC50cyBwYXJhIHF1ZSBwdWVkYW4gc2VyIHJlbmRlcml6YWRvcy5cclxuICAgICAqIFxyXG4gICAgICogQHJldHVybnMgW3N0cmluZywgRG9jdW1lbnRGcmFnbWVudF1cclxuICAgICAqL1xyXG4gICAgZ2V0VmlldygpOiBbc3RyaW5nLCBEb2N1bWVudEZyYWdtZW50XSB7XHJcbiAgICAgICAgcmV0dXJuIFt0aGlzLmNvbXBvbmVudC5pZCwgZG9jdW1lbnQuY3JlYXRlUmFuZ2UoKS5jcmVhdGVDb250ZXh0dWFsRnJhZ21lbnQodGhpcy52aWV3KV07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgc2UgZW5jYXJnYSBkZSBsb3MgZXZlbnRvcyBkZSBzY3JvbGwsIGxsYW1hbmRvIGxhcyBmdW5jaW9uZXMgZXNwZWPDrWZpY2FzIHBhcmEgY2FkYSBldmVudG8gc2Vnw7puIGxhIHBvc2ljacOzbi5cclxuICAgICAqL1xyXG4gICAgc2Nyb2xsRXZlbnRzKCkge1xyXG4gICAgICAgIHdpbmRvdy5vbnNjcm9sbCA9IChfOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdmFyIHNjcm9sbF9wb3NpdGlvbiA9IHdpbmRvdy5zY3JvbGxZO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhzY3JvbGxfcG9zaXRpb24pO1xyXG5cclxuICAgICAgICAgICAgLy8gUGxheSB5IFBhdXNlIGF1ZGlvc1xyXG4gICAgICAgICAgICBpZihzY3JvbGxfcG9zaXRpb24gPCA3MCl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBhdXNlQXVkaW8oKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gcmVsbGVuYXIgeSB2YWNpYXIgYmFycmFzXHJcbiAgICAgICAgICAgIGlmKHNjcm9sbF9wb3NpdGlvbiA+PSAwICYmIHNjcm9sbF9wb3NpdGlvbiA8PSA0NTApe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maWxsU2tpbGxCYXJzKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVJY29ucygpO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1wdHlTa2lsbEJhcnMoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZUljb25zKHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChzY3JvbGxfcG9zaXRpb24gPj0gNzAgJiYgc2Nyb2xsX3Bvc2l0aW9uIDwgMTU2MCkgey8vQW5pbWFjaW9uZXMgZGVsIGhlYWRlclxyXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlSGVhZGVyKClcclxuICAgICAgICAgICAgICAgIGlmIChzY3JvbGxfcG9zaXRpb24gPiA0NTAgJiYgc2Nyb2xsX3Bvc2l0aW9uIDwgMTQ1MCkgey8vQW5pbWFjaW9uZXMgZGUgbGEgc2VjY2nDs24gZGUgYmlvZ3JhZsOtYVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGxheUF1ZGlvKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlQmlvZ3JhcGh5KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVCaW9ncmFwaHkodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVIZWFkZXIodHJ1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChzY3JvbGxfcG9zaXRpb24gPiAxNDUwICYmIHNjcm9sbF9wb3NpdGlvbiA8IDI5MzgpIHsvL0FuaW1hY2lvbmVzIGRlIGxhIHNlY2Npw7NuIFByb2plY3RzXHJcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVQcm9qZWN0U2VjdGlvbigpO1xyXG5cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZVByb2plY3RTZWN0aW9uKHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihzY3JvbGxfcG9zaXRpb24gPj0gMzAwMCAmJiBzY3JvbGxfcG9zaXRpb24gPDM5MDApe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlUGhvdG9zKCk7XHJcbiAgICAgICAgICAgIH1lbHNlIGlmKHNjcm9sbF9wb3NpdGlvbjwzMDAwKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZVBob3Rvcyh0cnVlKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVQaG90b3ModHJ1ZSx0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYoc2Nyb2xsX3Bvc2l0aW9uPj00MTAwKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuYW5pbWF0ZVBsYXllcigpXHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlUGxheWVyKHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZihzY3JvbGxfcG9zaXRpb24gPj0gMTU2MCAmJiBzY3JvbGxfcG9zaXRpb24gPDMxMTApe1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hbmltYXRlSGVhZGVyKClcclxuICAgICAgICAgICAgICAgIHRoaXMucGxheUF1ZGlvKGZhbHNlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHNjcm9sbF9wb3NpdGlvbiA+PSAzMTEwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFuaW1hdGVIZWFkZXIoKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5wYXVzZUF1ZGlvKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgYW5pbWEgZWwgaGVhZGVyIGRlIGxhIHDDoWdpbmEuIEVsIHBhcsOhbWV0cm8gZGUgZW50cmFkYSBkZWZpbmUgc2kgbGEgYW5pbWFjacOzbiBlcyBkZSBlbnRyYWRhIG8gZGUgc2FsaWRhLlxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gb3V0IFxyXG4gICAgICovXHJcbiAgICBhbmltYXRlSGVhZGVyKG91dCA9IGZhbHNlKXtcclxuICAgICAgICBjb25zdCBoZWFkZXI6IGFueSA9IHRoaXMuZmluZEluc2lkZU1lKFwiLmhlYWRlclwiKTtcclxuICAgICAgICBpZighb3V0KXsgICBcclxuICAgICAgICAgICAgaGVhZGVyLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICcjY2FlN2I5JztcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgaGVhZGVyLnN0eWxlLmJhY2tncm91bmRDb2xvciA9ICd0cmFuc3BhcmVudCc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIGFuaW1hIGxvcyDDrWNvbm9zIGRlIGxhIHNlY2Npw7NuIGRlIHByZXNlbnRhY2nDs24uIEVsIHBhcsOhbWV0cm8gZGUgZW50cmFkYSBkZWZpbmUgc2kgbGEgYW5pbWFjacOzbiBlcyBkZSBlbnRyYWRhIG8gZGUgc2FsaWRhLlxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gb3V0IFxyXG4gICAgICovXHJcbiAgICBhbmltYXRlSWNvbnMob3V0ID0gZmFsc2Upe1xyXG4gICAgICAgIGNvbnN0IGljb25PcGFjaXR5OiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZShcIi5wcmVzZW50YXRpb25JY29uQ29sb3JcIiwgdHJ1ZSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgaWYoIW91dCl7XHJcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGk8aWNvbk9wYWNpdHkubGVuZ3RoOyBpKyspe1xyXG4gICAgICAgICAgICAgICAgaWNvbk9wYWNpdHlbaV0uc3R5bGUub3BhY2l0eSA9IDE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaTxpY29uT3BhY2l0eS5sZW5ndGg7IGkrKyl7XHJcbiAgICAgICAgICAgICAgICBpY29uT3BhY2l0eVtpXS5zdHlsZS5vcGFjaXR5ID0gMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmNpw7NuIHF1ZSBhbmltYSBsYSBzZWNjacOzbiBkZSBwcm95ZWN0b3MuIEVsIHBhcsOhbWV0cm8gZGUgZW50cmFkYSBkZWZpbmUgc2kgbGEgYW5pbWFjacOzbiBlcyBkZSBlbnRyYWRhIG8gZGUgc2FsaWRhLlxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gb3V0IFxyXG4gICAgICovXHJcbiAgICBhbmltYXRlUHJvamVjdFNlY3Rpb24ob3V0ID0gZmFsc2Upe1xyXG4gICAgICAgIGNvbnN0IHRpdHVsb1Byb2plY3Q6IGFueSA9IHRoaXMuZmluZEluc2lkZU1lKCcucHRpdHVsbycsIHRydWUpO1xyXG4gICAgICAgIGNvbnN0IHN1YnRpdGxlUHJvamVjdDogYW55ID0gdGhpcy5maW5kSW5zaWRlTWUoJy5wc3VidGl0dWxvJywgdHJ1ZSk7XHJcbiAgICAgICAgY29uc3QgdGV4dFByb2plY3Q6IGFueSA9IHRoaXMuZmluZEluc2lkZU1lKCcucHRleHRvJywgdHJ1ZSk7XHJcbiAgICAgICAgY29uc3QgYW5pbWFQcm9qZWN0OiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnLmFuaW1hdGlvblAnLCB0cnVlKTtcclxuICAgICAgICBpZighb3V0KXtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aXR1bG9Qcm9qZWN0Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB0aXR1bG9Qcm9qZWN0W2ldLnN0eWxlLmFuaW1hdGlvbiA9ICd0ZXh0X3JldmVhbCAuNXMgZWFzZSBmb3J3YXJkcyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzdWJ0aXRsZVByb2plY3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHN1YnRpdGxlUHJvamVjdFtpXS5zdHlsZS5hbmltYXRpb24gPSAndGV4dF9yZXZlYWwgLjVzIGVhc2UgZm9yd2FyZHMnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGV4dFByb2plY3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHRleHRQcm9qZWN0W2ldLnN0eWxlLmFuaW1hdGlvbiA9ICd0ZXh0X3JldmVhbCAuNXMgZWFzZSBmb3J3YXJkcyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbmltYVByb2plY3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGFuaW1hUHJvamVjdFtpXS5zdHlsZS5hbmltYXRpb24gPSAndGV4dF9yZXZlYWxfYm94IDFzIGVhc2UnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGl0dWxvUHJvamVjdC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgdGl0dWxvUHJvamVjdFtpXS5zdHlsZS5hbmltYXRpb24gPSAnJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHN1YnRpdGxlUHJvamVjdC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgc3VidGl0bGVQcm9qZWN0W2ldLnN0eWxlLmFuaW1hdGlvbiA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGV4dFByb2plY3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHRleHRQcm9qZWN0W2ldLnN0eWxlLmFuaW1hdGlvbiA9ICcnO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYW5pbWFQcm9qZWN0Lmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBhbmltYVByb2plY3RbaV0uc3R5bGUuYW5pbWF0aW9uID0gJyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgYW5pbWEgbG9zIHJvbWJvcyBkZSBsYSBzZWNjacOzbiBkZSBCaW9ncmFmw61hLiBFbCBwYXLDoW1ldHJvIGRlIGVudHJhZGEgZGVmaW5lIHNpIGxhIGFuaW1hY2nDs24gZXMgZGUgZW50cmFkYSBvIHNhbGlkYS5cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIG91dCBcclxuICAgICAqL1xyXG4gICAgYW5pbWF0ZUJpb2dyYXBoeShvdXQgPSBmYWxzZSl7XHJcbiAgICAgICAgY29uc3Qgcm9tYm9JbmZvOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnLlJvbWJvSW5mbycsIHRydWUpO1xyXG4gICAgICAgIGNvbnN0IHJvbWJvU29saXRhcmlvOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnLlJvbWJvU29saXRhcmlvJywgdHJ1ZSk7XHJcbiAgICAgICAgaWYoIW91dCl7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcm9tYm9JbmZvLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICByb21ib0luZm9baV0uc3R5bGUub3BhY2l0eSA9IDE7XHJcbiAgICAgICAgICAgICAgICByb21ib0luZm9baV0uc3R5bGUudHJhbnNmb3JtID0gJ3JvdGF0ZSg0NWRlZykgc2NhbGUoMSwxKSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByb21ib1NvbGl0YXJpby5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgcm9tYm9Tb2xpdGFyaW9baV0uc3R5bGUub3BhY2l0eSA9IDE7XHJcbiAgICAgICAgICAgICAgICByb21ib1NvbGl0YXJpb1tpXS5zdHlsZS50cmFuc2Zvcm0gPSAncm90YXRlKDQ1ZGVnKSBzY2FsZSgxLDEpJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJvbWJvSW5mby5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgcm9tYm9JbmZvW2ldLnN0eWxlLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgICAgICAgICAgcm9tYm9JbmZvW2ldLnN0eWxlLnRyYW5zZm9ybSA9ICdyb3RhdGUoNDVkZWcpIHNjYWxlKDAsMCknO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcm9tYm9Tb2xpdGFyaW8ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHJvbWJvU29saXRhcmlvW2ldLnN0eWxlLm9wYWNpdHkgPSAwO1xyXG4gICAgICAgICAgICAgICAgcm9tYm9Tb2xpdGFyaW9baV0uc3R5bGUudHJhbnNmb3JtID0gJ3JvdGF0ZSg0NWRlZykgc2NhbGUoMCwwKSc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBhbmltYSBxdWUgZWwgZ3JpZCBxdWUgbXVlc3RyYSBsYXMgZm90b3MgZW4gbGEgc2VjY2nDs24gUGhvdG9zLiBcclxuICAgICAqIEVsIHByaW1lciBwYXLDoW1ldHJvIGRlIGVudHJhZGEgZGVmaW5lIHNpIGxhIGFuaW1hY2nDs24gZXMgZGUgZW50cmFkYSBvIHNhbGlkYS5cclxuICAgICAqIEVsIHNlZ3VuZG8gcGFyYW1ldHJvIGRlIGVudHJhZGEgZGVmaW5lIHNpIGVsIHNjcm9sbCBlc3TDoSBhcnJpYmEgbyBhYmFqbyBkZSBsYSBzZWNjacOzbiBwYXJhXHJcbiAgICAgKiBxdWUgZGVwZW5kaWVuZG8gZGUgZXN0YSBzZSB2YXlhIGEgbGEgZGVyZWNoYSBvIGEgbGEgaXpxdWllcmRhLlxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gb3V0XHJcbiAgICAgKiBAcGFyYW0gYWZlcnRcclxuICAgICAqL1xyXG4gICAgYW5pbWF0ZVBob3RvcyhvdXQgPSBmYWxzZSxhZnRlcj1mYWxzZSl7XHJcbiAgICAgICAgY29uc3QgZGlzcGxheTogYW55ID0gdGhpcy5maW5kSW5zaWRlTWUoJy5kaXNwbGF5JywgZmFsc2UpO1xyXG4gICAgICAgIGlmKCFvdXQpe1xyXG4gICAgICAgICAgICBkaXNwbGF5LnN0eWxlLm9wYWNpdHk9MTtcclxuICAgICAgICAgICAgZGlzcGxheS5zdHlsZS5sZWZ0PVwiMHB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgaWYgKCFhZnRlcil7XHJcbiAgICAgICAgICAgIGRpc3BsYXkuc3R5bGUub3BhY2l0eT0wO1xyXG4gICAgICAgICAgICBkaXNwbGF5LnN0eWxlLmxlZnQ9XCI5MDBweFwiO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICBkaXNwbGF5LnN0eWxlLm9wYWNpdHk9MDtcclxuICAgICAgICAgICAgZGlzcGxheS5zdHlsZS5sZWZ0PVwiLTUwMHB4XCI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmNpw7NuIHF1ZSBhbmltYSBsYSBvcGFjaWRhZCBkZWwgdsOtZGVvLiBFbCBwYXLDoW1ldHJvIGRlIGVudHJhZGEgZGVmaW5lIHNpIGxhIGFuaW1hY2nDs24gZXMgZGUgZW50cmFkYSBvIHNhbGlkYS5cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIG91dCBcclxuICAgICAqL1xyXG4gICAgYW5pbWF0ZVBsYXllcihvdXQgPSBmYWxzZSl7XHJcbiAgICAgICAgY29uc3QgcGxheWVyOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnLnBsYXllcicsZmFsc2UpO1xyXG4gICAgICAgIGlmKCFvdXQpe1xyXG4gICAgICAgICAgICBwbGF5ZXIuc3R5bGUub3BhY2l0eT0xO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBwbGF5ZXIuc3R5bGUub3BhY2l0eT0wO1xyXG4gICAgICAgICAgICAvLyByb21ib1NvbGl0YXJpb1tpXS5zdHlsZS50cmFuc2Zvcm0gPSAncm90YXRlKDQ1ZGVnKSBzY2FsZSgwLDApJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmNpw7NuIHF1ZSBjb250cm9sYSBsYSByZXByb2R1Y2Npw7NuIGRlIGxvcyBhdWRpb3MuIEVsIHBhcsOhbWV0cm8gZGUgZW50cmFkYSBkZWZpbmUgc2kgc2UgZGViZSByZXByb2R1Y2lyIGVsIG1wMyBvIGVsIG9nZy5cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIG1wMyBcclxuICAgICAqL1xyXG4gICAgcGxheUF1ZGlvKG1wMyA9IHRydWUpIHtcclxuICAgICAgICBjb25zdCBNcDM6IGFueSA9IHRoaXMuZmluZEluc2lkZU1lKCcjTXAzJyk7XHJcbiAgICAgICAgY29uc3QgT2dnOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnI09nZycpO1xyXG4gICAgICAgIGlmKG1wMyl7XHJcbiAgICAgICAgICAgIE1wMy5wbGF5KCkudGhlbigpLmNhdGNoKChlcnI6IGFueSkgPT4gY29uc29sZS5sb2coYEVycm9yIGFsIHJlcHJvZHVjaXIgZWwgYXJjaGl2bzogJHtlcnJ9YCkpO1xyXG4gICAgICAgICAgICBNcDMudm9sdW1lID0gMC4xNTtcclxuICAgICAgICAgICAgT2dnLnBhdXNlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIE1wMy5wYXVzZSgpO1xyXG4gICAgICAgICAgICBPZ2cucGxheSgpLnRoZW4oKS5jYXRjaCgoZXJyOiBhbnkpID0+IGNvbnNvbGUubG9nKGBFcnJvciBhbCByZXByb2R1Y2lyIGVsIGFyY2hpdm86ICR7ZXJyfWApKTtcclxuICAgICAgICAgICAgT2dnLnZvbHVtZSA9IDAuMTU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIHBhdXNhIHRvZG9zIGxvcyBhdWRpb3MuXHJcbiAgICAgKi9cclxuICAgIHBhdXNlQXVkaW8oKXtcclxuICAgICAgICBjb25zdCBNcDM6IGFueSA9IHRoaXMuZmluZEluc2lkZU1lKCcjTXAzJyk7XHJcbiAgICAgICAgY29uc3QgT2dnOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnI09nZycpO1xyXG4gICAgICAgIE9nZy5wYXVzZSgpO1xyXG4gICAgICAgIE1wMy5wYXVzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIHNlIGVuY2FyZ2EgZGUgbGxlbmFyIGxhcyBiYXJyYXMgZGUgaGFiaWxpZGFkZXMuXHJcbiAgICAgKi9cclxuICAgIGZpbGxTa2lsbEJhcnMoKSB7XHJcbiAgICAgICAgY29uc3QgYmFyczogYW55ID0gdGhpcy5maW5kSW5zaWRlTWUoJy5maWxsZXInLCB0cnVlKTtcclxuICAgICAgICBiYXJzWzBdLnN0eWxlLndpZHRoID0gJzc1JSc7XHJcbiAgICAgICAgYmFyc1sxXS5zdHlsZS53aWR0aCA9ICc0MCUnO1xyXG4gICAgICAgIGJhcnNbMl0uc3R5bGUud2lkdGggPSAnNjAlJztcclxuICAgICAgICBiYXJzWzNdLnN0eWxlLndpZHRoID0gJzgwJSc7XHJcbiAgICAgICAgYmFyc1s0XS5zdHlsZS53aWR0aCA9ICc0NSUnO1xyXG4gICAgICAgIGJhcnNbNV0uc3R5bGUud2lkdGggPSAnNzAlJztcclxuICAgICAgICBiYXJzWzZdLnN0eWxlLndpZHRoID0gJzQ1JSc7XHJcbiAgICAgICAgYmFyc1s3XS5zdHlsZS53aWR0aCA9ICc4NSUnO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmNpw7NuIHF1ZSBzZSBlbmNhcmdhIGRlIHZhY2lhciBsYXMgYmFycmFzIGRlIGhhYmlsaWRhZGVzLlxyXG4gICAgICovXHJcbiAgICBlbXB0eVNraWxsQmFycygpIHtcclxuICAgICAgICBjb25zdCBiYXJzOiBhbnkgPSB0aGlzLmZpbmRJbnNpZGVNZSgnLmZpbGxlcicsIHRydWUpO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgYmFycy5sZW5ndGg7IGkrKyliYXJzW2ldLnN0eWxlLndpZHRoID0gJzAlJztcclxuICAgIH1cclxuXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgZW5jdWVudHJhIHVuIGVsZW1lbnRvIEhUTUwgZGVudHJvIGRlbCBkb2N1bWVudG8gc2Vnw7puIGVsIHNlbGVjdG9yIHF1ZSBsZSBlbnRyYSBjb21vIHBhcsOhbWV0cm8uIEVsIHBhcsOhbWV0cm8gYWxsIGRlZmluZSBzaVxyXG4gICAgICogc2UgYnVzY2EgcG9yIGlkIG8gcG9yIGNsYXNlLlxyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gc2VsZWN0b3IgXHJcbiAgICAgKiBAcGFyYW0gYWxsIFxyXG4gICAgICovXHJcbiAgICBmaW5kSW5zaWRlTWUoc2VsZWN0b3I6IHN0cmluZywgYWxsID0gZmFsc2UpIHtcclxuICAgICAgICBjb25zdCBxdWVyeSA9IGAjYXBwICMke3RoaXMuY29tcG9uZW50LmlkfSAke3NlbGVjdG9yfWA7XHJcbiAgICAgICAgaWYgKCFhbGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IocXVlcnkpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKHF1ZXJ5KTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufSIsImV4cG9ydCBmdW5jdGlvbiBjb3VudChjb3VudGVyRWw6IEhUTUxFbGVtZW50KXtcclxuICAgIGNvdW50ZXJFbC5pbm5lckhUTUwgPSBgJHsoK2NvdW50ZXJFbC5pbm5lckhUTUwpKzF9YDtcclxufSIsInZhciBtYXAgPSB7XG5cdFwiLi9Mb2FkaW5nUGFnZVwiOiBcIi4vc3JjL2FwcC9wYWdlcy9Mb2FkaW5nUGFnZS9Mb2FkaW5nUGFnZS5odG1sXCIsXG5cdFwiLi9Mb2FkaW5nUGFnZS5odG1sXCI6IFwiLi9zcmMvYXBwL3BhZ2VzL0xvYWRpbmdQYWdlL0xvYWRpbmdQYWdlLmh0bWxcIixcblx0XCIuL0xvYWRpbmdQYWdlLnNjc3NcIjogXCIuL3NyYy9hcHAvcGFnZXMvTG9hZGluZ1BhZ2UvTG9hZGluZ1BhZ2Uuc2Nzc1wiLFxuXHRcIi4vTG9hZGluZ1BhZ2VDb250cm9sbGVyXCI6IFwiLi9zcmMvYXBwL3BhZ2VzL0xvYWRpbmdQYWdlL0xvYWRpbmdQYWdlQ29udHJvbGxlci50c1wiLFxuXHRcIi4vTG9hZGluZ1BhZ2VDb250cm9sbGVyLnRzXCI6IFwiLi9zcmMvYXBwL3BhZ2VzL0xvYWRpbmdQYWdlL0xvYWRpbmdQYWdlQ29udHJvbGxlci50c1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKG1hcCwgcmVxKSkge1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gbWFwW3JlcV07XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuL3NyYy9hcHAvcGFnZXMvTG9hZGluZ1BhZ2Ugc3luYyByZWN1cnNpdmUgXlxcXFwuXFxcXC8uKiRcIjsiLCIvLyBJbXBvcnRzXG52YXIgX19fSFRNTF9MT0FERVJfR0VUX1NPVVJDRV9GUk9NX0lNUE9SVF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvaHRtbC1sb2FkZXIvZGlzdC9ydW50aW1lL2dldFVybC5qc1wiKTtcbnZhciBfX19IVE1MX0xPQURFUl9JTVBPUlRfMF9fXyA9IHJlcXVpcmUoXCIuLi8uLi8uLi9hc3NldHMvaW1nL2xvYWRpbmcuZ2lmXCIpO1xuLy8gTW9kdWxlXG52YXIgX19fSFRNTF9MT0FERVJfUkVQTEFDRU1FTlRfMF9fXyA9IF9fX0hUTUxfTE9BREVSX0dFVF9TT1VSQ0VfRlJPTV9JTVBPUlRfX18oX19fSFRNTF9MT0FERVJfSU1QT1JUXzBfX18pO1xudmFyIGNvZGUgPSBcIjxoMSBpZD1cXFwid2VsY29tZVxcXCI+XFxyXFxuICAgIFdlbGNvbWUhXFxyXFxuPC9oMT5cXHJcXG48ZGl2IGlkPVxcXCJjYXJnYW5kb1xcXCI+XFxyXFxuICAgIDxpbWcgc3JjPVxcXCJcIiArIF9fX0hUTUxfTE9BREVSX1JFUExBQ0VNRU5UXzBfX18gKyBcIlxcXCIgYWx0PVxcXCJcXFwiPlxcclxcbiAgICBMb2FkaW5nLi4uXFxyXFxuPC9kaXY+XCI7XG4vLyBFeHBvcnRzXG5tb2R1bGUuZXhwb3J0cyA9IGNvZGU7IiwidmFyIGFwaSA9IHJlcXVpcmUoXCIhLi4vLi4vLi4vLi4vbm9kZV9tb2R1bGVzL3N0eWxlLWxvYWRlci9kaXN0L3J1bnRpbWUvaW5qZWN0U3R5bGVzSW50b1N0eWxlVGFnLmpzXCIpO1xuICAgICAgICAgICAgdmFyIGNvbnRlbnQgPSByZXF1aXJlKFwiISEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvY3NzLWxvYWRlci9kaXN0L2Nqcy5qcyEuLi8uLi8uLi8uLi9ub2RlX21vZHVsZXMvc2Fzcy1sb2FkZXIvZGlzdC9janMuanMhLi9Mb2FkaW5nUGFnZS5zY3NzXCIpO1xuXG4gICAgICAgICAgICBjb250ZW50ID0gY29udGVudC5fX2VzTW9kdWxlID8gY29udGVudC5kZWZhdWx0IDogY29udGVudDtcblxuICAgICAgICAgICAgaWYgKHR5cGVvZiBjb250ZW50ID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICBjb250ZW50ID0gW1ttb2R1bGUuaWQsIGNvbnRlbnQsICcnXV07XG4gICAgICAgICAgICB9XG5cbnZhciBvcHRpb25zID0ge307XG5cbm9wdGlvbnMuaW5zZXJ0ID0gXCJoZWFkXCI7XG5vcHRpb25zLnNpbmdsZXRvbiA9IGZhbHNlO1xuXG52YXIgdXBkYXRlID0gYXBpKGNvbnRlbnQsIG9wdGlvbnMpO1xuXG5cblxubW9kdWxlLmV4cG9ydHMgPSBjb250ZW50LmxvY2FscyB8fCB7fTsiLCJleHBvcnQgY2xhc3MgTG9hZGluZ1BhZ2VDb250cm9sbGVyIHtcclxuXHJcbiAgICB2aWV3OiBhbnk7XHJcbiAgICBjb21wb25lbnQgPSB7XHJcbiAgICAgICAgaWQ6ICdsb2FkaW5nLXBhZ2UnLFxyXG4gICAgICAgIHZpZXc6ICdMb2FkaW5nUGFnZS5odG1sJyxcclxuICAgICAgICBzdHlsZTogJ0xvYWRpbmdQYWdlLnNjc3MnLFxyXG4gICAgICAgIHN0YXJ0OiAoKSA9PiB7IHRoaXMuc3RhcnQoKSB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkVmlldygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIGluaWNpYWxpemEgbGFzIGFuaW1hY2lvbmVzIGRlIGNhcmdhLiBTZSBsbGFtYSBkZXNkZSBlbCBhcHAudHMgY3VhbmRvIHNlIGNhcmdhIGVsIGJvZHkuXHJcbiAgICAgKi9cclxuICAgIHN0YXJ0KCkge1xyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIEZ1bmNpw7NuIHF1ZSBjb250cm9sYSBsYSBkZXNhcGFyaWNpw7NuIGRlIGxhIHBhbnRhbGxhIGRlIGNhcmdhLiBFc3TDoSBwdWVzdGEgcGFyYSBxdWUgYSBsb3MgNSBzZWd1bmRvcywgZXN0YSBwYW50YWxsYSBkZXNhcGFyZXpjYS5cclxuICAgICAgICAgKi9cclxuICAgICAgICB3aW5kb3cuc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhbnRhbGxhOiBhbnkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHt0aGlzLmNvbXBvbmVudC5pZH1gKTtcclxuICAgICAgICAgICAgcGFudGFsbGEuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICB9LDUwMDApXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jacOzbiBxdWUgY2FyZ2EgbGEgdmlzdGEgKGVsIGh0bWwpIGRlIGxhIHDDoWdpbmEgZGUgbWFuZXJhIGRpbsOhbWljYSBzZWfDum4gc3UgYXRyaWJ1dG8gZGUgY29tcG9uZW50LiBcclxuICAgICAqL1xyXG4gICAgbG9hZFZpZXcoKSB7XHJcbiAgICAgICAgcmVxdWlyZShgLi8ke3RoaXMuY29tcG9uZW50LnN0eWxlfWApO1xyXG4gICAgICAgIHRoaXMudmlldyA9IHJlcXVpcmUoYC4vJHt0aGlzLmNvbXBvbmVudC52aWV3fWApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIG9idGllbmUgZWwgaWQgZGVsIGNvbXBvbmVudGUgeSBzdSBmcmFnbWVudG8gZGUgZG9jdW1lbnRvIGh0bWwgeSBsb3MgcmV0b3JuYSBhbCBhcHAudHMgcGFyYSBxdWUgcHVlZGFuIHNlciByZW5kZXJpemFkb3MuXHJcbiAgICAgKiBcclxuICAgICAqIEByZXR1cm5zIFtzdHJpbmcsIERvY3VtZW50RnJhZ21lbnRdXHJcbiAgICAgKi9cclxuICAgIGdldFZpZXcoKTogW3N0cmluZywgRG9jdW1lbnRGcmFnbWVudF0ge1xyXG4gICAgICAgIHJldHVybiBbdGhpcy5jb21wb25lbnQuaWQsIGRvY3VtZW50LmNyZWF0ZVJhbmdlKCkuY3JlYXRlQ29udGV4dHVhbEZyYWdtZW50KHRoaXMudmlldyldO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY2nDs24gcXVlIGVuY3VlbnRyYSB1biBlbGVtZW50byBIVE1MIGRlbnRybyBkZWwgZG9jdW1lbnRvIHNlZ8O6biBlbCBzZWxlY3RvciBxdWUgbGUgZW50cmEgY29tbyBwYXLDoW1ldHJvLiBFbCBwYXLDoW1ldHJvIGFsbCBkZWZpbmUgc2lcclxuICAgICAqIHNlIGJ1c2NhIHBvciBpZCBvIHBvciBjbGFzZS5cclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIHNlbGVjdG9yIFxyXG4gICAgICogQHBhcmFtIGFsbCBcclxuICAgICAqL1xyXG4gICAgZmluZEluc2lkZU1lKHNlbGVjdG9yOiBzdHJpbmcsIGFsbCA9IGZhbHNlKSB7XHJcbiAgICAgICAgY29uc3QgcXVlcnkgPSBgI2FwcCAjJHt0aGlzLmNvbXBvbmVudC5pZH0gJHtzZWxlY3Rvcn1gO1xyXG4gICAgICAgIGlmICghYWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHF1ZXJ5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChxdWVyeSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn0iLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9hdWRpby9NcDNfc29uZy5tcDNcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9hdWRpby9PZ2dfc29uZy5vZ2dcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9pbWcvM2QtZGVzaWduLnBuZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy8zZC1kZXNpbmcyLnBuZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9jb21wZXRlbmNlLnBuZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9jb21wZXRlbmNlMi5wbmdcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9pbWcvZmFjZWJvb2suc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL2luc3RhZ3JhbS5zdmdcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9pbWcvbGlua2VkaW4uc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL2xvYWRpbmcuZ2lmXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL3Bob3RvMS5qcGdcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9pbWcvcGhvdG8yLmpwZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9waG90bzMuanBlZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9waG90bzQuanBnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL3Bob3RvNS5qcGdcIjsiLCJleHBvcnQgZGVmYXVsdCBcImFzc2V0cy9pbWcvcHJvamVjdDIuanBnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL3Byb2plY3QzLmpwZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9wcm9qZWN0czEuanBnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL3N0dWR5LnBuZ1wiOyIsImV4cG9ydCBkZWZhdWx0IFwiYXNzZXRzL2ltZy9zdHVkeTIucG5nXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvaW1nL3R3aXR0ZXIuc3ZnXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCJhc3NldHMvdmlkZW8vdGVzdC5tcDRcIjsiLCJleHBvcnQgZGVmYXVsdCBcImZvbnRzL0Vhc3RtYW5UcmlhbC1CbGFjay5vdGZcIjsiLCJyZXF1aXJlKCcuL2FwcC9hcHAnKTsiXSwic291cmNlUm9vdCI6IiJ9