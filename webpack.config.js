var HtmlWebpackPlugin = require('html-webpack-plugin');

/**
 * Plugin que limpia la carpeta dist y la vuelve a crear después de correr el npm run build
 * 
 * Para instalarlo: npm install --save-dev clean-webpack-plugin
 */
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const path = require('path');

module.exports = {

    mode: 'development',
    entry: './src/main.ts',
    devtool: 'inline-source-map',

    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },

    module: {

        rules: [{
            test: /\.tsx?$/,
            use: 'awesome-typescript-loader',
            exclude: /node_modules/,
        },

        {//Configuración para cargar imágenes
            test: /\.(png|jpe?g|gif|svg)$/i,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'assets/img/',
                publicPath: 'assets/img/',
            }
        },
        {//Configuración para cargar videos
            test: /\.(mp4|mov|avi|wmv)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'assets/video/',
                publicPath: 'assets/video/',
            }
        },
        {//Configuración para cargar audio
            test: /.(ogg|mp3|wav|mpe?g)$/i,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'assets/audio/',
                publicPath: 'assets/audio/',
            }
        },
        {//Configuración para cargar fuentes
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'fonts/',
                publicPath: 'fonts/',
            }
        },

        {
            test: /\.s[ac]ss$/i,
            use: [
                // Creates `style` nodes from JS strings
                'style-loader',
                // Translates CSS into CommonJS
                'css-loader',
                // Compiles Sass to CSS
                'sass-loader',
            ],
        },

        {
            test: /\.html$/i,
            loader: 'html-loader',
        },

        ],
    },

    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.html', '.css'],
    },

    plugins: [
        new HtmlWebpackPlugin(),
        new CleanWebpackPlugin(),
    ],

    devServer: {

        contentBase: path.join(__dirname, 'src'),
        compress: true,
        port: 8888,
        open: true,
        hot: true,

        watchOptions: {
            poll: true
        }
    }
};