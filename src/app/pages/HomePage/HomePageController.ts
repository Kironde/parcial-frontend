export class HomePageController {

    view: any;
    component = {
        id: 'home-page',
        view: 'HomePage.html',
        style: 'HomePage.scss',
        start: () => { this.start() }
    }

    constructor() {
        this.loadView();
    }

    /**
     * Función que inicializa las animaciones de carga. Se llama desde el app.ts cuando se carga el body.
     */
    start() {
        window.setTimeout(() => {
            this.fillSkillBars();
            this.animateIcons();
        },5000)
        this.scrollEvents();
    }

    /**
     * Función que carga la vista (el html) de la página de manera dinámica según su atributo de component. 
     */
    loadView() {
        require(`./${this.component.style}`);
        this.view = require(`./${this.component.view}`);
    }

    /**
     * Función que obtiene el id del componente y su fragmento de documento html y los retorna al app.ts para que puedan ser renderizados.
     * 
     * @returns [string, DocumentFragment]
     */
    getView(): [string, DocumentFragment] {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    }

    /**
     * Función que se encarga de los eventos de scroll, llamando las funciones específicas para cada evento según la posición.
     */
    scrollEvents() {
        window.onscroll = (_: any) => {
            var scroll_position = window.scrollY;
            console.log(scroll_position);

            // Play y Pause audios
            if(scroll_position < 70){
                this.pauseAudio();
            }

            // rellenar y vaciar barras
            if(scroll_position >= 0 && scroll_position <= 450){
                this.fillSkillBars();
                this.animateIcons();

            }
            else {
                this.emptySkillBars();
                this.animateIcons(true);
            }
            if (scroll_position >= 70 && scroll_position < 1560) {//Animaciones del header
                this.animateHeader()
                if (scroll_position > 450 && scroll_position < 1450) {//Animaciones de la sección de biografía
                    this.playAudio();
                    this.animateBiography();
                }
                else {
                    this.animateBiography(true);
                }
            }
            else {
                this.animateHeader(true);
            }

            if (scroll_position > 1450 && scroll_position < 2938) {//Animaciones de la sección Projects
                this.animateProjectSection();

            }
            else {
                this.animateProjectSection(true);
            }

            if(scroll_position >= 3000 && scroll_position <3900){
                this.animatePhotos();
            }else if(scroll_position<3000){
                this.animatePhotos(true);
            }else{
                this.animatePhotos(true,true);
            }

            if(scroll_position>=4100){
                this.animatePlayer()
            }else{
                this.animatePlayer(true);
            }

            if(scroll_position >= 1560 && scroll_position <3110){
                this.animateHeader()
                this.playAudio(false);
            }

            if (scroll_position >= 3110) {
                this.animateHeader()
                this.pauseAudio();
            }
        }
    }

    /**
     * Función que anima el header de la página. El parámetro de entrada define si la animación es de entrada o de salida.
     * 
     * @param out 
     */
    animateHeader(out = false){
        const header: any = this.findInsideMe(".header");
        if(!out){   
            header.style.backgroundColor = '#cae7b9';
        }
        else{
            header.style.backgroundColor = 'transparent';
        }
    }

    /**
     * Función que anima los íconos de la sección de presentación. El parámetro de entrada define si la animación es de entrada o de salida.
     * 
     * @param out 
     */
    animateIcons(out = false){
        const iconOpacity: any = this.findInsideMe(".presentationIconColor", true);
        
        if(!out){
            for(let i = 0; i<iconOpacity.length; i++){
                iconOpacity[i].style.opacity = 1;
            }
        }
        else{
            for(let i = 0; i<iconOpacity.length; i++){
                iconOpacity[i].style.opacity = 0;
            }
        }
    }

    /**
     * Función que anima la sección de proyectos. El parámetro de entrada define si la animación es de entrada o de salida.
     * 
     * @param out 
     */
    animateProjectSection(out = false){
        const tituloProject: any = this.findInsideMe('.ptitulo', true);
        const subtitleProject: any = this.findInsideMe('.psubtitulo', true);
        const textProject: any = this.findInsideMe('.ptexto', true);
        const animaProject: any = this.findInsideMe('.animationP', true);
        if(!out){
            for (let i = 0; i < tituloProject.length; i++) {
                tituloProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (let i = 0; i < subtitleProject.length; i++) {
                subtitleProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (let i = 0; i < textProject.length; i++) {
                textProject[i].style.animation = 'text_reveal .5s ease forwards';
            }
            for (let i = 0; i < animaProject.length; i++) {
                animaProject[i].style.animation = 'text_reveal_box 1s ease';
            }
        }
        else{
            for (let i = 0; i < tituloProject.length; i++) {
                tituloProject[i].style.animation = '';
            }
            for (let i = 0; i < subtitleProject.length; i++) {
                subtitleProject[i].style.animation = '';
            }
            for (let i = 0; i < textProject.length; i++) {
                textProject[i].style.animation = '';
            }
            for (let i = 0; i < animaProject.length; i++) {
                animaProject[i].style.animation = '';
            }
        }
    }

    /**
     * Función que anima los rombos de la sección de Biografía. El parámetro de entrada define si la animación es de entrada o salida.
     * 
     * @param out 
     */
    animateBiography(out = false){
        const romboInfo: any = this.findInsideMe('.RomboInfo', true);
        const romboSolitario: any = this.findInsideMe('.RomboSolitario', true);
        if(!out){
            for (let i = 0; i < romboInfo.length; i++) {
                romboInfo[i].style.opacity = 1;
                romboInfo[i].style.transform = 'rotate(45deg) scale(1,1)';
            }
            for (let i = 0; i < romboSolitario.length; i++) {
                romboSolitario[i].style.opacity = 1;
                romboSolitario[i].style.transform = 'rotate(45deg) scale(1,1)';
            }
        }
        else{
            for (let i = 0; i < romboInfo.length; i++) {
                romboInfo[i].style.opacity = 0;
                romboInfo[i].style.transform = 'rotate(45deg) scale(0,0)';
            }
            for (let i = 0; i < romboSolitario.length; i++) {
                romboSolitario[i].style.opacity = 0;
                romboSolitario[i].style.transform = 'rotate(45deg) scale(0,0)';
            }
        }
    }

    /**
     * Función anima que el grid que muestra las fotos en la sección Photos. 
     * El primer parámetro de entrada define si la animación es de entrada o salida.
     * El segundo parametro de entrada define si el scroll está arriba o abajo de la sección para
     * que dependiendo de esta se vaya a la derecha o a la izquierda.
     * 
     * @param out
     * @param afert
     */
    animatePhotos(out = false,after=false){
        const display: any = this.findInsideMe('.display', false);
        if(!out){
            display.style.opacity=1;
            display.style.left="0px";
        }
        else if (!after){
            display.style.opacity=0;
            display.style.left="900px";
        }else{
            display.style.opacity=0;
            display.style.left="-500px";
        }
    }


    /**
     * Función que anima la opacidad del vídeo. El parámetro de entrada define si la animación es de entrada o salida.
     * 
     * @param out 
     */
    animatePlayer(out = false){
        const player: any = this.findInsideMe('.player',false);
        if(!out){
            player.style.opacity=1;
        }
        else{
            player.style.opacity=0;
            // romboSolitario[i].style.transform = 'rotate(45deg) scale(0,0)';
        }
    }



    /**
     * Función que controla la reproducción de los audios. El parámetro de entrada define si se debe reproducir el mp3 o el ogg.
     * 
     * @param mp3 
     */
    playAudio(mp3 = true) {
        const Mp3: any = this.findInsideMe('#Mp3');
        const Ogg: any = this.findInsideMe('#Ogg');
        if(mp3){
            Mp3.play().then().catch((err: any) => console.log(`Error al reproducir el archivo: ${err}`));
            Mp3.volume = 0.15;
            Ogg.pause();
        }
        else{
            Mp3.pause();
            Ogg.play().then().catch((err: any) => console.log(`Error al reproducir el archivo: ${err}`));
            Ogg.volume = 0.15;
        }
    }

    /**
     * Función que pausa todos los audios.
     */
    pauseAudio(){
        const Mp3: any = this.findInsideMe('#Mp3');
        const Ogg: any = this.findInsideMe('#Ogg');
        Ogg.pause();
        Mp3.pause();
    }

    /**
     * Función que se encarga de llenar las barras de habilidades.
     */
    fillSkillBars() {
        const bars: any = this.findInsideMe('.filler', true);
        bars[0].style.width = '75%';
        bars[1].style.width = '40%';
        bars[2].style.width = '60%';
        bars[3].style.width = '80%';
        bars[4].style.width = '45%';
        bars[5].style.width = '70%';
        bars[6].style.width = '45%';
        bars[7].style.width = '85%';
    }


    /**
     * Función que se encarga de vaciar las barras de habilidades.
     */
    emptySkillBars() {
        const bars: any = this.findInsideMe('.filler', true);
        for (let i = 0; i < bars.length; i++)bars[i].style.width = '0%';
    }


    /**
     * Función que encuentra un elemento HTML dentro del documento según el selector que le entra como parámetro. El parámetro all define si
     * se busca por id o por clase.
     * 
     * @param selector 
     * @param all 
     */
    findInsideMe(selector: string, all = false) {
        const query = `#app #${this.component.id} ${selector}`;
        if (!all) {
            return document.querySelector(query);
        } else {
            return document.querySelectorAll(query);

        }
    }

}