export class LoadingPageController {

    view: any;
    component = {
        id: 'loading-page',
        view: 'LoadingPage.html',
        style: 'LoadingPage.scss',
        start: () => { this.start() }
    }

    constructor() {
        this.loadView();
    }

    /**
     * Función que inicializa las animaciones de carga. Se llama desde el app.ts cuando se carga el body.
     */
    start() {
        /**
         * Función que controla la desaparición de la pantalla de carga. Está puesta para que a los 5 segundos, esta pantalla desaparezca.
         */
        window.setTimeout(() => {
            const pantalla: any = document.getElementById(`${this.component.id}`);
            pantalla.style.display = 'none';
        },5000)
    }

    /**
     * Función que carga la vista (el html) de la página de manera dinámica según su atributo de component. 
     */
    loadView() {
        require(`./${this.component.style}`);
        this.view = require(`./${this.component.view}`);
    }

    /**
     * Función que obtiene el id del componente y su fragmento de documento html y los retorna al app.ts para que puedan ser renderizados.
     * 
     * @returns [string, DocumentFragment]
     */
    getView(): [string, DocumentFragment] {
        return [this.component.id, document.createRange().createContextualFragment(this.view)];
    }

    /**
     * Función que encuentra un elemento HTML dentro del documento según el selector que le entra como parámetro. El parámetro all define si
     * se busca por id o por clase.
     * 
     * @param selector 
     * @param all 
     */
    findInsideMe(selector: string, all = false) {
        const query = `#app #${this.component.id} ${selector}`;
        if (!all) {
            return document.querySelector(query);
        } else {
            return document.querySelectorAll(query);

        }
    }

}