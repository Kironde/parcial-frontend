import { HomePageController } from './pages/HomePage/HomePageController';
import { LoadingPageController } from './pages/LoadingPage/LoadingPageController';

class App {

  mainPages = [
    LoadingPageController,
    HomePageController,
  ];

  loaded: any = [];

  /**
   * Función que renderiza las páginas que están en el array de mainPages.
   * Esta función llama a varias funciones dentro de las páginas para, por ejemplo, obtener sus elementos.
   */
  render() {
    const component = this.component();
    this.mainPages.forEach(pageController => {
      const controller = new pageController();
      this.loaded.push(controller);
      const [elId, element] = new pageController().getView();
      component.appendChild(this.createPage(elId, element));
    })
    
    document.body.append(component);
    document.body.onload = () => {
      this.loaded.forEach((controller: any) => {
        if ('start' in controller.component) {
          controller.component.start();
        }
      })
    }
  }

  /**
   * Función que crea un div dentro de la página. Este div tendrá todo el contenido de la página en su interior.
   * 
   * @param id 
   * @param element 
   * 
   * @return HTMLDivElement
   */
  createPage(id: string, element: DocumentFragment) {
    const page = document.createElement('div');
    page.id = id;
    page.appendChild(element);
    return page;
  }

  /**
   * Función que crea la div #app dentro de la página. Esta es la div principal de toda la aplicación, dentro de ella estarán las páginas creadas.
   * 
   * @return HTMLDivElement
   */
  component() {
    const element = document.createElement('div');
    element.id = "app";
    return element;
  }

}

const app = new App();
app.render();
